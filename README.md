# Project Condor
WordPress theme files plus a handful of custom plugins for project condor. The  theme is `wd_s` by [WebDev Studios](https://wdunderscores.com/). The theme features [Gulp](http://gulpjs.com/), [LibSass](http://sass-lang.com/), [PostCSS](https://github.com/postcss/postcss), [Bourbon](http://bourbon.io/), [Neat](http://neat.bourbon.io/), and [BrowserSync](https://www.browsersync.io/) to help make your development process fast and efficient. 

<pre>
   #
  :#:
  : :
  : :
.'   '.
:_____:  .___. .___.
|     |  |   | |   |        
|     |  '. .' '. .'                 
|     |    |     |      .--''''''--.
|_____|    |     |     |'''''/      |
:_____:   -'-   -'-    '''''/_...--'|
                           |__...--'    
</pre>

Also bundled with the theme is [TGM Plugin Activation](http://tgmpluginactivation.com/) that notifies the admin which plugins are required to operate the site. The custom plugins included are: Countdown and MailChimp for Caldera Forms.

##Set-Up WP Install
Please set up the local site's domain name as `temeculaescapes.dev`. Some of the premium plugins are configured to work with that local domain name.

## Development

After you've installed and activated the theme. It's time to setup Gulp.

1) From the command line, change directories to your new theme directory

```bash
cd /your-project/wordpress/wp-content/themes/escapes
```

2) Install Node dependencies

```bash
npm install
```
![Install and Gulp](https://dl.dropbox.com/s/cj1p6xjz51cpckq/wdescapes-install.gif?dl=0)

### Gulp Tasks

From the command line, type any of the following to perform an action:

`gulp watch` - Automatically handle changes to CSS, JS, SVGs, and image sprites. Also kicks off BrowserSync.

`gulp icons` - Minify, concatenate, and clean SVG icons.

`gulp i18n` - Scan the theme and create a POT file

`gulp sass:lint` - Run Sass against WordPress code standards

`gulp scripts` - Concatenate and minify javascript files

`gulp sprites` - Generate an image sprite and the associated Sass (sprite.png)

`gulp styles` - Compile, prefix, combine media queries, and minify CSS files

`gulp` - Runs the following tasks at the same time: i18n, icons, scripts, styles, sprites


##Install Plugins

Plugins are either bundled with this repo or you'll be prompted to install them once the theme is installed. License keys are required for some of the Caldera plugins. To apply licenses, hover over Caldera Forms, click on the appropriate product
* Clarity For FacetWP: `cwp-768-19W8-0G03`
* Easy Pod: `cwp-QITH-HLW-8GDPTYHB`
* Easy Queries: `cwp-A3B0-55ZV-33KYIK0C`

##Install a copy of the Database

Import a copy of the website database. Please see Evaero for a copy. 