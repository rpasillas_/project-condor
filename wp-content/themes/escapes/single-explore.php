<?php

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">

			<?php escapes_do_post_action_menu(); ?>	

			<main id="main" class="site-main" role="main">

			<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			}
			
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					

					get_template_part( 'template-parts/content', 'eventfeatureprofile' );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>

			</main><!-- #main -->
			
			<?php echo fb_comments_tag(); ?>

		</div><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->

	<div class="featured-articles">
		<div class="wrap">
			<div class="heading"><span class="h1">Featured Articles</span></div>
			
			<?php 
				$posts = te_featured_articles( 'explore' ); 
				
				foreach ( $posts as $post )
				{
					$author_id = $post->post_author;
					setup_postdata( $post ); 

					echo escapes_do_featured_articles( $post, $author_id );
				}
			?>
			
		</div>
	</div>

<?php get_footer(); ?>