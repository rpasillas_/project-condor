<?php
/**
 * The template for displaying the experience landing page.
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">
				
				<h1>Temecula Wineries, Breweries &amp; Events</h1>
				<article>
					<p>You cannot come to Temecula Valley without experiencing the crown jewels of the region. This valley runs plentiful with some of the best wines and brews in the world. Each winery and brewery has mastered their unique craft and offers a wide variety of delicious options that you will not find anywhere else. From large hill top tasting rooms with gorgeous views of Temecula Valley to quirky taprooms scattered around the city, we encourage you to experience all the wineries and breweries you can handle. Browse Temecula’s favorite local wineries, breweries and events or blaze your own trail.</p>
				</article>


				<?php echo do_shortcode('[caldera_clarity name="experiences"]'); ?>



			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->


	<?php 		
		get_template_part('template-parts/experiences', 'banners'); 

		//See inc/template-tags.php for function definition
		escapes_do_local_favorites( array('winery', 'brewery') ); 
	?>

<?php get_footer(); ?>





