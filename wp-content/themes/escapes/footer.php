<?php
/**
 * The template for displaying the footer.
 * 
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Temecula Escapes
 */

?>

	</div><!-- #content -->

<?php if(!is_page_template('tpl-page-mailchimp-raffle.php')): ?>

	<section class="contact-footer" style="background-image: url(<?php echo get_field('contact_banner_image', 'options'); ?>);">
		<div class="wrap">
			<div class="form-wrap">
				<header><h1><span class="ostrich">Send a Quick</span> <span class="new-shine">Hello</span></h1></header>
				<?php echo do_shortcode( '[caldera_form id="CF578e9449cd4b4"]' ); ?>
			</div>
		</div>

		<nav class="footer-navigation-primary">
			<ul class="menu footer-menu-primary" id="footer-menu-primary-lrg">
				<li class="menu-item"><a href="<?php echo esc_url( home_url('explore-temecula/') ); ?>">Explore</a></li>
				<li class="menu-item"><a href="<?php echo esc_url( home_url('eat-drink-temecula/') ); ?>">Eat &amp; Drink</a></li>
				<li class="menu-item"><a href="<?php echo esc_url( home_url('temecula-events/') ); ?>">Happenings</a></li>

			</ul>
		</nav>
	</section>

<?php endif; ?>	

	<footer class="site-footer">
		<div class="wrap">

			<svg class="site-logo"><use xlink:href="#icon-temecula-escapes"></use></svg>
			
			<?php echo social_media_nav('Connect With Us!'); ?>

			<nav class="footer-navigation-primary">
				<ul class="menu footer-menu-primary" id="footer-menu-primary">
					<li class="menu-item"><a href="<?php echo esc_url( home_url('experience/') ); ?>">Explore</a></li>
				<li class="menu-item"><a href="<?php echo esc_url( home_url('eat-drink-temecula/') ); ?>">Eat &amp; Drink</a></li>
				<li class="menu-item"><a href="<?php echo esc_url( home_url('temecula-events/') ); ?>">Happenings</a></li>
				</ul>
			</nav>

			<?php
				wp_nav_menu( array(
					'theme_location' => 'footer',
					'menu_id'        => 'footer-menu-secondary',
					'menu_class'     => 'menu footer-menu-secondary',
					'container'		=> 'nav',
					'container_class'	=> 'footer-navigation-secondary'
				) );
			?>

			<div class="site-copyright">
				<?php escapes_do_copyright_text(); ?>
			</div>

		</div><!-- .wrap -->
	</footer><!-- .site-footer -->
</div><!-- #page -->

<div class="feature-popup-outer-container">
	<div class="feature-popup-container">
		<div id="account-popup" class="feature-popup">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/red-wine-bottle-glass.jpg" alt="Wine Bottle with Glass">
			
			<div class="text">
				<span class="h1">Like a fine <span class="new-shine">wine,</span><br>
				this feature will be<br>
				ready in <span class="new-shine">good time.</span></span>
			</div>
			
			<span class="close">x</span>
		</div>

		<div id="favorites-popup" class="feature-popup">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/white-wine-bottle-glass.jpg" alt="Wine Bottle with Glass">
			
			<div class="text">
				<span class="h1 new-shine">Coming Soon!</span>
				<span class="h1">Grab a glass of <span class="new-shine">chardonnay,</span><br>
			this feature is on its <span class="new-shine">way.</span></span>
			</div>

			<span class="close">x</span>
		</div>

		<div id="brewery-popup" class="feature-popup">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/beer-keg.jpg" alt="Keg of Beer">
			
			<div class="text">
				<span class="h1 new-shine">Still Brewing.</span>
				<span class="h1">This feature will be<br>
				available soon.</span>
			</div>

			<span class="close">x</span>
		</div>
	</div>
</div>

<?php wp_footer(); ?>

</body>
</html>
