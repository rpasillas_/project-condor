<?php
function eat_and_drink_cpt() { 
	register_taxonomy( 'eat_and_drink_category', 
		array('eat_and_drink'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' 				=> 'Eat and Drink Categories',
				'singular_name' 	=> 'Eat and Drink Category',
				'search_items' 		=>  'Search Eat and Drink Categories',
				'all_items' 		=> 'All Eat and Drink Categories',
				'parent_item' 		=> 'Parent Eat and Drink Category',
				'parent_item_colon' => 'Parent Eat and Drink Category:',
				'edit_item' 		=> 'Edit Eat and Drink Category',
				'update_item' 		=> 'Update Eat and Drink Category',
				'add_new_item' 		=> 'Add New Eat and Drink Category',
				'new_item_name' 	=> 'New Eat and Drink Category Name',
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> false,
			'rewrite' 			=> array( 'slug' => 'eat-drink-temecula/categories', 'with_front'=> false ),
		)
	);
	
	register_post_type( 'eat_and_drink',
		array( 
			'labels' 	=> array(
					'name' 					=> 'Eat and Drink', 			
					'singular_name' 		=> 'Eat and Drink',
					'all_items' 			=> 'Eat and Drink', 
					'add_new' 				=> 'Add New', 
					'add_new_item' 			=> 'Add New Eat and Drink', 
					'edit' 					=> 'Edit', 
					'edit_item' 			=> 'Edit Eat and Drink', 
					'new_item' 				=> 'New Eat and Drink', 
					'view_item' 			=> 'View Eat and Drink', 
					'search_items' 			=> 'Search Eat and Drink', 
					'not_found' 			=> 'Nothing found in the Database.',  
					'not_found_in_trash' 	=> 'Nothing found in Trash', 
					'parent_item_colon' 	=> ''
				), 
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search'	=> false,
			'show_ui' 				=> true,
			'query_var' 			=> true, 
			'menu_icon' 			=> get_template_directory_uri() . '/icons/hamburger.png', 
			'rewrite'				=> array( 'slug' => 'eat-drink-temecula', 'with_front' => false ), 
			'has_archive' 			=> 'eat-drink-temecula', 
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'show_in_rest'			=> true,			
			'supports' 				=> array(  'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) 
	); 
			
}

add_action( 'init', 'eat_and_drink_cpt');