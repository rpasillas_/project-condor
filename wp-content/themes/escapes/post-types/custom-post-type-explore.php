<?php
function explore_cpt() { 
	register_taxonomy( 'explore_category', 
		array('explore'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' 				=> 'Explore Categories',
				'singular_name' 	=> 'Explore Category',
				'search_items' 		=>  'Search Explore Categories',
				'all_items' 		=> 'All Explore Categories',
				'parent_item' 		=> 'Parent Explore Category',
				'parent_item_colon' => 'Parent Explore Category:',
				'edit_item' 		=> 'Edit Explore Category',
				'update_item' 		=> 'Update Explore Category',
				'add_new_item' 		=> 'Add New Explore Category',
				'new_item_name' 	=> 'New Explore Category Name',
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> false,
			'rewrite' 			=> array( 'slug' => 'explore-temecula/categories', 'with_front'=> false ),
		)
	);
	
	register_post_type( 'explore',
		array( 
			'labels' 	=> array(
					'name' 					=> 'Explore', 			
					'singular_name' 		=> 'Explore',
					'all_items' 			=> 'Explore', 
					'add_new' 				=> 'Add New', 
					'add_new_item' 			=> 'Add New Explore', 
					'edit' 					=> 'Edit', 
					'edit_item' 			=> 'Edit Explore', 
					'new_item' 				=> 'New Explore', 
					'view_item' 			=> 'View Explore', 
					'search_items' 			=> 'Search Explore', 
					'not_found' 			=> 'Nothing found in the Database.',  
					'not_found_in_trash' 	=> 'Nothing found in Trash', 
					'parent_item_colon' 	=> ''
				), 
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search'	=> false,
			'show_ui' 				=> true,
			'query_var' 			=> true, 
			'menu_icon' 			=> get_template_directory_uri() . '/icons/compass.png', 
			'rewrite'				=> array( 'slug' => 'explore-temecula', 'with_front' => false ), 
			'has_archive' 			=> 'explore-temecula', 
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'show_in_rest'			=> true,			
			'supports' 				=> array(  'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) 
	); 
			
}

add_action( 'init', 'explore_cpt');