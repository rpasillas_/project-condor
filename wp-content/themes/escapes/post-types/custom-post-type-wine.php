<?php
function winery_cpt() { 
	register_taxonomy( 'winery_category', 
		array('winery'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' 				=> 'Winery Categories',
				'singular_name' 	=> 'Winery Category',
				'search_items' 		=> 'Search Winery Categories',
				'all_items' 		=> 'All Winery Categories',
				'parent_item' 		=> 'Parent Winery Category',
				'parent_item_colon' => 'Parent Winery Category:',
				'edit_item' 		=> 'Edit Winery Category',
				'update_item' 		=> 'Update Winery Category',
				'add_new_item' 		=> 'Add New Winery Category',
				'new_item_name' 	=> 'New Winery Category Name',
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> false,
			'rewrite' 			=> array( 'slug' => 'experience/temecula-wineries/categories', 'with_front'=> false ),
		)
	);

	register_taxonomy( 'winery_profile', 
		array('winery'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' => 'Profile Types',
				'singular_name' => 'Profile Type',
				'search_items' =>  'Search Profile Types',
				'all_items' => 'All Profile Types',
				'parent_item' => 'Parent Profile Type',
				'parent_item_colon' => 'Parent Profile Type:',
				'edit_item' => 'Edit Profile Type',
				'update_item' => 'Update Profile Type',
				'add_new_item' => 'Add New Profile Type',
				'new_item_name' => 'New Profile Type'
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> 'winery_profile_category',
			'rewrite' 			=> array( 'slug' => 'winery', 'with_front'=> false ),
		)
	);
	
	register_post_type( 'winery', 
		array( 
			'labels' 	=> array(
					'name' 					=> 'Winery', 			
					'singular_name' 		=> 'Winery',
					'all_items' 			=> 'Wineries', 
					'add_new' 				=> 'Add New', 
					'add_new_item' 			=> 'Add New Winery', 
					'edit' 					=> 'Edit', 
					'edit_item' 			=> 'Edit Winery', 
					'new_item' 				=> 'New Winery', 
					'view_item' 			=> 'View Winery', 
					'search_items' 			=> 'Search Wineries', 
					'not_found' 			=> 'Nothing found in the Database.',  
					'not_found_in_trash' 	=> 'Nothing found in Trash', 
					'parent_item_colon' 	=> ''
				), 
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search'	=> false,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'menu_icon' 			=> get_template_directory_uri() . '/icons/wine.png', 
			'rewrite'				=> array( 'slug' => 'experience/temecula-wineries', 'with_front' => true ), 
			'has_archive' 			=> true, 
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'show_in_rest'			=> true,			
			'supports' 				=> array(  'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) 
	); 
			
}

add_action( 'init', 'winery_cpt');