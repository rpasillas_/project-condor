<?php
function brewery_cpt() { 
	register_taxonomy( 'brewery_category', 
		array('brewery'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' 				=> 'Brewery Categories',
				'singular_name' 	=> 'Brewery Category',
				'search_items' 		=> 'Search Brewery Categories',
				'all_items' 		=> 'All Brewery Categories',
				'parent_item' 		=> 'Parent Brewery Category',
				'parent_item_colon' => 'Parent Brewery Category:',
				'edit_item' 		=> 'Edit Brewery Category',
				'update_item' 		=> 'Update Brewery Category',
				'add_new_item' 		=> 'Add New Brewery Category',
				'new_item_name' 	=> 'New Brewery Category Name',
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> false,
			'rewrite' 			=> array( 'slug' => 'experience/temecula-breweries/categories', 'with_front'=> false ),
		)
	);

	register_taxonomy( 'brewery_profile', 
		array('brewery'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' => 'Profile Types',
				'singular_name' => 'Profile Type',
				'search_items' =>  'Search Profile Types',
				'all_items' => 'All Profile Types',
				'parent_item' => 'Parent Profile Type',
				'parent_item_colon' => 'Parent Profile Type:',
				'edit_item' => 'Edit Profile Type',
				'update_item' => 'Update Profile Type',
				'add_new_item' => 'Add New Profile Type',
				'new_item_name' => 'New Profile Type'
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> 'brewery_profile_category',
			'rewrite' 			=> array( 'slug' => 'brewery', 'with_front'=> false ),
		)
	);
	
	register_post_type( 'brewery', 
		array( 
			'labels' 	=> array(
					'name' 					=> 'Brewery', 			
					'singular_name' 		=> 'Brewery',
					'all_items' 			=> 'Breweries', 
					'add_new' 				=> 'Add New', 
					'add_new_item' 			=> 'Add New Brewery', 
					'edit' 					=> 'Edit', 
					'edit_item' 			=> 'Edit Brewery', 
					'new_item' 				=> 'New Brewery', 
					'view_item' 			=> 'View Brewery', 
					'search_items' 			=> 'Search Breweries', 
					'not_found' 			=> 'Nothing found in the Database.',  
					'not_found_in_trash' 	=> 'Nothing found in Trash', 
					'parent_item_colon' 	=> ''
				), 
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search'	=> false,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'menu_icon' 			=> get_template_directory_uri() . '/icons/beer.png', 
			'rewrite'				=> array( 'slug' => 'experience/temecula-breweries', 'with_front' => true ), 
			'has_archive' 			=> true, 
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'show_in_rest'			=> true,			
			'supports' 				=> array(  'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) 
	); 
			
}

add_action( 'init', 'brewery_cpt');