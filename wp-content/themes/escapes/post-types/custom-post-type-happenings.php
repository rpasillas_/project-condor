<?php
function happenings_cpt() { 
	register_taxonomy( 'happenings_category', 
		array('happenings'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' 				=> 'Happenings Categories',
				'singular_name' 	=> 'Happenings Category',
				'search_items' 		=> 'Search Happenings Categories',
				'all_items' 		=> 'All Happenings Categories',
				'parent_item' 		=> 'Parent Happenings Category',
				'parent_item_colon' => 'Parent Happenings Category:',
				'edit_item' 		=> 'Edit Happenings Category',
				'update_item' 		=> 'Update Happenings Category',
				'add_new_item' 		=> 'Add New Happenings Category',
				'new_item_name' 	=> 'New Happenings Category Name',
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> false,
			'rewrite' 			=> array( 'slug' => 'temecula-events/categories', 'with_front'=> false ),
		)
	);

	register_taxonomy( 'happenings_locations', 
		array('happenings'), 
		array('hierarchical' => true,
			'labels' => array(
				'name' 				=> 'Happenings Locations',
				'singular_name' 	=> 'Happenings Location',
				'search_items' 		=> 'Search Happenings Locations',
				'all_items' 		=> 'All Happenings Locations',
				'parent_item' 		=> 'Parent Happenings Location',
				'parent_item_colon' => 'Parent Happenings Location:',
				'edit_item' 		=> 'Edit Happenings Location',
				'update_item' 		=> 'Update Happenings Location',
				'add_new_item' 		=> 'Add New Happenings Location',
				'new_item_name' 	=> 'New Happenings Location Name',
			),
			'public'			=> true,
			'show_admin_column' => true, 
			'show_ui' 			=> true,
			'query_var' 		=> false,
			'rewrite' 			=> array( 'slug' => 'temecula-events/locations/', 'with_front'=> false ),
		)
	);
	
	register_post_type( 'happenings', 
		array( 
			'labels' 	=> array(
					'name' 					=> 'Happenings', 			
					'singular_name' 		=> 'Happenings',
					'all_items' 			=> 'Happenings', 
					'add_new' 				=> 'Add New', 
					'add_new_item' 			=> 'Add New Happenings', 
					'edit' 					=> 'Edit', 
					'edit_item' 			=> 'Edit Happenings', 
					'new_item' 				=> 'New Happenings', 
					'view_item' 			=> 'View Happenings', 
					'search_items' 			=> 'Search Happenings', 
					'not_found' 			=> 'Nothing found in the Database.',  
					'not_found_in_trash' 	=> 'Nothing found in Trash', 
					'parent_item_colon' 	=> ''
				), 
			'public' 				=> true,
			'publicly_queryable' 	=> true,
			'exclude_from_search'	=> false,
			'show_ui' 				=> true,
			'query_var' 			=> true,
			'menu_icon' 			=> get_template_directory_uri() . '/icons/hot-air-balloon.png', 
			'rewrite'				=> array( 'slug' => 'temecula-events', 'with_front' => true ), 
			'has_archive' 			=> 'temecula-events', 
			'capability_type' 		=> 'post',
			'hierarchical' 			=> false,
			'show_in_rest'			=> true,
			'supports' 				=> array(  'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		) 
	); 
			
}

add_action( 'init', 'happenings_cpt');