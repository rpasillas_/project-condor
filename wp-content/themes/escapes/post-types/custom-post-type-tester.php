<?php

function my_custom_post_product() {
	$labels = array(
		'name'               => 'Prodotti',
		'singular_name'      => 'Prodotto',
		'add_new'            => 'Aggiungi Nuovo',
		'add_new_item'       => 'Aggiungi Nuovo Prodotto',
		'edit_item'          => 'Modifica Prodotto',
		'new_item'           => 'Nuovo Prodotto',
		'all_items'          => 'Tutti i Prodotti',
		'view_item'          => 'Vedi Prodotto',
		'search_items'       => 'Cerca Prodotti',
		'not_found'          => 'Nessun Prodotto Trovato',
		'not_found_in_trash' => 'Nessun Prodotto Trovato nel Cestino',
		'parent_item_colon'  => '',
		'menu_name'          => 'Prodotti'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Inserisci un nuovo prodotto',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
		'has_archive'   => true,
		'hierarchical'	=> true,
		'rewrite'		=> array('slug' => 'prodotto/%brand%','with_front' => false),
		'query_var'		=> true,
		//'rewrite'		=> true,
		//'publicly_queryable' => false,
	);
	register_post_type( 'prodotto', $args );
}
add_action( 'init', 'my_custom_post_product', 0 );


function my_taxonomies_product() {
	$labels = array(
		'name'              => 'Brand',
		'singular_name'     => 'Brand',
		'search_items'      => 'Search Product Categories',
		'all_items'         => 'All Product Categories',
		'parent_item'       => 'Parent Product Category',
		'parent_item_colon' => 'Parent Product Category:',
		'edit_item'         => 'Edit Product Category',
		'update_item'       => 'Update Product Category',
		'add_new_item'      => 'Add New Product Category',
		'new_item_name'     => 'New Product Category',
		'menu_name'         => 'Brand',
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' 	=> true,
		'public'		=> true,
		'query_var'		=> 'brand',

		'rewrite'		=>  array('slug' => 'prodotto' ),
		'_builtin'		=> false,
	);
	register_taxonomy( 'brand', 'prodotto', $args );
}
add_action( 'init', 'my_taxonomies_product', 0 );


add_filter('post_link', 'brand_permalink', 1, 3);
add_filter('post_type_link', 'brand_permalink', 1, 3);

function brand_permalink($permalink, $post_id, $leavename) {
    if (strpos($permalink, '%brand%') === FALSE) return $permalink;
        // Get post
        $post = get_post($post_id);
        if (!$post) return $permalink;

        // Get taxonomy terms
        $terms = wp_get_object_terms($post->ID, 'brand');
        if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0]))
        	$taxonomy_slug = $terms[0]->slug;
        else $taxonomy_slug = 'no-brand';

    return str_replace('%brand%', $taxonomy_slug, $permalink);
}