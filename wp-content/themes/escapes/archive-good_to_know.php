<?php
/**
 * The template for displaying the experience landing page.
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">				
				<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
				?>
				
				<?php echo do_shortcode('[caldera_clarity name="good_to_know"]'); ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->


	<?php 

		//See inc/template-tags.php for function definition
		escapes_do_local_favorites( 'good_to_know' ); 
	?>

<?php get_footer(); ?>