	<?php 
		if ( get_field( 'make_event' ) )
		{
	?>
		<?php escapes_do_post_action_menu(); ?>

		<?php dynamic_sidebar( 'sidebar-happenings' ); ?>

		<div class="venue-information">
			<span class="h2">Venue</span>
			<address>
				<?php
					echo "\t\t\t\t\t<strong>" . get_field( 'venue_name' ) . "</strong><br>\n";
					echo "\t\t\t\t\t" . get_field( 'venue_street_address' ) . "<br>\n";
					echo "\t\t\t\t\t" . get_field( 'venue_city' ) . ', CA ' . get_field( 'venue_zip' ) . "\n";
				?>
			</address>
			<?php the_field( 'additional_information' ); ?>

			
			<?php the_field( 'google_maps_embed' ); ?>


		</div>

		<?php echo do_shortcode('[te-ssm]'); ?>

	<?php } else{ ?>
		<?php escapes_do_post_action_menu(); ?>
	
		<?php dynamic_sidebar( 'sidebar-company' ); ?>
		
		<?php get_template_part( 'template-parts/upcoming', 'events' ); ?>

		<?php echo do_shortcode('[te-ssm]'); ?>

	<?php	}//if/else	?>