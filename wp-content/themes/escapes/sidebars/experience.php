<?php
	global $post;

	$the_ID = $post->ID;
?>
<?php escapes_do_post_action_menu(); ?>

	<div class="share-page">
		<span class="h2">Share</span>
		<?php echo do_shortcode('[ssba]'); ?>
	</div>

	<div class="fb-page" data-href="<?php the_field('facebook_profile'); ?>" data-tabs="timeline" data-width="303" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
		<blockquote cite="<?php the_field('facebook_profile'); ?>" class="fb-xfbml-parse-ignore">
			<a href="<?php the_field('facebook_profile'); ?>"><?php echo get_the_title($the_ID); ?></a>
		</blockquote>
	</div>

	<?php echo do_shortcode('[te-ssm]'); ?>