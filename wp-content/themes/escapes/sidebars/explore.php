<?php
	/**
	 * IF THE EXPLORE POST IS A COMPANY PROFILE OR NOT
	 */
	if( get_field('is_company_profile') )
		{
			escapes_do_post_action_menu(); 
?>
			<div class="venue-information">
				<span class="h2">Information</span>
				<?php the_field( 'additional_information' ); ?>
			</div>

			<?php echo do_shortcode('[te-ssm]'); ?>
			
<?php }else{ ?>

			<?php escapes_do_post_action_menu(); ?>

			<div class="te-socials">
				<span class="h1">Let's get social</span>
				<?php echo social_media_nav(); ?>
				@<?php the_field('instagram_handle', 'option'); ?>
			</div>

			<?php get_template_part( 'template-parts/upcoming', 'events' ); ?>
				
<?php
		}

	
?>


	

