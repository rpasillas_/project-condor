<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">

			<?php 
				if( is_singular('eat_and_drink') ){
					escapes_do_post_action_menu(); 
				}
			?>


			<main id="main" class="site-main" role="main">

			<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			}
				
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', get_post_format() );

			endwhile; // End of the loop.
			?>

			</main><!-- #main -->

			<?php echo fb_comments_tag(); ?>

		</div><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->

	<div class="featured-articles">
		<div class="wrap">
			<div class="heading"><span class="h1">Featured Articles</span></div>
			
			<?php 
				$posts = te_featured_articles( 'eat_and_drink' ); 
				
				foreach ( $posts as $post )
				{
					$author_id = $post->post_author;
					setup_postdata( $post ); 

					echo escapes_do_featured_articles( $post, $author_id );
				}
			?>
			
		</div>
	</div>

<?php get_footer(); ?>