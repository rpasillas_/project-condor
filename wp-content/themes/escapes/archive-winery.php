<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">
				<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
				?>


				<h1>Temecula Area Wineries</h1>
				<article>
					<p>Temecula Valley Wine Country, it’s in the name. Temecula offers wine lovers of all walks the chance to taste and enjoy the award-winning wines served here. With over 40+ wineries in the region, there is plenty of opportunity for new experiences upon each trip. Every winery offers its own personal take on the finer things in life and you can learn about how your wine goes from grape to glass. Discover all that wine country has to offer. Browse our Temecula area winery listings below and plan your visit today!</p>
				</article>


				
				<?php echo do_shortcode( '[caldera_clarity name="temecula_wineries"]' ); ?>


			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->


	<?php 		
		get_template_part('template-parts/experiences', 'banners'); 

		//See inc/template-tags.php for function definition
		escapes_do_local_favorites( 'winery' ); 
	?>

	<div class="featured-articles">
		<div class="wrap">
			<div class="heading"><span class="h1">Featured Articles</span></div>
			
			<?php 
				$posts = te_featured_articles( 'winery' ); 
				
				foreach ( $posts as $post )
				{
					$author_id = $post->post_author;
					setup_postdata( $post ); 

					echo escapes_do_featured_articles( $post, $author_id );
				}
			?>
			
		</div>
	</div>
	
<?php get_footer(); ?>