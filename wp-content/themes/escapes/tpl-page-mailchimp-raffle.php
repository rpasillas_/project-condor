<?php
/**
 * Template Name: TE MailChimp Special Events
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">

<?php
	if( get_query_var('special') == 'vssn122016' ):		
		
		get_template_part('template-parts/mailchimp','raffle');
	else:
		while ( have_posts() ) : the_post();

			the_content();

		endwhile; // End of the loop.
	endif; 
?>



			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->

<?php get_footer(); ?>