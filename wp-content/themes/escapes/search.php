<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Temecula Escapes
 */

get_header(); ?>
	

	<div class="wrap">
		<section class="primary content-area">
			<main id="main" class="site-main" role="main">			

			<?php
			if ( have_posts() ) : ?>

				<?php
				$cpts = array('winery', 'brewery', 'eat_and_drink', 'explore', 'happenings' );
				
				
    			foreach( $cpts as $cpt ) :
    				$i = 1;

    				$items = '';
    				$items_title = '';
    				$items_sizer = '';
    				
					/* Start the Loop */
					while ( have_posts() ) : the_post();
						
						if( $cpt == get_post_type() ):
							//--print the CPT Heading only once
							if( $i == 1 ):
								$items_title = '<h3 class="group-title">' . preg_replace('/_/', ' ', get_post_type()) . ' Results with "' . get_search_query() . '"</h3>';
								$items_sizer = '<div class="grid-sizer"></div>';
							endif;

							//get_template_part( 'template-parts/content', 'search' ); 


							$grid_item = new Grid;

							$post_thumb = $grid_item->gridImage( get_the_ID() );

							$grid = $grid_item->gridItem( $cat, get_the_ID() , get_the_title(), $post->post_name, $post_thumb );
							
							
							$items .= $grid;

							$i++;
						endif;												

					endwhile;
					rewind_posts();

					
					echo $items_title;
					echo '<div class="masonry-tiles search-tiles result-group ' . $cpt . '" id="' . $cpt . '_results">';
					echo $items_sizer;
					echo $items;
					echo '</div>';

					
				endforeach;

			else:

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>

			</main><!-- #main -->
		</section><!-- .primary -->

		<?php get_sidebar(); ?>

	</div><!-- .wrap -->	

<?php get_footer(); ?>