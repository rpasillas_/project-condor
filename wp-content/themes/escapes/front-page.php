<?php
/**
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Temecula Escapes
 */

global $post;


//define('FACEBOOK_SDK_V4_SRC_DIR', __DIR__ . '/inc/facebook/');
//require( __DIR__ . '/inc/Facebook/autoload.php');

$test_App_ID = '1773120069603203';
$test_App_Secret = 'acbf982feee3994a7fc433b82774b511';
$te_App_ID = '1773096546272222';
$te_App_Secret = '32ac560de8ee858dbdf711d3373a9a21';
$teProfile_ID = '1100068270023612';

// $fb = new Facebook\Facebook([
// 		'app_id' => $test_App_ID,
// 		'app_secret' => $test_App_Secret,
// 		'default_graph_version' => 'v2.5'
// 	]);



get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">

				<?php
					//See inc/template-tags.php for function definition
					escapes_do_escapes_choice( array('winery', 'brewery') );
				?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->
	<div class="locals-and-travelers" style="background-image: url(<?php the_field('locals_and_travelers_background'); ?>);">
		<div class="wrap">

			<div class="title">
				<span class="h1">Locals<br><span class="none">&amp;</span> Travelers</span>
			</div>

			<?php the_field('locals_and_travelers_text'); ?>

			<a href="<?php echo get_permalink(527); ?>" class="btn btn-outline btn-purple">Get Our Newsletter</a>
		</div>
	</div>

	<div class="wrap">
		<div class="section-local-favorites">
			<?php				
				//See inc/template-tags.php for function definition
				escapes_do_local_favorites( array('winery', 'brewery') ); 
			?>
		</div>
	</div>

	<div class="ss-banner">
		<div class="wrap">

			<?php 
				$img = get_field('ss_banner_image');
				$alt = ( $img['alt'] ) ? $img['alt']: $img['name'];				
			?>

			<div class="image-wrap" style="background-image: url(<?php echo $img['url']; ?>);">
				<img src="<?php echo $img['url']; ?>" alt="<?php echo $alt; ?>">
			</div>

			<div class="text-wrap">
				<div class="title">
					<span class="h1">Happenings</span>
				</div>
				<?php the_field('ss_banner_text'); ?>						
				<a href="<?php the_field('ss_btn_link'); ?>" title="Click here to <?php the_field('ss_btn_text'); ?>" class="btn btn-outline btn-white"><?php the_field('ss_btn_text'); ?></a>
			</div>

		</div>
	</div>

	<?php get_template_part('template-parts/ssm', 'widgets'); ?>

<?php get_footer(); ?>