<?php
/**
 * SET TRANSIENT FOR A DAY IF ITS NOT THE ADMIN
 *
 * @link http://wptheming.com/2012/08/display-the-most-recent-post-in-each-category/
 */
function te_transient_lifespan() 
{
  if( is_admin() && WP_DEBUG ) {
    return 1;
  } else {
    return DAY_IN_SECONDS;
  }
}

/** 
  * EACH MAIN NAV ITEM (OR CPT) 
  * GETS ITS OWN NAV FUNCTION
  * SO THAT IT CAN STORE ITS OWN TRANSIENT
 **/ 
function te_explore_navigation()
{
	$te_explore_nav = array();
	$te_explore_nav = te_main_navigation('explore', 'explore_category', 'explore_featured_posts');

	$lifespan = te_transient_lifespan();
	set_transient('te_explore_nav', $te_explore_nav, $lifespan);

	return $te_explore_nav;
}

/** 
  * EACH MAIN NAV ITEM (OR CPT) 
  * GETS ITS OWN NAV FUNCTION
  * SO THAT IT CAN STORE ITS OWN TRANSIENT
 **/ 
function te_happenings_navigation()
{
	$te_happenings_nav = array();
	$te_happenings_nav = te_main_navigation('happenings', 'happenings_category', 'happenings_featured_posts');

	$lifespan = te_transient_lifespan();
	set_transient('te_happenings_nav', $te_happenings_nav, $lifespan);

	return $te_happenings_nav;
}

/** 
  * EACH MAIN NAV ITEM (OR CPT) 
  * GETS ITS OWN NAV FUNCTION
  * SO THAT IT CAN STORE ITS OWN TRANSIENT
 **/ 
function te_eat_and_drink_navigation()
{
	$te_eat_and_drink_nav = array();
	$te_eat_and_drink_nav = te_main_navigation('eat_and_drink', 'eat_and_drink_category', 'eat_and_drink_featured_posts');

	$lifespan = te_transient_lifespan();
	set_transient('te_eat_and_drink_nav', $te_eat_and_drink_nav, $lifespan);

	return $te_eat_and_drink_nav;
}

/** 
  * EACH MAIN NAV ITEM (OR CPT) 
  * GETS ITS OWN NAV FUNCTION
  * SO THAT IT CAN STORE ITS OWN TRANSIENT
 **/ 
function te_good_to_know_navigation()
{
	$te_good_to_know_nav = array();
	$te_good_to_know_nav = te_main_navigation('good_to_know', 'good_to_know_category', 'good_to_know_featured_posts');

	$lifespan = te_transient_lifespan();
	set_transient('te_good_to_know_nav', $te_good_to_know_nav, $lifespan);

	return $te_good_to_know_nav;
}

/**
 * BUILD A SUB-NAV OF TERMS, POSTS AND FEATURED POSTS
 * 
 * @param string $cpt The custom post type name.
 * @param string $taxonomy  The taxonomy name.
 * @param string $acf_featured   The ACF featured posts field name.
 *
 * @return array of the selected Terms, the latest post that belongs to the term and featured posts
 */
function te_main_navigation($cpt, $taxonomy, $acf_featured)
{
	//--- THIS NAV ARRAY IS BEING STORED IN A TRANSIENT FOR BETTER PERFORMANCE
	$te_nav = array();	


	//--- FIRST, GRAB ALL OF THIS CPT'S TERMS FROM ACF
	$selected_terms = get_field($cpt, 'option');
	$selected_terms_count = (int)count($selected_terms);


	//--- LOOP THROUGH MANY, MANY WP QUERIES
	for ( $i=0; $i<$selected_terms_count; ++$i) {
		//--- TERM ID
		$term_id = $selected_terms[$i];

		//--- TERM OBJECT (NAME, ID, ETC.)
		$term_object = get_term($term_id, $taxonomy );

		//--- TERM LINK
		$term_link = get_term_link($term_id, $taxonomy);		

		

		//--- QUERY PARAMS GENERATED FROM CALDERA EASY QUERIES
		$params = array (
			'cache_results' => true,
			'update_post_meta_cache' => true,
			'update_post_term_cache' => true,
			'post_status' => 'publish',					
			'post_type' => 
				array (
					0 => $cpt,
				),
			'ignore_sticky_posts' => true,
			'posts_per_page' => 1,
			'tax_query' => 
				array (
					0 => 
						array (
							'taxonomy' => $taxonomy,
							'field' => 'term_id',
							'terms' => 
								array (
									0 => $term_id,
								),
								'operator' => 'IN',
						),
				),
				'orderby' => 
					array (
						'date' => 'ASC',
					),
		);//$params

		//--- QUERY FOR A POST IN THAT TERM
		$query = new WP_Query( $params );
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				//--- BEGIN THE ARRAY WITH THE TERM NAV ITEM
		$te_nav[$taxonomy]['nav-items'][$i]['term']['term_id'] = $term_object->term_id;
		$te_nav[$taxonomy]['nav-items'][$i]['term']['term_name'] = $term_object->name;
		$te_nav[$taxonomy]['nav-items'][$i]['term']['term_link'] = $term_link;
		
				//--- ADD THAT POST TO THE ARRAY UNDER THE 'POST' KEY
				$te_nav[$taxonomy]['nav-items'][$i]['post']['post_id'] = get_the_ID();	
				$te_nav[$taxonomy]['nav-items'][$i]['post']['post_title'] = get_the_title();
				$te_nav[$taxonomy]['nav-items'][$i]['post']['post_link'] = get_permalink();
			}
		}
		wp_reset_postdata();
	}//--- END SELECTED TERMS POSTS



	//--- SECOND, GRAB THE FEATURED POSTS FOR THIS CPT FROM ACF
	$featured = get_field($acf_featured, 'option');
	$featured_count = (int)count($featured);

	//--- LOOP THROUGH THE FEATURED POSTS FROM ACF
	for ( $i=0; $i<$featured_count; ++$i) {
		$post = $featured[$i];		
		$thumbnail = null;		

		//--- ADD THE POST (SHOULD ONLY BE 2) TO THE ARRAY UNDER A NEW KEY
		$te_nav[$taxonomy]['featured-items'][$i]['post_id'] = $post->ID;
		$te_nav[$taxonomy]['featured-items'][$i]['post_title'] = $post->post_title;
		$te_nav[$taxonomy]['featured-items'][$i]['post_link'] = get_permalink($post->ID);
		$te_nav[$taxonomy]['featured-items'][$i]['post_author'] = $post->post_author;
		$te_nav[$taxonomy]['featured-items'][$i]['post_date'] = $post->post_date;

		//--- THE POST THUMBNAIL
		if ( has_post_thumbnail( $post->ID ) ) {
            $te_nav[$taxonomy]['featured-items'][$i]['post_thumbnail'] =  get_the_post_thumbnail( $post->ID, 'featured-thumb' );
        }
	}//--- END ACF FEATURED POSTS
	//--- END SINGLE CPT NAV

	//--- ADD THIS NAV ARRAY TO A TRANSIENT
	// $lifespan = te_transient_lifespan();
	// set_transient('te_nav', $te_nav, $lifespan);
	
	//--- THE HEADER FILE DOES ALL THE TRANSIET CHECKING
	return $te_nav;
}