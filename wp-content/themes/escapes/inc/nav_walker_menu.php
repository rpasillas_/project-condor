<?php
class nav_walker_menu extends Walker_Nav_menu {
    /**
     * Provide the opening markup for a new menu within our menu (AKA a submenu).
     * 
     * @param string $output Passed by reference. @see start_el().
     * @param int    $depth  Depth of menu item. @see start_el().
     * @param array  $args   An array of arguments. @see start_el().
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
            );
        $class_names = implode( ' ', $classes );
      
        // build html
        $output .= "\n" . $indent . '<span class="sub-trigger"><span class="trigger-icon">+</span></span><ul class="' . $class_names . '">' . "\n";
    }
      
    /**
     * Append the opening html for a nav menu item, and the menu item itself.
     *
     * @param string $output Passed by reference. The output for all of the preceding menu items.
     * @param object $item   The Post object for this menu item.
     * @param int    $depth  The number of levels deep we are in submenu-land.
     * @param array  $args   An array of arguments for wp_nav_menu().
     * @param int    $id     Allegedly the current item ID -- seems to always just be 0.
     */
     function start_el(  &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
      
        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
      
        // passed classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
      

        if( $depth == 1 ){
            // build html
            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">' . $this->get_thumb($item->object_id);

            //this one here
            //$this->get_thumb($item->object_id)

        }else{
            // build html
            $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
        }
              
        // link attributes
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
      
        $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
            $args->before,
            $attributes,
            $args->link_before,
            apply_filters( 'the_title', $item->title, $item->ID ),
            $args->link_after,
            $args->after
        );
      
        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

       //var_dump($item);
    }

    /**
     * Grab the post thumbnail and append to menu
     *
     * @param int    $id     Page ID of the current item.
     */
    function get_thumb($id){
        // $thumbnail = null;

        // if( has_post_thumbnail( $id ) && get_field( 'display_featured_image_in_menu', $id ) ){
        //     $thumbnail =  get_the_post_thumbnail( $id, 'thumbnail' );
        // }

        // return $thumbnail;
    }
}
?>