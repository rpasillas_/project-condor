<?php
/**
 * Custom functions that act independently of the theme templates. 
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Temecula Escapes
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function escapes_body_classes( $classes ) {

	global $is_IE;

	// If it's IE, add a class.
	if ( $is_IE ) {
		$classes[] = 'ie';
	}

	// Give all pages a unique class.
	if ( is_page() ) {
		$classes[] = 'page-' . basename( get_permalink() );
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds "no-js" class. If JS is enabled, this will be replaced (by javascript) to "js".
	$classes[] = 'no-js';

	return $classes;
}
add_filter( 'body_class', 'escapes_body_classes' );


/**
 * Displays a navigation list of social media accounts.
 *
 * @param string $msg Message that displays with the icons.
 * @return string
 */
function social_media_nav( $msg = null ){
	$socials = array('facebook', 'instagram', 'twitter');
	$string = null;

	foreach($socials as $social){
		if( get_field($social, 'options') !== '' ){
			$te_social = 'te_facebook_profile';

			if( $social == 'instagram' ){
				$te_social = 'te_instagram_profile';
			}else if( $social == 'twitter' ){
				$te_social = 'te_twitter_profile';
			}

			$string .= '<li class="ssm-item"><a href="' . get_field($te_social, 'options') . '" target="_blank"><svg><use xlink:href="#icon-' . $social . '-round" class="st0"></use></svg></a></li>';
		}		
	}

	if( $string ){
		$string = '<div class="ssm-wrapper"><span class="msg">' . $msg . '</span><nav class="ssm-navigation"><ul class="ssm-menu">' . $string . '</ul></nav></div>';
	}

	return $string;
}


/**
 * Displays related posts
 *
 * @param string $post_type Custom Post Type name
 * @return array of posts
 */
function te_featured_articles( $post_type ){

	$args = array(
			'posts_per_page'	=> 4,
			'post_status'		=> 'publish',
			'orderby'			=> 'date',
			'order'				=> 'ASC',
			'post_type'			=> $post_type ,
		//	'meta_value'		=> ''
		);

	$posts_array = get_posts( $args );

	return $posts_array;
}

/**
 * Displays the required HTML for the Facebook comments plugin
 *
 * @link  https://developers.facebook.com/docs/plugins/comments/
 * @return string
 */
function fb_comments_tag(){
	global $post;

	$fb_string = '<section class="comments-fb">';
	$fb_string .= '<header><h2>Comments</h2></header>';
	$fb_string .= '<div class="fb-comments" data-href="' . get_permalink( $post->ID ) . '" data-width="100%" data-numposts="5"></div>';
	$fb_string .= '</section>';

	return $fb_string;
}