<?php

/**
 * Make a Tile Item for the Grid Layouts
 * Used for 75% of the Tiles seen on the website. 
 *
 * @author Ron Pasillas
 * 
 */

class Grid {

	/**
	 * GET THE POST'S CATEGORIES
	 *  
	 * @param INT $id Post Id
	 *
	 * @return string the post's categories
	 */
	public function gridCategory( $id )
	{
		//IF ERRORS OCCUR, CHECK TO MAKE SURE A POST HAS CATEGORIES ASSIGNED TO IT
		$cpt_category_terms = wp_get_post_terms( $id, get_post_type($id) . '_category' );

		$cat = '';

		if( !empty($cpt_category_terms) ){
			$cat = $cpt_category_terms[0]->slug;
		}

		return $cat;
	}

	/**
	 * GET THE POST'S PROFILE LEVEL (Standard, Premium, Deluxe)
	 *  
	 * @param INT $id Post Id
	 *
	 * @return string the post's profile level
	 */
	public function gridProfileLevel( $id )
	{
		$cpt_profile_terms = wp_get_post_terms($id, get_post_type($id) . '_profile');	

		if( is_array($cpt_profile_terms) ){
			$profile_level = $cpt_profile_terms[0]->slug;			
		}else if( get_field('size_of_tile') ){
			$profile_level = get_field('size_of_tile');
		}else{
			$profile_level = 'default';
		}

		//Upgraded Profiles have 2 different size orientations
		//Ones with Odd number IDs will be portrait
		//Ones with Even number IDs will be landscape
		if( $profile_level == 'upgraded' && ($id % 2)==1){
			$profile_level = 'upgraded-alt';
		}

		return $profile_level;
	}


	/**
	 * GET THE POST'S THUMBNAIL
	 *  
	 * @param INT $id Post Id
	 *
	 * @return string the post thumbnail
	 */
	public function gridImage( $id )
	{

		$profile_level = $this->gridProfileLevel( $id );

		if( has_post_thumbnail( $id ) ){		

			if( $profile_level == 'premium' )
			{
				$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'tile-large' );
			}else if( $profile_level == 'upgraded-alt' )
			{	
				$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'tile-medium-alt' );					
			}else if( $profile_level == 'upgraded' )
			{		
				$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'tile-medium' );				
			}else
			{
				$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'tile-small' );
			}

			$post_thumb = $post_thumb[0];
		}else{
			$post_thumb = 'none';
		}

		return $post_thumb;
	}




	public function getYoastMetas( $id )
	{
		$all_metas = get_post_meta( $id );

		if( isset($all_metas['_yoast_wpseo_twitter-description']) ){
			$metas['twitter_share_desc'] = $all_metas['_yoast_wpseo_twitter-description'][0];
		}else{			
			$metas['twitter_share_desc'] = $all_metas['_yoast_wpseo_metadesc'][0]; 
		}
		 
		if( isset($all_metas['_yoast_wpseo_opengraph-description']) ){
			$metas['facebook_share_desc'] = $all_metas['_yoast_wpseo_opengraph-description'][0];
		}else{
			$metas['facebook_share_desc'] = $all_metas['_yoast_wpseo_metadesc'][0];
		}	

		if( $all_metas['_yoast_wpseo_metadesc'] ){
			$metas['email_share_desc'] = $all_metas['_yoast_wpseo_metadesc'][0];
		}

		return $metas;
	}



	/**
	 * CREATE A GRID ITEM
	 *  
	 * @param string $cat category for the tile (family friendly, full bar, etc.)
	 *
	 * @param int $post_id the post ID
	 *
	 * @param string $post_title the post title
	 *
	 * @param string $post_name the hyphanated title
	 *
	 * @param string $post_thumb the post thumbnail
	 *
	 * @param string $profile_level the profile level, defaults
	 *
	 * @return string of a tile item
	 *
	 * @package Temecula Escapes
	 */
	public function gridItem($cat, $post_id, $post_title, $post_name, $post_thumb, $profile_level='default')
	{

		$metas = $this->getYoastMetas( $post_id );

		if( $metas['twitter_share_desc'] ){
			$twMeta = $metas['twitter_share_desc'];
		}else{
			$twMeta = $post_title;
		}

		if( $metas['email_share_desc'] ){
			$emailMeta = $metas['email_share_desc'];
		}else{
			$emailMeta = $post_title;
		}
		

		$item =  '<div class="masonry-grid-item '. $cat . ' ' . $profile_level . '" id="'. $post_name . '" style="background-image: url('. $post_thumb . ');">' . "\n";
		$item .= "\t" . '<div class="grid-inner">' . "\n";
		$item .= "\t\t" . '<a href="' . get_permalink($post_id) . '" class="title">'. $post_title . '</a>' . "\n";

		$item .= "\t" . '<div class="grid-item-share-icons">' . "\n";

		$item .= "\t\t\t" . ' <span class="grid-item-share-text">Share This</span> ' . "\n";

		//facebook icon
		$item .= "\t\t\t" . '<a href="' . esc_url( 'https://www.facebook.com/sharer/sharer.php?u=' . rawurlencode ( get_permalink($post_id) ) ) . '" class="hover-icon fb-hover-icon" title="Share this greatness on Facebook" onclick="window.open(this.href, \'targetWindow\', \'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300\' ); return false;"><svg><use xlink:href="#icon-facebook-f"></use></svg></a>' . "\n";

		//twitter icon
		$item .= "\t\t\t" . '<a href="' . esc_url( 'https://twitter.com/intent/tweet?text=' . urlencode( html_entity_decode( $twMeta ) ) . '&amp;url=' . rawurlencode ( get_permalink($post_id) ) ) . '" class="hover-icon tw-hover-icon" title="Tweet this awesomeness" onclick="window.open(this.href, \'targetWindow\', \'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300\' ); return false;"><svg><use xlink:href="#icon-twitter-bird"></use></svg></a>' . "\n";

		//email icon
		$item .= "\t\t\t" . '<a href="mailto:?subject=' . html_entity_decode( $post_title ) . '&body=' . $emailMeta . '. ' . get_permalink($post_id) . '" class="hover-icon email-hover-icon" title="Email this to your peeps"><svg><use xlink:href="#icon-airplane"></use></svg></a>' . "\n";

		//favorites icon
		$item .= "\t\t" . '<a href="#" class="hover-icon fav-hover-icon js-favorites" title="Add to your favorites list"><svg><use xlink:href="#icon-heart"></use></svg></a>' . "\n";

		$item .= "\t" . '</div>' . "\n";

		$item .= "\t" . '<a href="' . get_permalink($post_id) . '" class="grid-item-read-more"><svg><use xlink:href="#icon-magnifying-plus"></use></svg> Read More</a>' . "\n";
		
		$item .= "\t" . '</div>' . "\n";
		$item .= "\t" . '<div class="masonry-grid-item-overlay"></div>' . "\n";
		$item .= '</div>' . "\n";

		return $item;
	}
}