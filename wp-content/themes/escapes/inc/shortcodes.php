<?php
/**
 * A SHORTCODE USED BY THE CALDERA CLARITY PLUGIN
 * TO GENERATE A GRID ITEM
 *
 **/
function escapes_grid_tile_shortcode($atts, $content = null){
	extract( shortcode_atts(
		array(
			'cpt' => 'winery',
		),
		$atts ) );


	$cpt_category_terms = wp_get_post_terms(get_the_ID(), get_post_type() . '_category');
	$cpt_profile_terms = wp_get_post_terms(get_the_ID(), get_post_type() . '_profile');

	if( !empty($cpt_category_terms) ){
		$cat = $cpt_category_terms[0]->slug;
	}else{
		$cat = '';
	}


	if( !empty($cpt_profile_terms) ){
		$profile_level = $cpt_profile_terms[0]->slug;
	}else{
		$profile_level = 'standard';
	}

	//IF THERES NO THUMBNAIL
	//SHOW A DEFAULT
	if( has_post_thumbnail(get_the_ID()) ){
		//CHECK THE LEVEL OF PROFILE TO GRAB THE 
		//CORRECT SIZED THUMBNAIL FOR THE GRID
		if( $profile_level == 'premium' )
		{
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'tile-large' );
		}else if( $profile_level == 'upgraded' )
		{
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'tile-medium' );
		}else
		{
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'tile-small' );
			$profile_level == 'standard';
		}


		$post_thumb = $post_thumb[0];
	}else{
		$post_thumb = 'none';
	}


	echo '<div class="grid-sizer"></div>';
	echo '<div class="masonry-grid-item '. $cat . ' ' .$profile_level . '" id="'. get_the_title() . '" style="background-image: url('. $post_thumb . ');">
			
			<a href="' . get_permalink( get_the_ID() ) . '">'. get_the_title() . '</a>
		</div><!-- item -->';		
}
//add_shortcode( 'escapes-grid', 'escapes_grid_tile_shortcode' );

/**
 * A SHORTCODE USED BY THE CALDERA CLARITY PLUGIN
 * TO GENERATE A GRID ITEM (generates all small grid items)
 *
 **/
function escapes_grid_tile_no_sizing_shortcode($atts, $content = null){
	extract( shortcode_atts(
		array(
			'cpt' => 'explore',
		),
		$atts ) );

	global $post;

	$grid_item = new Grid;

	//GET THE CATEGORY
	$grid_category = $grid_item->gridCategory( $post->ID );

	//GET THE PROFILE LEVEL
	$grid_profile  = $grid_item -> gridProfileLevel( $post->ID );

	//GET THE THUMBNAIL
	$grid_image = $grid_item->gridImage( $post->ID );

	//CREATE THE GRID
	$grid = $grid_item->gridItem( $grid_category, $post->ID , get_the_title(), $post->post_name, $grid_image, $grid_profile );
	
	echo '<div class="grid-sizer"></div>';
	echo $grid;
}
add_shortcode( 'escapes-grid-alt', 'escapes_grid_tile_no_sizing_shortcode' );
add_shortcode( 'escapes-grid', 'escapes_grid_tile_no_sizing_shortcode' );

/**
 * SHORTCODE SHOWS FACEBOOK & INSTAGRAM PHOTO WIDGETS
 * SIDE-BY-SIDE
 *
 **/
function escapes_do_ssm_widgets($atts, $content = null){
	$data = '';
	ob_start();	
	get_template_part('template-parts/ssm', 'widgets');
	$data = ob_get_contents();
	ob_end_clean();
	return $data;
}
add_shortcode( 'ssm-widgets', 'escapes_do_ssm_widgets' );


/**
 * SHORTCODE SHOWS A MEDIA KIT DOWNLOAD BUTTON WITH TEXT
 * 
 **/
function escapes_do_media_kit_cta($atts, $content = null){
	extract( shortcode_atts(
		array(
			'arrow' => '',
		),
		$atts ) );

	
	$data = '';
	ob_start();	

	$show_arrow = '';
	if( $arrow ){
		$show_arrow = ' show-arrow';
	}

	$data .= '<div class="media-kit-wrap"><span class="h1 new-shine' . $show_arrow . '">' . $content . '</span>
	<a href="' . get_field('media_kit', 'options') . '" class="media-btn btn-solid btn-purple"><svg><use xlink:href="#icon-newspaper"></use></svg> Download Media Kit</a>';
 
	if( $arrow ){
		$data .= '<svg class="fancy-arrow"><use xlink:href="#icon-down-curve-arrow"></use></svg>';
	}

	$data .= '</div>';

	$data .= ob_get_contents();

	ob_end_clean();
	return $data;
}
add_shortcode( 'media-kit', 'escapes_do_media_kit_cta' );


/**
 * SHORTCODE SHOWS A TE SOCIAL WIDGET
 * 
 **/
function escapes_do_ssm_accounts($atts, $content = null){
	echo '<div class="te-socials">
		<span class="h1">Let\'s get social</span>'
		. social_media_nav() .
		'@' . get_field('instagram_handle', 'option') .
	'</div>';
}
add_shortcode( 'te-ssm', 'escapes_do_ssm_accounts' );



function escapes_do_ssm_accounts_from_editor($atts, $content = null){
	$data = '';
	ob_start();	
	do_shortcode('[te-ssm]');
	$data = ob_get_contents();
	ob_end_clean();
	return $data;
}
add_shortcode( 'te-do-ssm', 'escapes_do_ssm_accounts_from_editor' );