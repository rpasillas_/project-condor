<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Temecula Escapes
 */

if ( ! function_exists( 'escapes_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function escapes_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'escapes' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'escapes' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'escapes_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function escapes_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'escapes' ) );
		if ( $categories_list && escapes_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'escapes' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'escapes' ) );
		if ( $tags_list ) {
						printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'escapes' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( esc_html__( 'Leave a comment', 'escapes' ), esc_html__( '1 Comment', 'escapes' ), esc_html__( '% Comments', 'escapes' ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'escapes' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function escapes_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'escapes_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'escapes_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so escapes_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so escapes_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in escapes_categorized_blog.
 */
function escapes_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return false;
	}
	// Like, beat it. Dig?
	delete_transient( 'escapes_categories' );
}
add_action( 'delete_category', 'escapes_category_transient_flusher' );
add_action( 'save_post',     'escapes_category_transient_flusher' );

/**
 * Return SVG markup.
 *
 * @param  array  $args {
 *     Parameters needed to display an SVG.
 *
 *     @param string $icon Required. Use the icon filename, e.g. "facebook-square".
 *     @param string $title Optional. SVG title, e.g. "Facebook".
 *     @param string $desc Optional. SVG description, e.g. "Share this post on Facebook".
 * }
 * @return string SVG markup.
 */
function escapes_get_svg( $args = array() ) {

	// Make sure $args are an array.
	if ( empty( $args ) ) {
		return esc_html__( 'Please define default parameters in the form of an array.', 'escapes' );
	}

	// YUNO define an icon?
	if ( false === array_key_exists( 'icon', $args ) ) {
		return esc_html__( 'Please define an SVG icon filename.', 'escapes' );
	}

	// Set defaults.
	$defaults = array(
		'icon'  => '',
		'title' => '',
		'desc'  => ''
	);

	// Parse args.
	$args = wp_parse_args( $args, $defaults );

	// Begin SVG markup
	$svg = '<svg class="icon icon-' . esc_html( $args['icon'] ) . '" aria-hidden="true">';

		// If there is a title, display it.
		if ( $args['title'] ) {
			$svg .= '<title>' . esc_html( $args['title'] ) . '</title>';
		}

		// If there is a description, display it.
		if ( $args['desc'] ) {
			$svg .= '<desc>' . esc_html( $args['desc'] ) . '</desc>';
		}

	$svg .= '<use xlink:href="#icon-' . esc_html( $args['icon'] ) . '"></use>';
	$svg .= '</svg>';

	return $svg;
}

/**
 * Display an SVG.
 *
 * @param  array  $args  Parameters needed to display an SVG.
 */
function escapes_do_svg( $args = array() ) {
	echo escapes_get_svg( $args );
}

/**
 * Trim the title legnth.
 *
 * @param  array  $args  Parameters include length and more.
 * @return string        The shortened excerpt.
 */
function escapes_get_the_title( $args = array() ) {

	// Set defaults.
	$defaults = array(
		'length'  => 12,
		'more'    => '...'
	);

	// Parse args.
	$args = wp_parse_args( $args, $defaults );

	// Trim the title.
	return wp_trim_words( get_the_title( get_the_ID() ), $args['length'], $args['more'] );
}

/**
 * Limit the excerpt length.
 *
 * @param  array  $args  Parameters include length and more.
 * @return string        The shortened excerpt.
 */
function escapes_get_the_excerpt( $args = array() ) {

	// Set defaults.
	$defaults = array(
		'length' => 20,
		'more'   => '...'
	);

	// Parse args.
	$args = wp_parse_args( $args, $defaults );

	// Trim the excerpt.
	return wp_trim_words( get_the_excerpt(), absint( $args['length'] ), esc_html( $args['more'] ) );
}

/**
 * Echo an image, no matter what.
 *
 * @param string  $size  The image size you want to display.
 */
function escapes_do_post_image( $size = 'thumbnail' ) {

	// If featured image is present, use that.
	if ( has_post_thumbnail() ) {
		return the_post_thumbnail( $size );
	}

	// Check for any attached image
	$media = get_attached_media( 'image', get_the_ID() );
	$media = current( $media );

	// Set up default image path.
	$media_url = get_stylesheet_directory_uri() . '/assets/images/placeholder.png';

	// If an image is present, then use it.
	if ( is_array( $media ) && 0 < count( $media ) ) {
		$media_url = ( 'thumbnail' === $size ) ? wp_get_attachment_thumb_url( $media->ID ) : wp_get_attachment_url( $media->ID );
	}

	echo '<img src="' . esc_url( $media_url ) . '" class="attachment-thumbnail wp-post-image" alt="' . esc_html( get_the_title() )  . '" />';
}

/**
 * Return an image URI, no matter what.
 *
 * @param  string  $size  The image size you want to return.
 * @return string         The image URI.
 */
function escapes_get_post_image_uri( $size = 'thumbnail' ) {

	// If featured image is present, use that.
	if ( has_post_thumbnail() ) {

		$featured_image_id = get_post_thumbnail_id( get_the_ID() );
		$media = wp_get_attachment_image_src( $featured_image_id, $size );

		if ( is_array( $media ) ) {
			return current( $media );
		}
	}

	// Check for any attached image.
	$media = get_attached_media( 'image', get_the_ID() );
	$media = current( $media );

	// Set up default image path.
	$media_url = get_stylesheet_directory_uri() . '/assets/images/placeholder.png';

	// If an image is present, then use it.
	if ( is_array( $media ) && 0 < count( $media ) ) {
		$media_url = ( 'thumbnail' === $size ) ? wp_get_attachment_thumb_url( $media->ID ) : wp_get_attachment_url( $media->ID );
	}

	return $media_url;
}

/**
 * Get an attachment ID from it's URL.
 *
 * @param  string  $attachment_url  The URL of the attachment.
 * @return int                      The attachment ID.
 */
function escapes_get_attachment_id_from_url( $attachment_url = '' ) {

	global $wpdb;

	$attachment_id = false;

	// If there is no url, return.
	if ( '' == $attachment_url ) {
		return false;
	}

	// Get the upload directory paths.
	$upload_dir_paths = wp_upload_dir();

	// Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image.
	if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

		// If this is the URL of an auto-generated thumbnail, get the URL of the original image.
		$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

		// Remove the upload path base directory from the attachment URL.
		$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

		// Finally, run a custom database query to get the attachment ID from the modified attachment URL.
		$attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );

	}

	return $attachment_id;
}

/**
 * Echo the copyright text saved in the Customizer.
 */
function escapes_do_copyright_text() {

	// Grab our customizer settings.
	$copyright_text = get_theme_mod( 'escapes_copyright_text' );

	// Stop if there's nothing to display.
	if ( ! $copyright_text ) {
		return false;
	}

	// Echo the text.
	echo '<span class="copyright-text">' . wp_kses_post( $copyright_text ) . '</span>';
}

/**
 * Build social sharing icons.
 *
 * @return string
 */
function escapes_get_social_share() {

	// Build the sharing URLs.
	$twitter_url  = 'https://twitter.com/share?text=' . urlencode( html_entity_decode( get_the_title() ) ) . '&amp;url=' . rawurlencode ( get_the_permalink() );
	$facebook_url = 'https://www.facebook.com/sharer/sharer.php?u=' . rawurlencode ( get_the_permalink() );
	$linkedin_url = 'https://www.linkedin.com/shareArticle?title=' . urlencode( html_entity_decode( get_the_title() ) ) . '&amp;url=' . rawurlencode ( get_the_permalink() );

	// Start the markup.
	ob_start(); ?>
	<div class="social-share">
		<h5 class="social-share-title"><?php esc_html_e( 'Share This', 'escapes' ); ?></h5>
		<ul class="social-icons menu menu-horizontal">
			<li class="social-icon">
				<a href="<?php echo esc_url( $twitter_url ); ?>" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;">
					<?php escapes_do_svg( array( 'icon' => 'twitter-square', 'title' => 'Twitter', 'desc' => __( 'Share on Twitter', 'escapes' ) ) ); ?>
					<span class="screen-reader-text"><?php esc_html_e( 'Share on Twitter', 'escapes' ); ?></span>
				</a>
			</li>
			<li class="social-icon">
				<a href="<?php echo esc_url( $facebook_url ); ?>" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;">
					<?php escapes_do_svg( array( 'icon' => 'facebook-square', 'title' => 'Facebook', 'desc' => __( 'Share on Facebook', 'escapes' ) ) ); ?>
					<span class="screen-reader-text"><?php esc_html_e( 'Share on Facebook', 'escapes' ); ?></span>
				</a>
			</li>
			<li class="social-icon">
				<a href="<?php echo esc_url( $linkedin_url ); ?>" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=475, height=505' ); return false;">
					<?php escapes_do_svg( array( 'icon' => 'linkedin-square', 'title' => 'LinkedIn', 'desc' => __( 'Share on LinkedIn', 'escapes' ) ) ); ?>
					<span class="screen-reader-text"><?php esc_html_e( 'Share on LinkedIn', 'escapes' ); ?></span>
				</a>
			</li>
		</ul>
	</div><!-- .social-share -->

	<?php
	return ob_get_clean();
 }

/**
 * Echo social sharing icons.
 */
function escapes_do_social_share() {
	echo escapes_get_social_share();
}

/**
 * Output the mobile navigation
 */
function escapes_do_mobile_navigation_menu() {

	// Figure out which menu we're pulling
	$mobile_menu = has_nav_menu( 'mobile' ) ? 'mobile' : 'primary';
?>
	<nav id="mobile-menu" class="mobile-nav-menu">
		<button class="close-mobile-menu"><span class="screen-reader-text"><?php _e( 'Close menu', 'escapes' ); ?></span><?php escapes_do_svg( array( 'icon' => 'close' ) ); ?></button>
		<?php
			wp_nav_menu( array(
				'theme_location' => $mobile_menu,
				'menu_id'        => 'primary-menu',
				'menu_class'     => 'menu dropdown mobile-nav',
				'link_before'    => '<span>',
				'link_after'     => '</span>'
			) );
		?>
	</nav>
<?php
}



/**
 * Output the 4 icon menu items related to a
 * happening or an exploration
 */
function escapes_do_post_action_menu(){
?>	
	<nav class="menu post-actions-menu">
		<ul>
		<?php if( get_field( 'show_add_to_favorites' ) ){ ?> 
			<li class="action-item-fav js-favorites"> 
				<a href="#" onClick="ga('send', 'event', 'Favs', 'add', 'Gallivant');">
					<svg><use xlink:href="#icon-heart"></use></svg>
					<span>Add to Favorites</span>
				</a>
			</li>
		<?php } ?>
		<?php if( get_field( 'show_add_to_itinerary' ) ){ ?> 			
			<li class="action-item-itinerary js-itinerary">
				<a href="#" onClick="ga('send', 'event', 'Itinerary', 'add', 'Gallivant');">
					<svg><use xlink:href="#icon-checkmark"></use></svg>
					<span>Add to Itinerary</span>
				</a>
			</li>
		<?php } ?>
		<?php if( get_field( 'contact_email' ) ){ ?> 
			<li class="action-item-message">
				<a href="mailto:<?php the_field( 'contact_email' ); ?>" onClick="ga('send', 'event', 'Send Message', 'send', 'Website');">
					<svg><use xlink:href="#icon-airplane"></use></svg>
					<span>Send Message</span>
				</a>
			</li>
		<?php } ?>
		<?php if( get_field( 'google_maps_address' ) ){ ?> 
			<li class="action-item-directions">
				<a href="<?php the_field( 'google_maps_address' ); ?>" target="_blank" onClick="ga('send', 'event', 'Directions', 'show', 'Website');">
					<svg><use xlink:href="#icon-right-turn-symbol"></use></svg>
					<span>Get Directions</span>
				</a>
			</li>
		<?php } ?>
		</ul>
	</nav>

<?php 	
}

/**
 * Output the local favorites grid tiles
 *
 * @param string|array $cpt custom post type name (winery, brewery, event, or tour)
 *
 * @return string with query results of posts that have an ACF
 * meta value of 1 for meta key 'views'
 *
 * @package Temecula Escapes
 */

function escapes_do_local_favorites( $cpt ){
?>	
	<div class="wrap local-favorites">
		<div class="heading">
			<span class="new-shine">Local</span> <span class="ostrich">Favorites</span>
		</div>	
		<div class="local-favorites-tiles masonry-tiles">	

			<div class="grid-sizer"></div>		

		<?php
			$favs_args = array(
					'numberposts' => -1,
					'post_type'	=> $cpt,
					'meta_key' => 'designate_as_local_favorite',
					'meta_value' => 1,			
					'post_status' => 'publish'
				);			

			$fav_query = new WP_Query($favs_args);

			$posts = $fav_query->get_posts();

			shuffle($posts);
			
			foreach($posts as $post){

				$grid_item = new Grid;

				//GET THE CATEGORY
				$grid_category  = $grid_item -> gridCategory( $post->ID );

				//GET THE PROFILE LEVEL
				$grid_profile  = $grid_item -> gridProfileLevel( $post->ID );

				//GET THE THUMBNAIL
				$grid_image = $grid_item -> gridImage( $post->ID );

				//CREATE THE GRID
				$grid = $grid_item->gridItem( $grid_category, $post->ID, $post->post_title, $post->post_name, $grid_image, $grid_profile );

				echo $grid;
			}

			wp_reset_query();							
		?>


		</div>
	</div>
<?php	
}//escapes_do_local_favorites



/**
 * Output the escapes choice grid tiles
 * Relies on the PostViews plugin to determine view count
 *
 * @param string|array $cpt custom post type name (winery, brewery, event, or tour)
 *
 * @return string with query results of posts that have an ACF
 * meta value of 1 for meta key 'designate_as_e_choice'
 *
 * @package Temecula Escapes
 */
function escapes_do_escapes_choice( $cpt ){
?>	
	<div class="wrap escapes-choice">
		<div class="heading">
			<span class="new-shine">Escapes</span> <span class="ostrich">Choice</span>
		</div>	
		<div class="escapes-choice-tiles masonry-tiles">			
		
			<div class="grid-sizer"></div>


		<?php
			$favs_args = array(
					'numberposts' => -1,
					'post_type'	=> $cpt,
					'meta_key' => 'designate_as_e_choice',
					'meta_value' => 1,
					'post_status' => 'publish'
				);

			$fav_query = new WP_Query($favs_args);

			$posts = $fav_query->get_posts();

			shuffle($posts);
			
			foreach($posts as $post){

				$grid_item = new Grid;

				//GET THE CATEGORY
				$grid_category  = $grid_item -> gridCategory( $post->ID );

				//GET THE PROFILE LEVEL
				$grid_profile  = $grid_item -> gridProfileLevel( $post->ID );

				//GET THE THUMBNAIL
				$grid_image = $grid_item -> gridImage( $post->ID );

				//CREATE THE GRID
				$grid = $grid_item->gridItem( $grid_category, $post->ID, $post->post_title, $post->post_name, $grid_image, $grid_profile );

				echo $grid;
			}
			wp_reset_query();
				
			
			?>


		</div>
	</div>
<?php	
}//escapes_do_escapes_choice


/**
 * Out a featured article item
 *
 * @param $post object 
 *
 * @param $author_id int
 *
 * @return string of html containing a post article
 */
function escapes_do_featured_articles( $post, $author_id ){
	$article = '<article>' . "\n";

	if ( has_post_thumbnail(  ) )
	{
		$article .= '<a href="' . get_permalink() .'" class="post-thumb">' . get_the_post_thumbnail($post->ID, 'featured-thumb')  . '</a>' . "\n";
	} else
	{
		$article .= '<a href="' . get_permalink() .'" class="post-thumb-default"><svg><use xlink:href="#icon-te-initials"></use></svg></a>' . "\n";
	}

	$article .= '<a href="' . get_permalink() .'" class="post-title"><strong>' . get_the_title()  . '</strong></a><br>' . "\n";

	$article .= '<a href="' . esc_url( get_author_posts_url( $author_id) ) . '" class="post_author">By ' . get_the_author_meta( 'first_name', $author_id ) . ' ' . get_the_author_meta( 'last_name', $author_id ) . '</a><br>' . "\n";
	
	$article .= '<span class="post-date">';
	$article .= get_the_date();
	$article .= '</span><br>' . "\n";

	$article .= '</article>' . "\n";

	return $article;
}