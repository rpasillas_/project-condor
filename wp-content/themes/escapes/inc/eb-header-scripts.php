<?php
/**
 * Returns the ID of the top root ancestor of a page.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.1
 *
 * @return string of the data entered into the theme option.
 */
function eb_get_ga_analytics(){
	$option = (of_get_option('ga_analytics')) ? of_get_option('ga_analytics'): null;
	if($option==null){
		return;
	}	
	$data = '<script type="text/javascript">';
	$data .= $option;
	$data .= '</script>';	
	return $data;
}
/**
 * Returns the ID of the top root ancestor of a page.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.1
 *
 * @return string of the data entered into the theme option.
 */
function eb_get_g_remarketing(){
	$option = (of_get_option('g_remarketing')) ? of_get_option('g_remarketing'): null;
	if($option==null){
		return;
	}	
	$data = '<script type="text/javascript">';
	$data .= $option;
	$data .= '</script>';
	return $data;
}
/**
 * Returns the ID of the top root ancestor of a page.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.1
 *
 * @return string of the data entered into the theme option.
 */
function eb_get_call_tracking(){
	$option = (of_get_option('call_tracking')) ? of_get_option('call_tracking'): null;
	if($option==null){
		return;
	}	
	$data = '<script  async src="' . $option . '">';
	$data .= '</script>';
	return $data;
}
/**
 * Returns the ID of the top root ancestor of a page.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.1
 *
 * @return string of the data entered into the theme option.
 */
function eb_get_snapengage(){
	$option = (of_get_option('snap_engage')) ? of_get_option('snap_engage'): null;
	if($option==null){
		return;
	}	
	$data = '<script type="text/javascript">';
	$data .= $option;
	$data .= '</script>';
	return $data;
}
/**
 * Returns the ID of the top root ancestor of a page.
 *
 * @package WordPress
 * @subpackage evaero-boilerplate
 * @since Evaero Boilerplate 1.1
 *
 * @return all the above functions and hook them into the Head Hook.
 */
function eb_header_scripts(){
	echo eb_get_ga_analytics();
	echo eb_get_g_remarketing();
	echo eb_get_call_tracking();
	echo eb_get_snapengage();
}
add_action('wp_head', 'eb_header_scripts');