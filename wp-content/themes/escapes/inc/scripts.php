<?php
/**
 * Custom scripts and styles.
 *
 * @package Temecula Escapes
 */

/**
 * Register Google font.
 *
 * @link http://themeshaper.com/2014/08/13/how-to-add-google-fonts-to-wordpress-themes/
 */
function escapes_font_url() {

	$fonts_url = '';

	/**
	 * Translators: If there are characters in your language that are not
	 * supported by the following, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$roboto = _x( 'on', 'Roboto font: on or off', 'escapes' );

	if ( 'off' !== $roboto || 'off' !== $open_sans ) {
		$font_families = array();

		if ( 'off' !== $roboto ) {
			$font_families[] = 'Roboto:100,400,300,700';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
		);

		$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}

/**
 * Enqueue scripts and styles.
 */
function escapes_scripts() {
	/**
	 * If WP is in script debug, or we pass ?script_debug in a URL - set debug to true.
	 */
	$debug = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ) || ( isset( $_GET['script_debug'] ) ) ? true : false;

	/**
	 * If we are debugging the site, use a unique version every page load so as to ensure no cache issues.
	 */
	$version = '1.0.2';

	/**
	 * Should we load minified files?
	 */
	$suffix = ( true === $debug ) ? '' : '.min';

	// Register styles.
	wp_register_style( 'escapes-google-font', escapes_font_url(), array(), null );

	// Enqueue styles.
	wp_enqueue_style( 'escapes-google-font' );
	wp_enqueue_style( 'animate.css' );
	wp_enqueue_style( 'escapes-style', get_stylesheet_directory_uri() . '/style' . $suffix . '.css', array(), $version );

	// Enqueue scripts.
	wp_enqueue_script( 'escapes-scripts', get_template_directory_uri() . '/assets/js/project' . $suffix . '.js', array( 'jquery' ), $version, true );

	wp_enqueue_script( 'mobile-navigation', get_template_directory_uri() . '/assets/js/mobile-nav-menu' . $suffix . '.js', array( 'jquery' ), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Enqueue the mobile nav script
	// Since we're showing/hiding based on CSS and wp_is_mobile is wp_is_imperfect, enqueue this everywhere.
	//wp_enqueue_script( 'escapes-mobile-nav', get_template_directory_uri() . '/assets/js/mobile-nav-menu' . $suffix . '.js', array( 'jquery' ), $version, true );

	//The Final Countdown JS
	if( is_singular( 'happenings' )  ){
		wp_enqueue_script('final-countdown', get_template_directory_uri() . '/assets/bower_components/jquery.countdown/dist/jquery.countdown.min.js', array( 'jquery' ), $version, false);
	}

	//Load the Owl Carousel
	//IF YOU ADJUST THIS CONDITIONAL
	//ALSO ADJUST FUNCTION te_do_owl_carousel ON LINE 164
	if( is_page('experience') || is_post_type_archive( array('winery', 'brewery') ) ){
		wp_enqueue_style( 'owl-carousel-main', get_stylesheet_directory_uri() . '/assets/css/owl.carousel.css', array(), $version );
		wp_enqueue_style( 'owl-carousel-main', get_stylesheet_directory_uri() . '/assets/css/owl.theme.css', array(), $version );
		
		
		wp_enqueue_script('greensock-tweenmax', get_template_directory_uri() . '/assets/js/TweenMax.min.js', array( 'jquery' ), $version, true);
		wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array( 'jquery' ), $version, true);
		wp_enqueue_script('scrollmagic', get_template_directory_uri() . '/assets/bower_components/scrollmagic/scrollmagic/minified/ScrollMagic.min.js', array( 'jquery' ), $version, true);
		wp_enqueue_script('scrollmagic-greensock', get_template_directory_uri() . '/assets/bower_components/scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js', array( 'jquery' ), $version, true);		
	}

	//Load the Masonry
	//IF YOU ADJUST THIS CONDITIONAL
	//ALSO ADJUST FUNCTION te_do_masonry ON LINE 164
	if( is_page('experience') || is_post_type_archive( array('winery', 'brewery', 'explore', 'happenings', 'good_to_know' , 'eat_and_drink') ) || is_tax( array('winery_category', 'brewery_category', 'explore_category', 'happenings_category', 'good_to_know_category', 'eat_and_drink_category') ) || is_front_page() || is_search() ){

		wp_enqueue_script('masonry', get_template_directory_uri() . '/assets/bower_components/masonry/dist/masonry.pkdg.min.js', array( 'jquery' ), $version, true);
	}

	//Load autocomplete
	if( is_post_type_archive('happenings') ){
		wp_enqueue_script('autocomplete-min', get_template_directory_uri() . '/assets/js/jquery.auto-complete.min.js', array( 'jquery' ), $version, true);
		wp_enqueue_script('autocomplete-happenings', get_template_directory_uri() . '/assets/js/jquery.auto-complete-happenings.js', array( 'jquery' ), $version, true);
		wp_enqueue_style('autocomplete-css', get_template_directory_uri() . '/assets/css/jquery.auto-complete.css', $version, true);
	}

	if( is_page_template('tpl-page-mailchimp-raffle.php') ){
		wp_enqueue_script('greensock-timelinemax', get_template_directory_uri() . '/assets/js/TweenMax.min.js', array( 'jquery' ), $version, true);
	}	

}
add_action( 'wp_enqueue_scripts', 'escapes_scripts' );


if ( class_exists( 'WDS_Simple_Page_Builder' ) && version_compare( WDS_Simple_Page_Builder::VERSION, '1.6', '>=' ) ) :

	/**
	 * Conditionally enqueue styles & scripts via Page Builder.
	 */
	function escapes_enqueue_page_builder_scripts() {

		// Get the page builder parts
		$parts = get_page_builder_parts();

		// // If page builder part exsists, enqueue script
		// if ( in_array( 'cover-flow' , $parts ) ) {
		// 	wp_register_script( 'cover-flow', get_stylesheet_directory_uri() . '/js/cover-flow-script.js', array(), $version, true );
		// 	wp_enqueue_script( 'cover-flow' );
		// }

	}
	add_action( 'wds_page_builder_after_load_parts', 'escapes_enqueue_page_builder_scripts' );

endif;

/**
 * Add SVG definitions to <head>.
 */
function escapes_include_svg_icons() {

	// Define SVG sprite file.
	$svg_icons = get_template_directory() . '/assets/images/svg-icons.svg';

	// If it exsists, include it.
	if ( file_exists( $svg_icons ) ) {
		require_once( $svg_icons );
	}
}



//Load FB Comments SDK in the header
function fb_comments_sdk(){
	if( !is_front_page() ){
?>
	<div id="fb-root"></div>
	
	<script>
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
<?php	
	}
}
add_action( 'wp_head', 'fb_comments_sdk' );


//IF YOU ADJUST THIS CONDITIONAL
//ALSO ADJUST FUNCTION te_do_owl_carousel ON LINE 89
function te_do_owl_carousel(){
	if( is_page('experience') || is_post_type_archive( array('winery', 'brewery') ) ){
?>
	<script>
		( function( $ ) {			

		   $(document).ready(function(){

		   		$('.owl-carousel').owlCarousel({
		   			singleItem: true,
		   			slideSpeed: 300,
		   			navigation: true,
		   			paginationSpeed: 400
		   		});

			   	var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});

				var scene = new ScrollMagic.Scene({triggerElement: '.experiences-banners'})
					.setTween( '.parallax-bg', {y:'68%'} )
					.addTo( controller );
											   		
		   });		   

		} )( jQuery );
	</script>
<?php		
	}	
}

add_action( 'wp_footer', 'te_do_owl_carousel' );




//IF YOU ADJUST THIS CONDITIONAL
//ALSO ADJUST FUNCTION te_do_masonry ON LINE 106
function te_do_masonry(){
	if( is_page('experience') || is_post_type_archive( array('winery', 'brewery', 'explore', 'happenings', 'good_to_know' , 'eat_and_drink') ) ||  is_tax( array('winery_category', 'brewery_category', 'explore_category', 'happenings_category', 'good_to_know_category', 'eat_and_drink_category') ) || is_front_page() || is_search() ){
?>
	<script>
		( function( $ ) {			

			

		   	$(document).ready(function(){
		   		var windowW = $(window).width();
		   		var gridColWidth = 128;

		   		if( windowW > 767 ){
		   			gridColWidth = 240;
		   		}
	   			
	   			//Do masonry on grids that ARE
	   			//generated through caldera   						
				var $grid = $('.facetwp-template').masonry({
					itemSelector: '.masonry-grid-item',
					columnWidth: gridColWidth,
					gutter: 10,
					//fitWidth: true
					//percentPosition: true			
				});

				//When the tiles are filtered...
				$(document).on('facetwp-loaded', function() {
					//re-initialize masonry...
					$grid.masonry('reloadItems');
					$grid.masonry('layout');

					//hide the tool tip when the
					hideToolTip( $('.tool-tip') );
					console.log('checkbox clicked.');
				});

				//Do masonry on grids that ARE NOT
				//generated through caldera 
				$('.masonry-tiles').masonry({
					itemSelector: '.masonry-grid-item',
					columnWidth: gridColWidth,
					gutter: 10,
					//fitWidth: true
					//percentPosition: true
				});

				//Add the help button to the filter h3
				$('h3.clarity-label').append('<span class="tool-tip-trigger" title="Select any of the filters below (one or multiple) to tailor the results to your needs.">?</span>');

				//Show the tool tip
				$('.tool-tip-trigger').on('click', function(){
					if( $('.tool-tip').length ){
						hideToolTip( $(this) );
					}else{
						
						$(this).parent().append('<div class="tool-tip bounce"><div class="tool-tip-inner">Select any of the filters below (one or multiple) to tailor the results to your needs.</div></div>');
					
						$('.tool-tip').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e){
							hideToolTip( $(this) );
						});
					}//else					
				});//on click

				//When the filter clear button is clicked
				//hide the tool tip
				$('.facetwp-selections').on('click', function(){
					hideToolTip( $('.tool-tip') );
				})

				//Function that hides tool tip
				function hideToolTip( $el ){
					$el.animate({opacity: 0, top: '-50'}, 500, function(){
						$el.remove();
					});
				}

				$('.masonry-tiles').on('click', '.masonry-grid-item', function(){
					document.location.href = $(this).find('a.title').attr('href');
				});	

				$('.facetwp-template').on('click', '.masonry-grid-item', function(){
					document.location.href = $(this).find('a.title').attr('href');
				});			
					   		
		   });		



		} )( jQuery );
	</script>
<?php		
	}//if
	
}//te_do_masonry

add_action( 'wp_footer', 'te_do_masonry' );




function te_do_happenings_search(){
	if( is_post_type_archive('happenings') ){
		wp_enqueue_script('handlebars', get_template_directory_uri() . '/assets/js/handlebars-v4.0.5.js', array( 'jquery' ), '', true);
		wp_enqueue_script('happenings-search', get_template_directory_uri() . '/assets/js/jquery.happenings-search.js', array( 'jquery' ), '', true);
		wp_enqueue_script('input-mask', get_template_directory_uri() . '/assets/js/jquery.mask.min.js', array( 'jquery' ), '', true);

		wp_enqueue_script('datepicker', get_template_directory_uri() . '/assets/js/jquery.simple-dtpicker.min.js', array( 'jquery' ), '', true);
		wp_enqueue_style('datepicker', get_template_directory_uri() . '/assets/css/jquery.simple-dtpicker.css', '', true);
		
	?>
		<script>
			( function( $ ) {
				$(document).ready(function(){
					$('#happenings-date').mask('0000-00-00');

					$('#happenings-date').appendDtpicker({
						"dateFormat" : "YYYY-MM-DD",
						"dateOnly" : true
					});

				});
			})( jQuery );
		</script>
	<?php
	}	
}

add_action( 'wp_footer', 'te_do_happenings_search' );

//Adds Event Tracking to Caldera Forms
function te_do_google_event_tracking(){
?>
	<script>
		function do_g_et_newsletter( obj ){
			var entry_id = obj.data.cf_id;
		    ga('send', 'event', 'Newsletter', 'signup', 'Website');
		}

		function do_g_et_contact( obj ){
			var entry_id = obj.data.cf_id;
		    ga('send', 'event', 'Contact', 'send email', 'Website');
		}
	</script>
<?php	
}
add_action( 'wp_footer', 'te_do_google_event_tracking', 100 );



function te_do_search_results_links(){
	if( is_search() ){
?>
	<script>
		( function( $ ) {
			$(document).ready(function(){

				if( $('#brewery_results').find('.grid-sizer').length ){
					$('.brewery-results-link').css('display','inline');
				}

				if( $('#winery_results').find('.grid-sizer').length ){
					$('.winery-results-link').css('display','inline');
				}

				if( $('#eat_and_drink_results').find('.grid-sizer').length ){
					$('.eat-and-drink-results-link').css('display','inline');
				}

				if( $('#explore_results').find('.grid-sizer').length ){
					$('.explore-results-link').css('display','inline');
				}

				if( $('#happenings_results').find('.grid-sizer').length ){
					$('.happenings-results-link').css('display','inline');
				}

				$('.result-link').click(function(e){
					e.preventDefault();

					var result_link = $(this).attr('href');
					var result_position = $(result_link).offset().top;
					result_position = result_position - 300;

					$('html, body').animate({ scrollTop: result_position });
				});
			});

		} )( jQuery );
	</script>

<?php
	}
}
add_action( 'wp_footer', 'te_do_search_results_links', 100 );

?>
