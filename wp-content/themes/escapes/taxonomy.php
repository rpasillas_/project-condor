<?php
/**
 * The template for displaying the experience landing page.
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">				
				<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
				?>


				<?php
					if( have_posts() ):
				?>

				<div class="tax-tiles masonry-tiles">
					<div class="grid-sizer"></div>

				<?php
						while ( have_posts() ) : the_post();

							$grid_item = new Grid;

							//GET THE CATEGORY
							$grid_category  = $grid_item -> gridCategory( $post->ID );

							//GET THE PROFILE LEVEL
							$grid_profile  = $grid_item -> gridProfileLevel( $post->ID );

							//GET THE THUMBNAIL
							$grid_image = $grid_item -> gridImage( $post->ID );

							//CREATE THE GRID
							$grid = $grid_item->gridItem( $grid_category, $post->ID, $post->post_title, $post->post_name, $grid_image, $grid_profile );

							echo $grid;

						endwhile; // End of the loop.

					else: //if have_posts()
				?>
				</div>


					No Results.

					
				<?php				
					endif; //if have_posts()
				?>	

				


				

				
				

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->


	<?php 

		//See inc/template-tags.php for function definition
		escapes_do_local_favorites( 'winery' ); 
	?>

<?php get_footer(); ?>





