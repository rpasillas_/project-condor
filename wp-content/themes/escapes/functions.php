<?php
/**
 * Temecula Escapes functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Temecula Escapes
 */

if ( ! function_exists( 'escapes_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function escapes_setup() {
	/**
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Temecula Escapes, use a find and replace
	 * to change 'escapes' to the name of your theme in all the template files.
	 * You will also need to update the Gulpfile with the new text domain
	 * and matching destination POT file.
	 */
	load_theme_textdomain( 'escapes', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/**
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'escapes' ),
		'mobile'  => esc_html__( 'Optional Mobile Menu', 'escapes' ),
		'footer'  => esc_html__( 'Footer Menu', 'escapes' ),
	) );

	/**
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'escapes_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add styles to the post editor
	add_editor_style( array( 'editor-style.css', escapes_font_url() ) );

	add_image_size('featured-thumb', 264, 175, array( 'center', 'center') );

	add_image_size('winery-logo', 300, 259, false );

	//the 3 tile sizes
	add_image_size('tile-large', 490, 510, true );
	add_image_size('tile-medium', 240, 510, array('left', 'top') );	
	add_image_size('tile-medium-alt', 510, 240, array('center', 'center') );
	add_image_size('tile-small', 240, 250, true );

}
endif; // escapes_setup
add_action( 'after_setup_theme', 'escapes_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function escapes_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'escapes_content_width', 640 );
}
add_action( 'after_setup_theme', 'escapes_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function escapes_widgets_init() {

	// Define sidebars
	$sidebars = array(
		'sidebar-happenings'  => esc_html__( 'Happenings', 'escapes' ),
		'sidebar-explore'  => esc_html__( 'Explore', 'escapes' ),
		'sidebar-company'  => esc_html__( 'Company Pages', 'escapes' ),
		'sidebar-blogs'  => esc_html__( 'Blog Pages', 'escapes' ),
		'home-ssm'  => esc_html__( 'Home Page Social Media Widgets', 'escapes' ),
	);

	// Loop through each sidebar and register
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar( array(
			'name'       	=> $sidebar_name,
			'id'            => $sidebar_id,
			'description'   => sprintf ( esc_html__( 'Widget area for %s', 'escapes' ), $sidebar_name ),
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}

}
add_action( 'widgets_init', 'escapes_widgets_init' );


/**
 * TILE GRID CLASS
 * Responsible for making the grids
 */
require get_template_directory() . '/inc/tile-grid.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * The Levenshtein Distance Library
 * @link https://github.com/wyattferguson/Improved-Levenshtein-Distance
 */
require_once( 'inc/levpho.php' );



/**
 * TGM:: SPECIFY REQUIRED PLUGINS FOR THIS THEME
 */
require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

function escapes_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// Include these plugins that come with the Git Repo.
		array(
			'name'               => 'Event Countdown', // The plugin name.
			'slug'               => 'countdown', // The plugin slug (typically the folder name).
			'source'             => plugin_dir_path(__FILE__) . '/countdown', // The plugin source.
			'required'           => true, 
			'version'            => '',
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'MailChimp Caldera Forms Pre-processor', // The plugin name.
			'slug'               => 'mailchimp-for-caldera', // The plugin slug (typically the folder name).
			'source'             => plugin_dir_path(__FILE__) . '/mailchimp-for-caldera', // The plugin source.
			'required'           => true, 
			'version'            => '',
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'Advanced Custom Fields Pro', // The plugin name.
			'slug'               => 'mailchimp-for-caldera', // The plugin slug (typically the folder name).
			'source'             => plugin_dir_path(__FILE__) . '/advanced-custom-fields-pro', // The plugin source.
			'required'           => true, 
			'version'            => '',
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'Search WP', // The plugin name.
			'slug'               => 'searchwp', // The plugin slug (typically the folder name).
			'source'             => plugin_dir_path(__FILE__) . '/advanced-custom-fields-pro', // The plugin source.
			'required'           => true, 
			'version'            => '',
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'Facet WP', // The plugin name.
			'slug'               => 'facetwp', // The plugin slug (typically the folder name).
			'source'             => plugin_dir_path(__FILE__) . '/advanced-custom-fields-pro', // The plugin source.
			'required'           => true, 
			'version'            => '',
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),


		// Include this plugin from an arbitrary external source in your theme.

		// Include these plugins from the WordPress Plugin Repository.
		array(
			'name'      => 'Advanced Custom Fields',
			'slug'      => 'advanced-custom-fields',
			'required'  => true,
		),
		array(
			'name'      => 'Caldera Forms',
			'slug'      => 'caldera-forms',
			'required'  => true,
		),
		array(
			'name'      => 'Instagram Slider',
			'slug'      => 'instagram-slider-widget',
			'required'  => true,
		),
		array(
			'name'      => 'WP REST API',
			'slug'      => 'rest-api',
			'required'  => true,
		),
		array(
			'name'      => 'Admin Menu Editor',
			'slug'      => 'admin-menu-editor',
			'required'  => false,
		),
	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'escapes',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	);

	tgmpa( $plugins, $config );
}


add_action( 'tgmpa_register', 'escapes_register_required_plugins' );



require get_template_directory() . '/post-types/custom-post-type-wine.php';
require get_template_directory() . '/post-types/custom-post-type-brewery.php';
require get_template_directory() . '/post-types/custom-post-type-happenings.php';
require get_template_directory() . '/post-types/custom-post-type-explore.php';
require get_template_directory() . '/post-types/custom-post-type-eat-drink.php';
//require get_template_directory() . '/post-types/custom-post-type-tester.php';

require get_template_directory() . '/inc/nav_walker_menu.php';

//THEME OPTIONS
require get_template_directory() . '/options.php';

//ACF Options Page
if( function_exists('acf_add_options_page') ) {	

	//customized option page
	acf_add_options_page(array(
		'page_title' 	=> 'Site Navigation',
		'menu_title'	=> 'Site Navigation',
		'menu_slug' 	=> 'site-navigation-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

		//customized option page
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title'	=> 'Site Options',
		'menu_slug' 	=> 'site-options-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	//a sub-page option 
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Main Navigation',
	// 	'menu_title'	=> 'Header',
	// 	'parent_slug'	=> 'site-navigation-settings',
	// ));
}

//Proof of concept for how to add new fields to 
//nav_menu_item posts in the WordPress menu editor.
//https://gist.github.com/kucrut/3804376


//add logo to admin login
function te_login_logo() { ?>
    <style type="text/css">
	   #login h1 a, .login h1 a {
		  background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/cropped-temecula-escapes-initials-icon.png);
		  padding-bottom: 30px;
	   }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'te_login_logo' );


//ADD GOOGLE MAPS API KEY FOR ACF MAP PICKER
function my_acf_init() {	
	acf_update_setting('google_api_key', 'AIzaSyDldRs46dhvEvsuAcLuMDd_6hEmwVqkHkU');
}

add_action('acf/init', 'my_acf_init');


//FUZZY SEARCH: INCREASE THRESTHOLD FROM 5 (DEFAULT) TO 4
function my_fuzzy_word_length()
{
  return 4;
}
add_filter( 'searchwp_fuzzy_min_length', 'my_fuzzy_word_length' );


//REST CUSTOM REST END POINT FOR HAPPENINGS
add_action( 'rest_api_init', function(){	
	register_rest_route( 'happenings-search/v1', '/search/', array(
		'methods' => 'GET',
		'callback' => 'getResults'
	));
	register_rest_route( 'happenings-search/v1', '/default/', array(
		'methods' => 'GET',
		'callback' => 'getDefaultResults'
	));
});

//THIS IS THE MAIN HAPPENINGS PAGE SEARCH FUNCTION
function getResults( WP_REST_Request $request ){
	$category = $request->get_param( 'happenings-category' );
	$location = $request->get_param( 'happenings-location' );
	$date 	= $request->get_param( 'happenings-date' ); //format 2016-10-29
	$page 	= $request->get_param( 'page' );


	$posts_per_page = 20;

	if( $page <= 0){
		$page = 1;
	}

	$query_params = array (
		'cache_results' 		 => true,
		'update_post_meta_cache' => true,
		'update_post_term_cache' => true,
		'post_status' 			=> 'publish',
		'post_type' 			=> 'happenings',
		'posts_per_page' 		=> $posts_per_page,
		'paged'					=> $page,
		'meta_key'				=> 'event_date',
		'orderby' 				=> 'meta_value_num',		
		'order'					=> 'ASC',
		'meta_query' 			=> array (
					'relation' => 'AND',
			    	0 => array (
				     	'key' 		=> 'event_date',				    
				     	'value' 	=> $date,
				     	'compare' 	=> '<=',
				     	'type'		=> 'DATE'
				    ), //happening_start_date
				    1 => array (
				     	'key' 		=> 'event_end_date',
				     	'value' 	=> $date,
				     	'compare' 	=> '>=',
				     	'type'		=> 'DATE'
				    ), //happening_start_date

		), //meta_query		
	);

	if( $category !=='' || $location !=='' ){		
			$query_params['tax_query'] = array( 0=> array() ) ;		
	}

	if( $category !=='' ){
		array_push(
			$query_params['tax_query'][0], 
			array(
				'taxonomy' 	=> 'happenings_category',
				'field' 	=> 'slug',
				'terms' 	=> array ( 0 => $category, ),
				'operator' 	=> 'IN',
			)
		);
	}

	if( $location !=='' ){
		array_push(
			$query_params['tax_query'][0],
			array(
				'taxonomy' 	=> 'happenings_locations',
				'field' 	=> 'slug',
				'terms' 	=> array ( 0 => $location, ),
				'operator' 	=> 'IN',
			)
		);
	}

	if( $location !=='' && $category!=='' ){
		array_push(
			$query_params['tax_query'][0],
			array('relation' => 'AND')
		);
	}

	//return $query_params;

	$query = new WP_Query();

	$results = $query->query( $query_params );

	$data = array();
	$data['max_num_pages'] = $query->max_num_pages;
	$data['next_page'] = false;

	if( $page < $data['max_num_pages'] ){
		$data['next_page'] = $page + 1;
	}
	

	if( !empty( $results ) ){
		return parseResults( $results );
	}else{
		return new WP_Error( 'te_no_happenings_posts', 'No Happenings', array( 'status' => 404 ));
	}
}

//THIS IS THE DEFAULT REST REQUEST
//USED ON THE EVENTS PAGE LOADS
//AND USED WHEN A SPECIFIC SEARCH DOESN'T RETURN RESULTS
function getDefaultResults( WP_REST_Request $request ){
	$date 	= date("Y-m-d"); //format 2016-10-29
	$page 	= $request->get_param( 'page' );

	$posts_per_page = 20;

	if( $page <= 0){
		$page = 1;
	}

	$query_params = array (
		'cache_results' 		 => true,
		'update_post_meta_cache' => true,
		'update_post_term_cache' => true,
		'post_status' 			=> 'publish',
		'post_type' 			=> 'happenings',
		'ignore_sticky_posts' 	=> true,
		'posts_per_page' 		=> $posts_per_page,
		'paged'					=> $page,
		'orderby' 				=> 'meta_value_num',		
		'order'					=> 'ASC',
		'meta_query' 			=> array (					
				    0 => array (
				     	'key' 		=> 'event_end_date',
				     	'value' 	=> $date,
				     	'compare' 	=> '>=',
				    ), //happening_start_date

		), //meta_query
	);

	$query = new WP_Query();

	$results = $query->query( $query_params );

	$data = array();
	$data['max_num_pages'] = $query->max_num_pages;
	$data['next_page'] = false;

	if( $page < $data['max_num_pages'] ){
		$data['next_page'] = $page + 1;
	}
	

	if( !empty( $results ) ){	

		return parseResults( $results );

	}else{
		return new WP_Error( 'te_no_happenings_posts', 'No Happenings', array( 'status' => 404 ));
	}
}

//HELPER FUNCTION TO PARSE HAPPENINGS RESULTS
function parseResults( $results ){
	$i = 0;
	foreach( $results as $post ){
		$data['posts'][++$i]['post'] = $post;

		$formatted_start_date = get_post_meta( $post->ID, 'event_date', true );
		$unix_start_date = strtotime( $formatted_start_date );

		$data['posts'][$i]['happening_start_date']['date'] = $formatted_start_date;

		$data['posts'][$i]['happening_start_date']['month'] = date('M', $unix_start_date);
		$data['posts'][$i]['happening_start_date']['day'] = date('d', $unix_start_date);
		$data['posts'][$i]['happening_start_time'] = date('g:ia', $unix_start_date);

		if( get_post_meta( $post->ID, 'event_end_date', true ) ){
			$formatted_end_date = get_post_meta( $post->ID, 'event_end_date', true );
			$unix_end_date = strtotime( $formatted_end_date );

			$data['posts'][$i]['happening_end_date']['date'] = $formatted_end_date;
			$data['posts'][$i]['happening_end_date']['month'] = date('M', $unix_end_date);
			$data['posts'][$i]['happening_end_date']['day'] = date('d', $unix_end_date);	
			$data['posts'][$i]['happening_end_time'] = date('g:ia', $unix_end_date);

			//IS THE EVENT LONGER THAN ONE DAY?
			if( $data['posts'][$i]['happening_end_date']['day'] !== $data['posts'][$i]['happening_start_date']['day'] ){
				$data['posts'][$i]['happening_multi_date'] = true;
			}
		}
	 
		$data['posts'][$i]['tile_size'] = 'default';
		if( get_field( 'size_of_tile', $post->ID ) ){
			$field = get_field_object( 'size_of_tile', $post->ID );
			$data['posts'][$i]['tile_size'] = $field['value'];

			//SET UP THE ALTERNATE UPGRADED SIZE
			if( $field['value'] == 'upgraded' && ($post->ID % 2) == 1 ){
				$data['posts'][$i]['tile_size'] = 'upgraded-alt';
			}
		}


		if( $data['posts'][$i]['tile_size'] == 'premium' )
		{
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'tile-large' );
		}else if( $data['posts'][$i]['tile_size'] == 'upgraded-alt' )
		{	
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'tile-medium-alt' );					
		}else if( $data['posts'][$i]['tile_size'] == 'upgraded' )
		{		
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'tile-medium' );				
		}else
		{
			$post_thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'tile-small' );
		}


		$data['posts'][$i]['image'] = $post_thumb[0];
		$data['posts'][$i]['post_link'] = get_permalink( $post->ID );
		
			

	}//FOREACH

	return $data;
}

//SEARCH FOR FUTURE & IN PROGRESS HAPPENINGS
//RETURN THEIR LOCATIONS & CATEGORY TERMS
function getLocCat(){	
	date_default_timezone_set("America/Los_Angeles");
	$query_params = array (
		'post_type' 		=> 'happenings',
		'post_status'		=> 'publish',
        'orderby' 			=> 'meta_value',
        'meta_key' 			=> 'event_end_date',
        'order' 			=> 'ASC',
        'posts_per_page' 	=> '-1',
        'meta_query'		=> array(
        		0 => array(
        				'key' => 'event_end_date',
        				'value' => date("Y-m-d H:i"),
        				'compare' => '>=',
        				'type' => 'DATE'
        			),
        		1 => array(
        				'key' => 'make_event',
        				'value' => 1,
        				'compare' => '=',
        				'type' => 'NUMERIC'
        			)
        	)
	);

	$query = new WP_Query();

	$results = $query->query( $query_params );

	$data = array();	

	if( !empty( $results ) ){
		foreach( $results as $post ){

			$location_terms = wp_get_post_terms($post->ID, 'happenings_locations');		
			foreach( $location_terms as $term ){
				$data['locations'][$term->slug] = $term->name;			
			}

			$category_terms = wp_get_post_terms($post->ID, 'happenings_category');			
			foreach( $category_terms as $term ){
				$data['categories'][$term->slug] = $term->name;
			}
		}//FOREACH

		return $data;
	}else{
		return 'Nothing';
	}
}


//GET ALL OF THE MEMBERS OF A SPECIFIC SEGMENT 
//FROM TE'S MAILCHIMP LIST
//THIS FUNCTION IS USED FOR TE BOOTH EVENTS
//TO CONDUCT RAFFLES
function teMailChimpListSegmentMembers(){
	$apiKey = '6bd61cc9ebb6a021013bc1bcc691ed26-us6';
    $listId = 'd7c794b67e';
    $segmentId = '25953';

    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/segments/' . $segmentId . '/members?count=100';



	$ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);     

    $result = curl_exec($ch);

    //just the status code
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    //--MailChimp's result object
    //--full of all the goodies
    //$httpCode = json_decode($result, true);

    return $result;
}

//USED BY MAILCHIMP RAFFLE
function add_query_vars_filter( $vars ){
	date_default_timezone_set('America/Los_Angeles');	
	if( date('dmY') == 9122016 ){
		$vars[] = "special";
	}
	
	return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

//USED BY MAILCHIMP RAFFLE
add_action( 'rest_api_init', function(){	
	register_rest_route( 'raffle/v1', '/send_winner/', array(
		'methods' => 'GET',
		'callback' => 'send_mc_email'
	));
});

function send_mc_email( WP_REST_Request $request ){
	$message = 'Raffle Winner: ';
	$message .= $request->get_param( 'winner_name' ) . ' ';
	$message .= $request->get_param( 'winner_email' );

	return wp_mail('it@evaero.co', 'Raffle Winner', $message);
}