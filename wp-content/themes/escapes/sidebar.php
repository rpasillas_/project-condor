<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Temecula Escapes
 */

// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
// 	return;
// }
?>

<aside class="secondary widget-area" role="complementary">
	
	<?php 

		if( is_singular( 'happenings' ) )
		{
			get_template_part( 'sidebars/happenings' );

		} else if ( is_singular( 'explore' ) )
		{
			get_template_part( 'sidebars/explore' );

		} else if( is_singular( array('winery', 'brewery') ) )
		{
			
			get_template_part( 'sidebars/experience' );

		} else if( is_page_template( 'tpl-page-company.php' ) )
		{

			get_template_part( 'sidebars/company' );

		} else if( is_singular(array( 'good_to_know', 'eat_and_drink' )) ) 
		{

			get_template_part( 'sidebars/good', 'to-know-eat-drink' );

		} else if( is_404() )
		{

			get_template_part( 'sidebars/404' );
		
		}else if( is_search() )
		{

			get_template_part( 'sidebars/search' );
		
		}else{

			get_template_part( 'sidebars/default' );

		}

	?>





	

</aside><!-- .secondary -->