<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">
				<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
				?>



				<h1>Temecula Area Breweries</h1>
				<article>
					<p>Temecula is more than wine country, this city is a hub for Southern California's exploding craft beer scene. Our valley offers breweries that can compete with any of the other craft breweries across the U.S. Each brewery has their own unique personality and offers a large variety of styles and flavors for any beer lover. Temecula’s west coast style beers always ensure that you’ll find what you’re looking for and more. Browse our brewery listings below and <em>hop</em> on over to discover your new favorite brew.</p>
				</article>

				
				<?php echo do_shortcode( '[caldera_clarity name="temecula_breweries"]' ); ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->



	

	<?php 		
		get_template_part('template-parts/experiences', 'banners'); 

		//See inc/template-tags.php for function definition
		escapes_do_local_favorites( 'brewery' ); 
	?>


	<div class="featured-articles">
		<div class="wrap">
			<div class="heading"><span class="h1">Featured Articles</span></div>
			
			<?php 
				$posts = te_featured_articles( 'brewery' ); 
				
				foreach ( $posts as $post )
				{
					$author_id = $post->post_author;
					setup_postdata( $post ); 

					echo escapes_do_featured_articles( $post, $author_id );					
				}
			?>
			
		</div>
	</div>
<?php get_footer(); ?>