<?php
/**
 * The template for displaying the experience landing page.
 *
 * @package Temecula Escapes
 */


require_once( 'inc/levpho.php' );

get_header(); ?>


	<div class="primary content-area">
		<main id="main" class="site-main" role="main">		

			<div class="wrap">	
				<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}				
				?>
			</div><!-- .wrap -->			

			<div class="happenings-search-wrap">
				<div class="wrap">
					<form id="happenings-search" class="happenings-search" data-search="/happenings-search/">

						<legend>Find Out What's Happening In Temecula</legend>
						
						<span class="form-sentence">I'm interested in</span>

						<div class="field-wrap">
							
							<select name="happenings-category" id="happenings-category">
								<option value="">All</option>
								<?php
									
									$locCat = getLocCat();

									foreach( $locCat['categories'] as $k=> $v){
										echo '<option value="'. $k .'">'. $v .'</option>';
									}							
								?>
							</select>

							<label for="happenings-category">category</label>
						</div>

						<span class="form-sentence">happenings in</span>

						<div class="field-wrap">								
							<select name="happenings-location" id="happenings-location">
								<option value="">All</option>
								<?php
									foreach( $locCat['locations'] as $k=> $v){
										echo '<option value="'. $k .'">'. $v .'</option>';
									}							
								?>
							</select>											
							<label for="happenings-location">location</label>
						</div>

						<span class="form-sentence">starting on</span>

						<div class="field-wrap">					
							<input type="text" id="happenings-date" name="happenings-date">
							<label for="happenings-date">date</label>
						</div>

						<span class="form-sentence">.</span>
						
						<div class="submit-wrap">
							<div class="submit-arrow">
								<input type="submit" value="Let's Go!">
							</div>
						</div>

					</form>
				</div><!-- .wrap -->
			</div>
			
			<div class="no-results-wrap"></div>

			<div class="wrap results-wrap">
				<div class="bubblingG loading" style="display: none;"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div>

				<div id="show-more-results" class="happenings-results-tiles">
					<div class="posts masonry-tiles">
						<div class="grid-sizer"></div>
					</div>

					<div class="load-more-wrap" style="display: none;"><button data-page="1" class="load-more">Load More</button></div>

				</div>
			</div>

			<script id="happenings-results" type="text/x-handlebars-template">

				{{#each this.posts}}

					<div class="masonry-grid-item {{this.tile_size}}{{#if this.happening_multi_date}} double-date{{/if}}" style="background-image: url({{image}});" itemscope itemtype="http://schema.org/Event">
						<div class="grid-inner">
					
							<div class="start-date" itemprop="startDate" content="{{this.happening_start_date.date}}T{{this.happening_start_time}}">
								<div class="start">
									<span class="month">{{this.happening_start_date.month}}</span>
									<span class="day">{{this.happening_start_date.day}}</span>
								</div>
								{{#if this.happening_multi_date}}
									<span>-</span>
									<div class="end">
										<span class="month">{{this.happening_end_date.month}}</span>
										<span class="day">{{this.happening_end_date.day}}</span>
									</div>
								{{/if}}
							</div>
							<div class="happening_time">
								<span class="time_start">{{this.happening_start_time}}</span> - <span class="time_end">{{this.happening_end_time}}</span>
							</div>
							<a href="{{this.post_link}}" class="happening_title title" itemprop="url"><span itemprop="name">{{this.post.post_title}}</span></a>
							
							<div class="grid-item-share-icons">
								<span class="grid-item-share-text">Share This</span>
								<a href="https://www.facebook.com/sharer/sharer.php?u={{this.post_link}}" class="hover-icon fb-hover-icon" title="Share this greatness on Facebook" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;"><svg><use xlink:href="#icon-facebook-f"></use></svg></a>

								<a href="https://twitter.com/share?text={{this.post.post_title}}&amp;url={{this.post_link}}" class="hover-icon tw-hover-icon" title="Tweet this awesomeness" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;"><svg><use xlink:href="#icon-twitter-bird"></use></svg></a>

								<a href="mailto:?subject={{this.post.post_title}}&body=Check out this article on Temecula Escapes, {{this.post_link}}" class="hover-icon email-hover-icon" title="Email this to your peeps"><svg><use xlink:href="#icon-airplane"></use></svg></a>
								
								<a href="#" class="hover-icon fav-hover-icon js-favorites" title="Add to your favorites list"><svg><use xlink:href="#icon-heart"></use></svg></a>
							</div>
							<a href="{{this.post_link}}" class="grid-item-read-more"><svg><use xlink:href="#icon-magnifying-plus"></use></svg> Read More</a>
													
						</div>
						<div class="masonry-grid-item-overlay"></div>
					</div>
				{{/each}}

			</script>
	
			<div class="add-event-banner">
				<div class="wrap">

					<?php 
						$home = 33; //banner image uploaded to home page
						$img = get_field('ss_banner_image', $home);
						$alt = ( $img['alt'] ) ? $img['alt']: $img['name'];				
					?>

					<div class="image-wrap" style="background-image: url(<?php echo $img['url']; ?>);">
						<img src="<?php echo $img['url']; ?>" alt="<?php echo $alt; ?>">
					</div>

					<div class="text-wrap">
						<div class="title">
							<span class="h1">Add An Event</span>
						</div>
						<p>Want to promote an event? We'd love to hear about it. Complete the form with all the details.</p>

						<a href="<?php echo site_url('/add-event'); ?>" title="Click here to add a new Event." class="btn btn-outline btn-white">Add Event</a>
					</div>

				</div>
			</div>

			
		</main><!-- #main -->
	</div><!-- .primary -->

	<div class="featured-articles">
		<div class="wrap">
			<div class="heading"><span class="h1">Featured Articles</span></div>
			
			<?php 
				$posts = te_featured_articles( 'happenings' ); 
				
				foreach ( $posts as $post )
				{
					$author_id = $post->post_author;
					setup_postdata( $post ); 

					echo escapes_do_featured_articles( $post, $author_id );
				}
			?>
			
		</div>
	</div>

	


<?php get_footer(); ?>