<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Temecula Escapes
 */

/**
 * Require the navigation file
 * instead of loading it through functions.php
 * because it's a heavy request to the db
 * so load it only when needed and not everywhere, including the admin.
*/
require_once('inc/navigation.php');

$class= '';
if( get_query_var('special') == 'vssn122016' ):	
	$class = 'raffle_screen';
endif;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php global $is_IE; if ( $is_IE ) : ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<meta property="fb:app_id" content="1100068270023612">


<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-74195003-1', 'auto');
	  ga('send', 'pageview');

	</script>


<?php wp_head(); ?>
</head>

<body <?php body_class($class); ?>>
<span class="svg-defs"><?php escapes_include_svg_icons(); ?></span>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'escapes' ); ?></a>

	<header class="site-header">
		
		
		<div class="header-top">
			<div class="wrap">
				<div class="logo-wrap">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="site-title"><svg><use xlink:href="#icon-temecula-escapes-color"></use></svg></a>
				</div>
				<nav class="account-navigation">
					<ul>
						<li class="account-nav-item account-item-account js-account"><a href="#" onClick="ga('send', 'event', 'Account', 'open', 'Gallivant');"><svg><use xlink:href="#icon-account"></use></svg><span>Account</span></a></li>
						<li class="account-nav-item account-item-favorites js-favorites"><a href="#" onClick="ga('send', 'event', 'Favs', 'view', 'Gallivant');"><svg><use xlink:href="#icon-favs"></use></svg><span>Favs</span></a></li>
						<li class="account-nav-item account-item-itinerary js-itinerary"><a href="#" onClick="ga('send', 'event', 'Itinerary', 'view', 'Gallivant');"><svg><use xlink:href="#icon-itinerary"></use></svg><span>Itinerary</span></a></li>
					</ul>
				</nav>
				<a href="#site-navigation" title="Open Menu" class="toggle-navigation"><svg><use xlink:href="#icon-mobile-menu"></use></svg></a>
			</div><!-- .wrap -->
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation">
			
			<?php 
				$explore_nav_items = get_transient('te_explore_nav');
				if ( !$explore_nav_items ) {
				    $explore_nav_items = te_explore_navigation();
				}

				$happenings_nav_items = get_transient('te_happenings_nav');
				if ( !$happenings_nav_items ) {
				    $happenings_nav_items = te_happenings_navigation();
				}

				$eat_and_drink_nav_items = get_transient('te_eat_and_drink_nav');
				if ( !$eat_and_drink_nav_items ) {
				    $eat_and_drink_nav_items = te_eat_and_drink_navigation();
				}

				//for debugging, uncomment these next few lines
				//shows you all of the arrays in one big array
				//$te_nav = array_merge($explore_nav_items, $happenings_nav_items, $eat_and_drink_nav_items, $good_to_know_nav_items);

				// echo '<pre style="color:#fff;">';
				// print_r( $eat_and_drink_nav_items );
				// echo '</pre>';
			?>
			<?php
				$nav_experience_selected = '';
				$nav_explore_selected = '';
				$nav_happenings_selected = '';
				$nav_eat_and_drink_selected = '';

				if( is_page('experience') || is_post_type_archive( array('winery', 'brewery') ) || is_singular( array('winery', 'brewery') ) ){

					$nav_experience_selected = ' selected';

				}else if( is_post_type_archive( 'explore' ) || is_singular( 'explore' ) ){
					
					$nav_explore_selected = ' selected';

				}else if( is_post_type_archive( 'happenings' ) || is_singular( 'happenings' ) ){
					
					$nav_happenings_selected = ' selected';

				}else if( is_post_type_archive( 'eat_and_drink' ) || is_singular( 'eat_and_drink' ) ){
					
					$nav_eat_and_drink_selected = ' selected';

				}
			?>

			<div class="wrap">
					<ul class="top-level">
						
						<li class="nav-item-js nav-item nav-item-account">
							<ul class="account-navigation">
								<li class="account-nav-item account-item-account js-account"><a href="#" onClick="ga('send', 'event', 'Account', 'open', 'Gallivant');"><svg><use xlink:href="#icon-account"></use></svg><span>Account</span></a></li>
								<li class="account-nav-item account-item-favorites js-favorites"><a href="#" onClick="ga('send', 'event', 'Favs', 'view', 'Gallivant');"><svg><use xlink:href="#icon-favs"></use></svg><span>Favs</span></a></li>
								<li class="account-nav-item account-item-itinerary js-itinerary"><a href="#" onClick="ga('send', 'event', 'Itinerary', 'view', 'Gallivant');"><svg><use xlink:href="#icon-itinerary"></use></svg><span>Itinerary</span></a></li>
							</ul>
						</li>

						<li class="nav-item-js nav-item nav-item-experience<?php echo $nav_experience_selected; ?>">
							<a href="<?php echo esc_url( home_url('experience/') ); ?>">Experience</a>

							<span class="submenu-toggle"><svg><use xlink:href="#icon-nav-arrow"></use></svg></span>
							<div class="sub-nav">
								<div class="wrap">
									<ul>
										<li class="experience-mobile-back"><svg><use xlink:href="#icon-nav-arrow"></use></svg> Experience</li>
										<li><a href="<?php echo esc_url( home_url('/experience/temecula-wineries/') ); ?>"><svg class="experience-icon"><use xlink:href="#icon-wine-glass"></use></svg><span class="link-text">Wineries</span></a></li>
										<li><a href="<?php echo esc_url( home_url('/experience/temecula-breweries/') ); ?>"><svg class="experience-icon"><use xlink:href="#icon-beer-mug"></use></svg><span class="link-text">Breweries</span></a></li>		
									</ul>
								</div>
							</div>
						</li>

						<li class="nav-item-js nav-item-has-children<?php echo $nav_explore_selected; ?>">
							<a href="<?php echo esc_url( home_url('explore-temecula/') ); ?>">Explore</a>
							<span class="submenu-toggle"><svg><use xlink:href="#icon-nav-arrow"></use></svg></span>
<?php
	echo "\t\t\t\t\t\t\t" . '<div class="sub-nav">' . "\n";
	echo "\t\t\t\t\t\t\t\t" . '<div class="wrap">' . "\n";
	//DISPLAY TERM NAMES & TERM POSTS
	$explore_sub_level_items = (int)count($explore_nav_items['explore_category']['nav-items']);
	
	echo '<ul class="terms-posts">' . "\n";
	
	for ( $i=0; $i<$explore_sub_level_items; ++$i ) {
		if( array_key_exists($i, $explore_nav_items['explore_category']['nav-items'] ) ){
			echo "\t\t\t\t\t\t\t\t" . '<li class="term-name"><a href="' . esc_url( $explore_nav_items['explore_category']['nav-items'][$i]['term']['term_link'] ) . '" class="term-name">' . $explore_nav_items['explore_category']['nav-items'][$i]['term']['term_name'] . '</a><svg><use xlink:href="#icon-nav-arrow"></use></svg></li>' . "\n";

			echo "\t\t\t\t\t\t\t\t" . '<li class="term-name-duplicate"><svg><use xlink:href="#icon-nav-arrow"></use></svg><span>' . $explore_nav_items['explore_category']['nav-items'][$i]['term']['term_name'] . '</span></li>' . "\n";

			echo "\t\t\t\t\t\t\t\t" . '<li class="term-post"><a href="' . esc_url( get_permalink($explore_nav_items['explore_category']['nav-items'][$i]['post']['post_id']) ) . '" class="term-post">' . $explore_nav_items['explore_category']['nav-items'][$i]['post']['post_title'] . '</a></li>' . "\n";
		}
	}
	echo '</ul><!-- /terms-posts -->' . "\n";

	//DISPLAY FEATURED POSTS
	if ( array_key_exists( 'featured-items', $explore_nav_items['explore_category'] ) ) {
		echo '<div class="featured-posts">' . "\n";

		echo "\t\t\t\t\t\t\t\t" . '<span class="featured-heading">Featured Articles</span>' . "\n";
		

		$explore_featured_items = (int)count($explore_nav_items['explore_category']['featured-items']);
		echo "\t\t\t\t\t\t\t\t\t" . '<ul class="featured-items featured-items-' . $explore_featured_items . '">' . "\n";

		

		for( $i=0; $i<$explore_featured_items; ++$i ){
			echo "\t\t\t\t\t\t\t\t\t\t" . '<li class="featured-item">' . "\n";

			if( $explore_featured_items > 1 && $i < 1 ){
				echo '<span class="featured-arrow featured-right"><svg><use xlink:href="#icon-arrow-left-black"></use></svg></span>';
			}else if( $explore_featured_items > 1 && $i <= 1){
				echo '<span class="featured-arrow featured-left"><svg><use xlink:href="#icon-arrow-left-black"></use></svg></span>';
			}

			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a href="' . esc_url( $explore_nav_items['explore_category']['featured-items'][$i]['post_link'] ) . '" class="post-link">' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t\t" . '<span class="post-link">' . $explore_nav_items['explore_category']['featured-items'][$i]['post_title'] . '</span>' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t\t" . $explore_nav_items['explore_category']['featured-items'][$i]['post_thumbnail']. "\n"; 
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '</a>' . "\n"; 
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a href="' . esc_url( get_author_posts_url($explore_nav_items['explore_category']['featured-items'][$i]['post_author']) ) . '" class="author-link">By ' . get_the_author_meta('first_name', $explore_nav_items['explore_category']['featured-items'][$i]['post_author']) . ' '. get_the_author_meta('last_name', $explore_nav_items['explore_category']['featured-items'][$i]['post_author']) .  '</a>'  . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<span class="post-date">' . date('F d, Y', strtotime($explore_nav_items['explore_category']['featured-items'][$i]['post_date']) ) . '</span>' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t" .'</li>' . "\n";
		}

		echo "\t\t\t\t\t\t\t\t\t" . '</ul>' . "\n";
		
		echo '</div><!-- /featured-posts -->' . "\n";
	}
	
	echo "\t\t\t\t\t\t\t" . '<div class="top-level-mobile-back"><svg><use xlink:href="#icon-nav-arrow"></use></svg>Explore</div>' . "\n";

	echo "\t\t\t\t\t\t\t\t" . '</div>' . "\n";
	echo "\t\t\t\t\t\t\t" . '</div>' . "\n";
?>
						</li><!-- /explore -->

						<li class="nav-item-js nav-item-has-children<?php echo $nav_happenings_selected; ?>">
							<a href="<?php echo esc_url( home_url('temecula-events/') ); ?>">Happenings</a>
							<span class="submenu-toggle"><svg><use xlink:href="#icon-nav-arrow"></use></svg></span>
<?php
	echo "\t\t\t\t\t\t\t" . '<div class="sub-nav">' . "\n";
	echo "\t\t\t\t\t\t\t\t" . '<div class="wrap">' . "\n";
	//DISPLAY TERM NAMES & TERM POSTS
	$happenings_sub_level_items = (int)count($happenings_nav_items['happenings_category']['nav-items']);

	echo '<ul class="terms-posts">' . "\n";
	for ( $i=0; $i<$happenings_sub_level_items; ++$i ) {
		if( array_key_exists($i, $happenings_nav_items['happenings_category']['nav-items'] ) ){

			echo "\t\t\t\t\t\t\t\t" . '<li class="term-name"><a href="' . esc_url( $happenings_nav_items['happenings_category']['nav-items'][$i]['term']['term_link'] ) . '" class="term-name">' . $happenings_nav_items['happenings_category']['nav-items'][$i]['term']['term_name'] . '</a><svg><use xlink:href="#icon-nav-arrow"></use></svg></li>' . "\n";

			echo "\t\t\t\t\t\t\t\t" . '<li class="term-name-duplicate"><svg><use xlink:href="#icon-nav-arrow"></use></svg><span>' . $happenings_nav_items['happenings_category']['nav-items'][$i]['term']['term_name'] . '</span></li>' . "\n";

			echo "\t\t\t\t\t\t\t\t" . '<li class="term-post"><a href="' . esc_url( get_permalink($happenings_nav_items['happenings_category']['nav-items'][$i]['post']['post_id']) ) . '" class="term-post">' . $happenings_nav_items['happenings_category']['nav-items'][$i]['post']['post_title'] . '</a></li>' . "\n";
		}
	}
	echo '</ul><!-- /terms-posts -->' . "\n";

	//DISPLAY FEATURED POSTS
	if ( array_key_exists( 'featured-items', $happenings_nav_items['happenings_category'] ) ) {
		echo '<div class="featured-posts">' . "\n";	

		echo "\t\t\t\t\t\t\t\t" . '<span class="featured-heading">Featured Articles</span>' . "\n";

		$happenings_featured_items = (int)count($happenings_nav_items['happenings_category']['featured-items']);

		echo "\t\t\t\t\t\t\t\t\t" . '<ul class="featured-items featured-items-' . $happenings_featured_items . '">' . "\n";

		
		for( $i=0; $i<$happenings_featured_items; ++$i ){
			echo "\t\t\t\t\t\t\t\t\t\t" . '<li class="featured-item">' . "\n";

			if( $happenings_featured_items > 1 && $i < 1 ){
				echo '<span class="featured-arrow featured-right"><svg><use xlink:href="#icon-arrow-left-black"></use></svg></span>';
			}else if( $happenings_featured_items > 1 && $i <= 1){
				echo '<span class="featured-arrow featured-left"><svg><use xlink:href="#icon-arrow-left-black"></use></svg></span>';
			}


			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a href="' . esc_url( $happenings_nav_items['happenings_category']['featured-items'][$i]['post_link'] ) . '" class="post-link">' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t\t" . '<span class="post-link">' . $happenings_nav_items['happenings_category']['featured-items'][$i]['post_title'] . '</span>' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t\t" . $happenings_nav_items['happenings_category']['featured-items'][$i]['post_thumbnail']. "\n"; 
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '</a>' . "\n"; 
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a href="' . esc_url( get_author_posts_url($happenings_nav_items['happenings_category']['featured-items'][$i]['post_author']) ) . '" class="author-link">By ' . get_the_author_meta('first_name', $happenings_nav_items['happenings_category']['featured-items'][$i]['post_author']) . ' '. get_the_author_meta('last_name', $happenings_nav_items['happenings_category']['featured-items'][$i]['post_author']) .  '</a>'  . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<span class="post-date">' . date('F d, Y', strtotime($happenings_nav_items['happenings_category']['featured-items'][$i]['post_date']) ) . '</span>' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t" .'</li>' . "\n";			
		}

		echo "\t\t\t\t\t\t\t\t\t" . '</ul>' . "\n";
		
		echo '</div><!-- /featured-posts -->' . "\n";
	}
	
	echo "\t\t\t\t\t\t\t" . '<div class="top-level-mobile-back"><svg><use xlink:href="#icon-nav-arrow"></use></svg>Happenings</div>' . "\n";

	echo "\t\t\t\t\t\t\t\t" . '</div>' . "\n";
	echo "\t\t\t\t\t\t\t" . '</div>' . "\n";
?>
						</li><!-- /happenings -->						

						<li class="nav-item-js nav-item-has-children<?php echo $nav_eat_and_drink_selected; ?>">
							<a href="<?php echo esc_url( home_url('eat-drink-temecula/') ); ?>">Eat &amp; Drink</a>
							<span class="submenu-toggle"><svg><use xlink:href="#icon-nav-arrow"></use></svg></span>
<?php
	echo "\t\t\t\t\t\t\t" . '<div class="sub-nav">' . "\n";
	echo "\t\t\t\t\t\t\t\t" . '<div class="wrap">' . "\n";
	//DISPLAY TERM NAMES & TERM POSTS
	$eat_and_drink_sub_level_items = (int)count($eat_and_drink_nav_items['eat_and_drink_category']['nav-items']);

	echo '<ul class="terms-posts">' . "\n";
	for ( $i=0; $i<$eat_and_drink_sub_level_items; ++$i ) {

		if( array_key_exists($i, $eat_and_drink_nav_items['eat_and_drink_category']['nav-items'] ) ){
			echo "\t\t\t\t\t\t\t\t" . '<li class="term-name"><a href="' . esc_url( $eat_and_drink_nav_items['eat_and_drink_category']['nav-items'][$i]['term']['term_link'] ) . '" class="term-name">' . $eat_and_drink_nav_items['eat_and_drink_category']['nav-items'][$i]['term']['term_name'] . '</a><svg><use xlink:href="#icon-nav-arrow"></use></svg></li>' . "\n";

			echo "\t\t\t\t\t\t\t\t" . '<li class="term-name-duplicate"><svg><use xlink:href="#icon-nav-arrow"></use></svg><span>' . $eat_and_drink_nav_items['eat_and_drink_category']['nav-items'][$i]['term']['term_name'] . '</span></li>' . "\n";

			echo "\t\t\t\t\t\t\t\t" . '<li class="term-post"><a href="' . esc_url( get_permalink($eat_and_drink_nav_items['eat_and_drink_category']['nav-items'][$i]['post']['post_id']) ) . '" class="term-post">' . $eat_and_drink_nav_items['eat_and_drink_category']['nav-items'][$i]['post']['post_title'] . '</a></li>' . "\n";
		}
		
	}
	echo '</ul><!-- /terms-posts -->' . "\n";

	//DISPLAY FEATURED POSTS
	if ( array_key_exists( 'featured-items', $eat_and_drink_nav_items['eat_and_drink_category'] ) ) {
		echo '<div class="featured-posts">' . "\n";
		echo "\t\t\t\t\t\t\t\t" . '<span class="featured-heading">Featured Articles</span>' . "\n";

		$eat_and_drink_featured_items = (int)count($eat_and_drink_nav_items['eat_and_drink_category']['featured-items']);

		echo "\t\t\t\t\t\t\t\t\t" . '<ul class="featured-items featured-items-' . $eat_and_drink_featured_items . '">' . "\n";
	
		for( $i=0; $i<$eat_and_drink_featured_items; ++$i ){
			echo "\t\t\t\t\t\t\t\t\t\t" . '<li class="featured-item">' . "\n";

			if( $eat_and_drink_featured_items > 1 && $i < 1 ){
				echo '<span class="featured-arrow featured-right"><svg><use xlink:href="#icon-arrow-left-black"></use></svg></span>';
			}else if( $eat_and_drink_featured_items > 1 && $i <= 1){
				echo '<span class="featured-arrow featured-left"><svg><use xlink:href="#icon-arrow-left-black"></use></svg></span>';
			}


			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a href="' . esc_url( $eat_and_drink_nav_items['eat_and_drink_category']['featured-items'][$i]['post_link'] ) . '" class="post-link">' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t\t" . '<span class="post-link">' . $eat_and_drink_nav_items['eat_and_drink_category']['featured-items'][$i]['post_title'] . '</span>' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t\t" . $eat_and_drink_nav_items['eat_and_drink_category']['featured-items'][$i]['post_thumbnail']. "\n"; 
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '</a>' . "\n"; 
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<a href="' . esc_url( get_author_posts_url($eat_and_drink_nav_items['eat_and_drink_category']['featured-items'][$i]['post_author']) ) . '" class="author-link">By ' . get_the_author_meta('first_name', $eat_and_drink_nav_items['eat_and_drink_category']['featured-items'][$i]['post_author']) . ' '. get_the_author_meta('last_name', $eat_and_drink_nav_items['eat_and_drink_category']['featured-items'][$i]['post_author']) .  '</a>'  . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t\t" . '<span class="post-date">' . date('F d, Y', strtotime($eat_and_drink_nav_items['eat_and_drink_category']['featured-items'][$i]['post_date']) ) . '</span>' . "\n";
			echo "\t\t\t\t\t\t\t\t\t\t" .'</li>' . "\n";			
		}

		echo "\t\t\t\t\t\t\t\t\t" . '</ul>' . "\n";
		
		echo '</div><!-- /featured-posts -->' . "\n";
	}
	
	echo "\t\t\t\t\t\t\t" . '<div class="top-level-mobile-back"><svg><use xlink:href="#icon-nav-arrow"></use></svg>Eat &amp; Drink</div>' . "\n";

	echo "\t\t\t\t\t\t\t\t" . '</div>' . "\n";
	echo "\t\t\t\t\t\t\t" . '</div>' . "\n";
?>
						</li><!-- /eat_and_drink -->

						<li class="nav-item-search"><a href="<?php echo esc_url( home_url('search/') ); ?>"><svg><use xlink:href="#icon-magnifying-glass"></use></svg></a>
							<div class="sub-nav">
								<div class="wrap">
									<?php get_search_form(); ?>
								</div>
							</div>
						</li><!-- /search -->

					</ul>

					<div class="mobile-ssm">
						<?php echo social_media_nav('Connect With Us!'); ?>
					</div>



					
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_id'        => 'static-pages-nav',
							'menu_class'     => 'menu static-pages-nav',
							'container_class'		=> 'static-pages-nav',
						) );
					?>
					


			</div><!-- .wrap -->	
		</nav><!-- #site-navigation -->

		
	</header><!-- #masthead -->

	<div id="content" class="site-content">
		<?php 
			$cpts = array('eat_and_drink', 'explore', 'good_to_know', 'happenings' );
			$exp = array('winery', 'brewery', 'tours');
			$tax = array('explore_category', 'happenings_category', 'good_to_know_category', 'eat_and_drink_category');
			
			//SINGLE TOP LEVEL (EAT&DRINK|EXPLORE|GOOD TO KNOW|HAPPENINGS)
			if( is_singular( $cpts ) )
			{
				get_template_part('template-parts/banners');

			//LANDING WINERY|BREWERY or MAIN EXPERIENCE PAGE
			} else if( is_post_type_archive( $exp ) || is_page('experience') || is_tax(array('winery_category', 'brewery_category') ) ) 
			{
				get_template_part('template-parts/banner', 'experience');

			//SINGLE WINERY|BREWERY	
			} else if( is_singular( $exp ) )
			{
				get_template_part('template-parts/banner', 'winerybrewery');			

			//HOME PAGE	
			} else if( is_front_page() ) {

				get_template_part('template-parts/banner', 'home');
			
			//TOP LEVEL (EAT&DRINK|EXPLORE|GOOD TO KNOW|HAPPENINGS)
			}else if( is_post_type_archive( $cpts ) || is_tax( $tax ) ) {				

				get_template_part('template-parts/banner', 'toplevel');
											
			} else if( is_page('search') || is_search() ) {

				get_template_part('template-parts/banner', 'search');
			
			}else if( is_404() ){

				get_template_part('template-parts/banner', '404');
				
			} else if( is_page_template('tpl-page-mailchimp-raffle.php') ){

			} else{

				get_template_part('template-parts/banner', 'page');

			}
		?>
