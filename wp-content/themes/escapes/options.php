<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}
/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */
function optionsframework_options() {
	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}
	$options = array();
	/* =========================*\
	 *	CONTACT INFO
	\* =========================*/
	$options[] = array(
		'name' => 'Contact Info',
		'type' => 'heading'
	);
		$options[] = array(
			'name' => 'Phone',
			'desc' => '',
			'id' => 'phone',
			'class' => 'mini',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Fax',
			'desc' => '',
			'id' => 'fax',
			'class' => 'mini',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Email',
			'desc' => '',
			'id' => 'email',
			'class' => 'mini',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Street Address',
			'desc' => '',
			'id' => 'address',
			'type' => 'textarea'
		);
	/* =========================*\
	 *	SOCIAL MEDIA ACCOUNTS
	\* =========================*/
	$options[] = array(
		'name' => 'Social Media',
		'type' => 'heading'
	);
		$options[] = array(
			'name' => 'Facebook',
			'desc' => 'Enter complete URL.',
			'id' => 'facebook',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Twitter',
			'desc' => 'Enter complete URL.',
			'id' => 'twitter',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Google+',
			'desc' => 'Enter complete URL.',
			'id' => 'google',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'LinkedIn',
			'desc' => 'Enter complete URL.',
			'id' => 'linkedin',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Pinterest',
			'desc' => 'Enter complete URL.',
			'id' => 'pinterest',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'Instagram',
			'desc' => 'Enter complete URL.',
			'id' => 'instagram',
			'type' => 'text'
		);
		$options[] = array(
			'name' => 'YouTube',
			'desc' => 'Enter complete URL.',
			'id' => 'youtube',
			'type' => 'text'
		);
	/* =========================*\
	 *	PPC
	\* =========================*/
	$options[] = array(
		'name' => 'PPC Settings',
		'type' => 'heading'
	);
		$options[] = array(
			'name' => 'Google Analytics',
			'desc' => '',
			'id' => 'ga_analytics',
			'type' => 'textarea'
		);
		$options[] = array(
			'name' => 'Google Remarketing',
			'desc' => '',
			'id' => 'g_remarketing',
			'type' => 'textarea'
		);
		$options[] = array(
			'name' => 'Call Tracking',
			'desc' => '',
			'id' => 'call_tracking',
			'type' => 'textarea'
		);
		$options[] = array(
			'name' => 'Call Tracking Span Class',
			'desc' => '',
			'id' => 'call_tracking_span_class',
			'type' => 'text',
			'class' => 'mini'
		);
		$options[] = array(
			'name' => 'SnapEngage',
			'desc' => '',
			'id' => 'snap_engage',
			'type' => 'textarea'
		);
	/* =========================*\
	 *	SECTION BACKGROUNDS
	\* =========================*/
	$options[] = array(
		'name' => 'Section Backgrounds',
		'type' => 'heading'
	);	
		$options[] = array(
			'name' => 'Home Banner Video',
			'desc' => 'Enter the ID to the Vimeo video that displays on the home page.',
			'id' => 'home_banner_vimeo_id',
			'type' => 'text'
		);	
		$options[] = array(
			'name' => 'Home Banner Mobile Fallback',
			'desc' => 'Upload the image that displays in place of the home page video on mobile devices.',
			'id' => 'home_banner_bg',
			'type' => 'upload'
		);
		$options[] = array(
			'name' => 'Contact Section',
			'desc' => 'Upload the image that displays in the background of the footer contact form section.',
			'id' => 'contact_section_banner_bg',
			'type' => 'upload'
		);

		
	return $options;
}