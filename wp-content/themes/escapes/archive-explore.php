<?php
/**
 * The template for displaying the experience landing page.
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">				
				<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
				?>
				
				<h1>Things To Do in Temecula</h1>
				<article>
					<p>Temecula Valley Wine Country is ripe with rich history, nightlife, outdoor adventures and excursions that are perfect for people of all ages. Our city is full of amazing wines, brews and events you can <em>escape</em> to and experience. We encourage you to discover the adventures that our beautiful home has to offer. On this page, you can explore a variety of fun things to do in Temecula, California.</p>
				</article>
				
				<?php echo do_shortcode('[caldera_clarity name="explore"]'); ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->


	<?php 

		//See inc/template-tags.php for function definition
		escapes_do_local_favorites( 'explore' ); 
	?>

<?php get_footer(); ?>





