<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Temecula Escapes
 */

?>

<article <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

	
		<div class="entry-meta">
			<?php escapes_posted_on(); ?>
		</div><!-- .entry-meta -->
		
	</header><!-- .entry-header -->

	<?php the_post_thumbnail('medium'); ?>


	<footer class="entry-footer">
		
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->