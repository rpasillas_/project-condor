<?php 
	$page = get_page_by_path('search');
	$search = '';

	if ($page) {
		$search = $page->ID;
	} else {
		$search = null;
	}

	$image = get_field('banner_image', $search); 

?>

<div class="banner search-hero-banner" style="background-image: url(<?php echo $image; ?>);">
		
	<div class="header">
		<span class="h1 page-title">Search</span>
	</div>
		
	<div class="heading-wrap">		
		<div class="header">
			<span class="h1 page-title"><?php printf( esc_html__( 'Search Results for: %s', 'escapes' ), '<span>"' . get_search_query() . '"</span>' ); ?></span>

			<span class="result-link-pipe winery-results-link">|</span> <a href="#winery_results" class="winery-results-link result-link">Winery</a> <span class="result-link-pipe winery-results-link">|</span> <a href="#brewery_results" class="brewery-results-link result-link">Brewery</a> <span class="result-link-pipe brewery-results-link">|</span> <a href="#eat_and_drink_results" class="eat-and-drink-results-link result-link">Eat &amp; Drink</a> <span class="result-link-pipe eat-and-drink-results-link">|</span> <a href="#explore_results" class="explore-results-link result-link">Explore</a> <span class="result-link-pipe explore-results-link">|</span> <a href="#happenings_results" class="happenings-results-link result-link">Happenings</a> <span class="result-link-pipe happenings-results-link">|</span>

		</div>
	</div>
		
</div>