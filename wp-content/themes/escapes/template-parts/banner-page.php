<?php 


$image = get_field('banner_image'); 


$title = 'Company';

if( is_page(568) ){
	$title = 'Advertise With Us';
}else if( is_page(885) ){
	$title = 'Add an Event';
}

?>




<div class="banner experience-hero-banner" style="background-image: url(<?php echo $image; ?>);">
		
	<div class="header">
		<span class="h1 page-title"><?php echo $title; ?></span>
	</div>


	<?php get_template_part('template-parts/experiences', 'navigation'); ?>

		
</div>