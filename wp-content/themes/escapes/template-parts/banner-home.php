<?php $image = get_field('banner_image'); ?>

<div class="banner experience-hero-banner home-hero-banner" style="background-image: url(<?php echo $image; ?>);">
		
	<div class="header">
		<span class="page-title"><span class="new-shine">Escape</span> <span class="roboto">Like a Local</span></span>

		<span class="h2">You don't have to live here to love here. Find out where to stay, what to do and where to go in Temecula and its surrounding areas.</span>
	</div>



	<?php get_template_part('template-parts/experiences', 'navigation'); ?>
		
</div>