<?php
	global $post;
	$id = $post->id;

	$metas = array();

	$all_metas = get_post_meta( $id );

	if( $all_metas['_yoast_wpseo_twitter-description'] )
	{
		$metas['twitter_share_desc'] = $all_metas['_yoast_wpseo_twitter-description'][0];

	}else if($all_metas['_yoast_wpseo_metadesc'])
	{			
		$metas['twitter_share_desc'] = $all_metas['_yoast_wpseo_metadesc'][0]; 
	}else
	{
		$metas['twitter_share_desc'] = get_the_title( $id );
	}


	 
	if( $all_metas['_yoast_wpseo_opengraph-description'] )
	{
		$metas['facebook_share_desc'] = $all_metas['_yoast_wpseo_opengraph-description'][0];

	}else if( $all_metas['_yoast_wpseo_metadesc'] )
	{
		$metas['facebook_share_desc'] = $all_metas['_yoast_wpseo_metadesc'][0];

	}else
	{
		$metas['facebook_share_desc'] = get_the_title( $id );
	}



	if( $all_metas['_yoast_wpseo_metadesc'] ){
		$metas['email_share_desc'] = $all_metas['_yoast_wpseo_metadesc'][0];
	}else{
		$metas['email_share_desc'] = get_the_title( $id );
	}

	$media = get_attached_media( 'image', get_the_ID() );
	$media = current( $media );

	$media_url = wp_get_attachment_thumb_url( $media->ID );
?>



<div class="article-sticky-share">
	<span class="share-text">Share this on:</span>

	<a href="<?php echo esc_url( 'https://www.facebook.com/sharer/sharer.php?u=' . rawurlencode ( get_permalink($id) ) ); ?>" title="Share this greatness on Facebook" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;"><svg><use xlink:href="#icon-facebook-f"></use></svg></a>

	<a href="<?php echo esc_url( 'https://twitter.com/intent/tweet?text=' . urlencode( html_entity_decode( $metas['twitter_share_desc'] ) ) . '&amp;url=' . rawurlencode ( get_permalink($id) ) ); ?>" title="Tweet this awesomeness" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;"><svg><use xlink:href="#icon-twitter-bird"></use></svg></a>

	<a href="https://plus.google.com/share?url=<?php echo rawurlencode ( get_permalink($id) ); ?>" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;"><svg><use xlink:href="#icon-"></use></svg></a>


	<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo rawurlencode ( get_permalink($id) ); ?>&title=<?php echo $metas['email_share_desc']; ?>&summary=<?php echo get_the_excerpt($id); ?>&source=<?php echo rawurlencode( home_url() ); ?>" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;"><svg><use xlink:href="#icon-"></use></svg></a>


	<a href="https://pinterest.com/pin/create/button/?url=<?php echo rawurlencode ( get_permalink($id) ); ?>&media=<?php echo rawurlencode (  esc_url( $media_url ) ); ?>&description=<?php echo html_entity_decode( $post_title ); ?>" onclick="window.open(this.href, 'targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, top=150, left=0, width=600, height=300' ); return false;"><svg><use xlink:href="#icon-"></use></svg></a>


	<a href="mailto:?subject=<?php echo html_entity_decode( $post_title ) . '&body=' . get_the_excerpt($id). $metas['email_share_desc']; ?>"><svg><use xlink:href="#icon-"></use></svg></a>
			



</div>









