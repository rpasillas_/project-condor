<?php 
/**
 * Template part for displaying the experience landing page
 * main hero banner.
 *
 *
 * @package Temecula Escapes
 */

	global $post;

	$image = get_field('experience_banner', 'options');

?>
<div class="banner experience-hero-banner" style="background-image: url(<?php echo $image; ?>);">

		
		<div class="header">
			<span class="h1 page-title">Experience</span>
		</div>

		<?php get_template_part('template-parts/experiences', 'navigation'); ?>
		
	
</div>