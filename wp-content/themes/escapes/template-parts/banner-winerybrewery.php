<?php 
/**
 * Template part for displaying a winery or brewery's
 * main hero banner.
 *
 *
 * @package Temecula Escapes
 */


	$image = get_field('banner_image');
?>

<div class="banner profile-hero-banner" style="background-image: url(<?php echo $image; ?>);">
		
	<div class="logo-wrap"><img src="<?php the_field('logo'); ?>" alt="<?php the_title(); ?> Logo"></div>

	<div class="heading-wrap">
		<div class="header">
			<div class="h1 page-title"><?php the_title(); ?></div>
		</div>			
	</div>

</div>