<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Temecula Escapes
 */

?>

<article <?php post_class(); ?> data-test="content-event-feature-profile.php">
	

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	
	<?php
		if( have_rows('event_schedule') ):
			$i = 1;

			echo '<div class="schedule">' . "\n";
			echo "\t" . '<span class="h1">This event\'s schedule and lineup is:</span>' . "\n";
			echo "\t" . '<div class="schedule-inner tabbed-ui">' . "\n";
			

			echo "\t\t" . '<div class="tabs">' . "\n";
		 	// loop through the rows of data
		    while ( have_rows('event_schedule') ) : the_row();
		    	echo '<a class="tab day-tab-' . $i . '" title="Curious about that day ' . $i . ' eh?">Day ' . $i . '</a>' . "\n";
		        ++$i;
		    endwhile;
		    echo "\t\t" . '</div>' . "\n"; //.tabs

		    $i = 1;
		    echo "\t\t" . '<div class="days">' . "\n";

		    // loop through the rows of data
		    while ( have_rows('event_schedule') ) : the_row();
		    	echo '<div class="day day-' . $i . '">' . "\n";
		    	the_sub_field( 'description' );
		    	echo '</div>' . "\n";
		        ++$i;
		    endwhile;

		    echo "\t\t" . '</div>' . "\n"; //.days
		    echo "\t" . '</div>'; //.schedule-inner
		    echo '</div>'; //.schedule

	    endif;


	?>
</article><!-- #post-## -->