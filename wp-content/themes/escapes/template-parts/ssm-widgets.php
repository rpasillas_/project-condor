<div class="wrap ssm-banner">
	<div class="facebook-widget">
		<div class="facebook-title">
			<svg class="facebook-svg"><use xlink:href="#icon-facebook-round"></use></svg>
			<strong>Facebook</strong>
		</div>

		<div class="fb-page" data-href="<?php the_field('te_facebook_profile', 'options'); ?>" data-tabs="timeline" data-width="480" data-height="480" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
			<blockquote cite="<?php the_field('te_facebook_profile', 'options'); ?>" class="fb-xfbml-parse-ignore">
				<a href="<?php the_field('te_facebook_profile', 'options'); ?>">Temecula Escapes Facebook</a>
			</blockquote>
		</div>

	</div>

	<div class="instagram-widget">
		<div class="instagram-title">
			<svg class="instagram-svg"><use xlink:href="#icon-instagram-round"></use></svg>
			<strong>Instagram</strong>
			<a href="https://www.instagram.com/temeculaescapes/">@temeculaescapes</a>
		</div>
		
		<?php dynamic_sidebar( 'home-ssm' ); ?>
	</div>
</div>