<div class="banner banner-404">
	
	<div class="wrap">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/hot-air-balloon-404.jpg" alt="404 Hot Air Balloon">

		<div class="text">
			<span class="h1 new-shine">Oops</span>
			<span class="h2">Looks like you veered off the path. Perhaps we can guide you <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="new-shine">home.</a>
		</div>
	</div>	
</div>