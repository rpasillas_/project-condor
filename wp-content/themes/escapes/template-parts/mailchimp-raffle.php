<?php
	//GET THE MAILCHIMP SEGMENT MEMBERS
	//FROM A CURL REQUEST
	$var = teMailChimpListSegmentMembers();
?>

<div class="raffle-header">
	<svg class="main-logo"><use xlink:href="#icon-temecula-escapes-color-alt"></use></svg>
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/vssn-logo.png" class="vssn-logo">
</div>

<div class="screen-one screen">
	<span class="h2">Winter Wonderland 2016</span>
	<span class="h1">Gift Basket Raffle</span>

	<div class="participants">
		<span class="participants-number">0</span>
		<span class="participants-label">Participants</span>
	</div>

	<div class="big-stupid-button">
		<svg class="btn-foreground"><use xlink:href="#icon-big-stupid-button-1"></use></svg>
		<svg class="btn-top"><use xlink:href="#icon-big-stupid-button-2"></use></svg>
		<svg class="btn-bottom"><use xlink:href="#icon-big-stupid-button-3"></use></svg>
	</div>

</div>

<div class="screen-two screen"></div>

<div class="screen-three screen">
	<span class="count-down">10</span>
</div>

<div class="screen-four screen">
	<div class="winner">
		<h3 class="winner-title">Winner!</h3>
		<span class="winner-name"></span>
		<span class="winner-email"></span>
	</div>
	<a href="<?php echo site_url(); ?>/wp-json/raffle/v1/send_winner" class="confirm-winner" style="position: fixed; bottom: 0; right: 20%; padding: 2px 22px 0;">Confirm Winner</a>
</div>

<script>
	( function( $ ) {
		var data = <?php echo $var; ?>,
		segment_total = data.total_items,
		returned_total = data.members.length,
		random,
		winner_email,
		winner_name;

		console.log( 'Members in MC Segment: ' + segment_total );
		console.log( 'Members Counted: ' + returned_total );

		//PRODUCE THE RANDOM WINNER
		random = Math.floor( Math.random() * returned_total ) + 1;

		//PUT THE WINNER'S INFO TOGETHER
		winner_email = data.members[random].email_address;
		winner_name = data.members[random].merge_fields.FNAME + ' ' + data.members[random].merge_fields.LNAME

		$('.winner-name').html(winner_name);
		$('.winner-email').html(winner_email);

		$(document).ready(function(){
			$('.screen').not('.screen-one').css('opacity', 0);

			//ANIMATE THE NUMBER OF PARTICIPANTS
			$('.participants-number').each(function(){
				$(this).prop('Counter',0).animate({
					Counter: $(this).html(returned_total).text()
				},{
					duration: 3000,
					easing: 'swing',
					step: function(now){
						$(this).text(Math.ceil(now));
					}
				});
			});


			//ANIMATE THE BUTTON CLICK
			$('.big-stupid-button').click(function(){
				$('.btn-top').addClass('press');
				setTimeout(function(){ $('.btn-top').removeClass('press') }, 600);

				//CALL THE ANIMATION
				setTimeout(function(){ doScreenOne() }, 1000);
			});


			var tl_1 = new TimelineMax();
			var tl_2 = new TimelineMax();
			var tl_4 = new TimelineMax();

			function doScreenOne(){
				tl_1.addLabel('opening-title', 0);
				tl_1.addLabel('opening-participants', .25);
				tl_1.addLabel('opening-button', .25);
				tl_1.eventCallback('onComplete', doScreenTwo);


				tl_1.add( TweenMax.to( '.h2', .500, {y: -1000, ease:Elastic.easeIn.config(.5,.9)} ), 'opening-title' );
				tl_1.add( TweenMax.to( '.h1', .500, {y: -1000, ease:Elastic.easeIn.config(.5,.9), delay: .15} ), 'opening-title' );

				tl_1.add( TweenMax.to( '.participants', .500, {y: -1000, ease:Elastic.easeIn.config(.5,.9)} ), 'opening-participants' );

				tl_1.add( TweenMax.to( '.big-stupid-button', .500, {y: 1000, ease:Elastic.easeIn.config(.5,.9)} ), 'opening-button' );
			}

			function doScreenTwo(){				
				tl_2.eventCallback('onComplete', doScreenThree);

				var $contestantsArray = $('.raffle-contestant').toArray();
				var $contestants = $('.raffle-contestant');
				var $contestantsRev = $contestantsArray.reverse();

				tl_2.to('.screen-two', .500, { opacity: 1 });
				tl_2.staggerFrom($contestants, .300, { y: 2000, ease:Elastic.easeOut.config(.5,.9) }, .05 );
				tl_2.staggerTo($contestantsRev, .300, { y: 2000, ease:Elastic.easeIn.config(.5,.9) }, .05, "+=4" );
			}

			function doScreenThree(){
				$('.screen-three').animate({'opacity': 1});

				var sec = 10;
				var timer = setInterval(function() { 
					$('.count-down').text(sec--);
					if (sec == -1) {
						clearInterval(timer);
						setTimeout( function(){$('.count-down').animate({'opacity' : 0})}, 1);
						setTimeout(doScreenFour, .5);
					} 
				}, 1000);
			}

			function doScreenFour(){				
				tl_4.addLabel('opening-title', 0);
				tl_4.eventCallback('onComplete', function(){ 
					$('.screen-four').css('z-index', 1000);
				});

				tl_4.add( TweenMax.to( '.screen-four', .500, {opacity: 1} ), 'opening-title' );
				tl_4.add( TweenMax.to( '.winner-name', .500, {scaleX: 1, scaleY: 1, delay: .15} ), 'opening-title' );
				tl_4.add( TweenMax.to( '.winner-email', .500, {scaleX: 1, scaleY: 1, delay: .2} ), 'opening-title' );
				tl_4.add( TweenMax.to( '.winner-title', .500, {opacity: 1, delay: .5} ), 'opening-title' );
			}

			$('.confirm-winner').click(function(e){
				e.preventDefault();
				var url = $(this).attr('href');

				$.ajax({
					url: url,
					data:{
						'winner_name': winner_name,
						'winner_email': winner_email
					}
				}).done(function(){
					alert('Confirmed.');
				});
			});

		});//document ready
		




		//LOOP THROUGH ALL THE ENTRIES
		$.each( data.members, function(i, v){
			//console.log(v.merge_fields.FNAME + ' ' + v.merge_fields.LNAME);
			$('.screen-two').append('<span class="raffle-contestant">' +   v.merge_fields.FNAME + ' ' + v.merge_fields.LNAME + '</span>');
		});


		//DO SOMETHING WITH THE WINNER
		console.log( 'The Winner: ' + winner_name + ' ' + winner_email );


	} )( jQuery );
</script>