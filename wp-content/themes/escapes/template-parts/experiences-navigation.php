<?php
	$winery_selected = '';
	$brewery_selected = '';
	$tours_selected = '';
	$events_selected = '';
	$none_selected = ' class="none-selected';

	if( is_post_type_archive( 'winery' ) || is_singular( 'winery' ) )
	{
		$winery_selected = ' class="current-item"';
		$none_selected = ' class="winery-selected"';

	} else if( is_post_type_archive( 'brewery' ) || is_singular( 'brewery' ) )
	{
		$brewery_selected = ' class="current-item"';
		$none_selected = ' class="brewery-selected"';

	} else if( is_post_type_archive( 'tours' ) || is_singular( 'tours' ) )
	{
		$tours_selected = ' class="current-item"';
		$none_selected = ' class="tours-selected"';

	} else if( is_post_type_archive( 'happenings' ) || is_singular( 'happenings' ) ){

		$events_selected = ' class="current-item"';
		$none_selected = ' class="happenings-selected"';

	} else{
		$none_selected .= '"';
	}
?>
<div class="experience-section-nav-wrap">
	<span class="h3">Discover Your Experience</span>
	<nav class="experience-section-nav">
		<ul<?php echo $none_selected; ?>>
			<li<?php echo $winery_selected; ?>><a href="<?php echo esc_url( home_url('/experience/temecula-wineries/') ); ?>">Wineries</a></li>
			<li<?php echo $brewery_selected; ?>><a href="<?php echo esc_url( home_url('/experience/temecula-breweries/') ); ?>">Breweries</a></li>
			<li<?php echo $events_selected; ?>><a href="<?php echo esc_url( home_url('/temecula-events/') ); ?>">Happenings</a></li>
		</ul>
		<span class="nav-trigger"><svg><use xlink:href="#icon-arrow-left-black"></use></svg></span>
	</nav>
</div>