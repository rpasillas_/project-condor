<?php 
/**
 * Template part for displaying the top level pages
 * main hero banner.
 *
 *
 * @package Temecula Escapes
 */

	global $post;

	if( is_post_type_archive( 'eat_and_drink' ) || is_tax('eat_and_drink_category') ){

		$image = get_field('eat_and_drink_banner_image', 'options');
		$title = 'Eat &amp; Drink';

	} else if( is_post_type_archive( 'explore' ) || is_tax('explore_category') ) {

		$image = get_field('explore_banner_image', 'options');
		$title = 'Explore';

	} else if( is_post_type_archive( 'good_to_know' ) || is_tax('good_to_know_category') ) {

		$image = get_field('good_to_know_banner_image', 'options');
		$title = 'Good to Know';

	} else{

		$image = get_field('happenings_banner_image', 'options');
		$title = 'Happenings';

	}


	

?>
<div class="banner top-level-hero-banner" style="background-image: url(<?php echo $image; ?>);">
		
	<div class="header">
		<div class="h1 page-title"><?php echo $title; ?></div>
	</div>

	<?php get_template_part('template-parts/experiences', 'navigation'); ?>
		
</div>