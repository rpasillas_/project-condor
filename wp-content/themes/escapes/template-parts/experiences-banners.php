<?php
/**
 * Template part for displaying the experiences carousel
 * 
 *
 *
 * @package Temecula Escapes
 */

	$array = array(
			'brewery' 	=> 'Breweries',
			'winery' 	=> 'Wineries',
			'events' 	=> 'Happenings',
		);

?>
<div id="parallax" class="experiences-banners owl-carousel">

	<?php
		foreach( $array as $k=>$v) { 
			$title = $v;
			$text = get_field('banner_text_' . $k, 'options'); 
			$image = get_field('banner_image_' . $k, 'options');

			if( $text && $image ) { 
	?>
			<div class="experience-banner">
				<div class="banner-content">
					<span class="h1"><?php echo $title; ?></span>
					<div class="banner-text">
						<p><?php echo $text; ?></p>
					</div>

					<?php
						if( $k == 'brewery' ){
							$link = '/experience/temecula-breweries';
						}else if( $k == 'winery' ){
							$link = '/experience/temecula-wineries';
						}else{
							$link = '/temecula-events';
						}
					?>

					<a href="<?php echo esc_url( home_url() ) . $link; ?>" class="btn btn-outline btn-white">Read More</a>



				</div>
				<div class="parallax-bg" style="background-image: url(<?php echo $image; ?>);"></div>
			</div>

		<?php }//if ?>
	<?php }//foreach ?>

</div>