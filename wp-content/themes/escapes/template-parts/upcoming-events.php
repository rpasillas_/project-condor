<div class="te-latest-events">
	<span class="h1">Upcoming Events</span>
	<ul>
	<?php		
		date_default_timezone_set("America/Los_Angeles");

		$args = array(
					'post_type' 		=> 'happenings',
					'post_status'		=> 'publish',
			        'orderby' 			=> 'meta_value',
			        'meta_key' 			=> 'event_end_date',
			        'order' 			=> 'ASC',
			        'posts_per_page' 	=> 3,
			        'meta_query'		=> array(
			        		0 => array(
			        				'key' => 'event_end_date',
			        				'value' => date("Y-m-d H:i"),
			        				'compare' => '>=',
			        				'type' => 'DATE'
			        			),
			        		1 => array(
			        				'key' => 'make_event',
			        				'value' => 1,
			        				'compare' => '=',
			        				'type' => 'NUMERIC'
			        			)
			        	)
				);

		$events_posts = get_posts( $args );
		foreach ( $events_posts as $post ) : setup_postdata( $post ); 
		
			if( has_post_thumbnail() ){
				$image = get_the_post_thumbnail( $post->ID, 'thumbnail', array( 'class' => 'thumbnail' ) );
			}else{
				$image = '<img src="' . get_template_directory_uri() . '/assets/images/default-pink-fpo.jpg" class="thumbnail" alt="An Event">';
			}
	?>

		<li itemscope itemtype="http://schema.org/Event">
			<a href="<?php the_permalink(); ?>"><?php echo $image; ?></a>
			<span class="h3" itemprop="name"><?php the_title(); ?></span>

			<div class="time">Starts: <time itemprop="startDate" datetime="<?php the_field('event_date'); ?>"><?php echo date( 'F d, Y', strtotime(get_field('event_date')) ); ?></time></div>

			<div class="time">Ends: <time itemprop="endDate" datetime="<?php the_field('event_end_date'); ?>"><?php echo date( 'F d, Y', strtotime(get_field('event_end_date')) ); ?></time></div>

			<a href="<?php the_permalink(); ?>" class="read-more" itemprop="url">Read More</a>
		</li>

	<?php 
		endforeach; 
		wp_reset_postdata();
	?>

	</ul>

	<a href="<?php echo esc_url( home_url('temecula-events/') ); ?>" class="more-events"><svg><use xlink:href="#icon-calendar"></use></svg>See Full Calendar of Events</a>
</div>