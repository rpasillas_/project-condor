<?php 
	global $post;

	$image = get_field('banner_image');

?>

<div class="banner" style="background-image: url(<?php echo $image; ?>);">
	<div class="heading-wrap">
		
		<div class="header">
			<div class="h1 page-title"><?php single_post_title(); ?></div>
		</div>
		<?php 
			if( get_field( 'company_website' ) )
			{
				echo '<span class="company-website">' . get_field('company_website') . '</span>';
			}

			if( is_singular( 'post' ) )
			{
				$author_id = get_post_field ('post_author', $post_id);
				echo '<a href="' . esc_url( get_author_posts_url(  $author_id ))  . '" class="author-link">By ' . get_the_author_meta('first_name', $author_id)  . ' '. get_the_author_meta('last_name', $author_id) .  '</a>';
				echo ' | ';
				echo get_the_date( 'F d, Y', $post->ID );

			}
		?>
	</div>
</div>