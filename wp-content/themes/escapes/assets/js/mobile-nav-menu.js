( function( $ ) {

    //-- MAIN MOBILE SUB-NAVIGATION
    $('.nav-item-js').on('click', '.submenu-toggle', function(){
        var $el = $(this);

        $el.parent().toggleClass('nav-open');
    });

    //-- MAIN MOBILE NAVIGATION
    $('.toggle-navigation').on('click', function(e){
    	var $el = $(this);

    	e.preventDefault();   	
    	$('.main-navigation').toggleClass('on');
    });

    //-- MOBILE 1ST LEVEL 
    $('.top-level-mobile-back').on('click', function(e){
        $(this).closest('.sub-nav').prev('.submenu-toggle').trigger('click');
    });

    //-- MOBILE 2ND LEVEL  (OPEN)
    $('.term-name').on('click', 'svg', function(e){                         
        $(this).closest('.sub-nav').addClass('third-level').scrollTop(0);       
        $(this).parent().next('.term-name-duplicate').toggleClass('on');
        $(this).parent().next().next('.term-post').toggleClass('on'); 
    });

    //-- MOBILE 2ND LEVEL  (CLOSE)
    $('.term-name-duplicate').on('click', function(e){  
        $('.term-post').removeClass('on');
        $('.term-name-duplicate').removeClass('on');
        $(this).closest('.sub-nav').removeClass('third-level').scrollTop(0);
        $(this).parent().next('.term-post').removeClass('on');      
    });

    //-- MOBILE EXPERIENCE SUB-NAV
    $('.experience-mobile-back').on('click', function(){
        $('.nav-item-experience').find('.submenu-toggle').trigger('click');
    });

    //-- MOBILE FEATURED ITEMS:: MOVE RIGHT
    $('.featured-right').on('click', function(){
        $(this).closest('.featured-items').addClass('move-right');
    });

    //-- MOBILE FEATURED ITEMS:: MOVE LEFT
    $('.featured-left').on('click', function(){
        $(this).closest('.featured-items').removeClass('move-right');
    });

    //-- EXPERIENCES MOBILE NAVIGATION
    $('nav.experience-section-nav').on('click', '.nav-trigger', function(e){
        var $el = $(this);

        $el.closest('nav.experience-section-nav').toggleClass('on');
    });


} )( jQuery );