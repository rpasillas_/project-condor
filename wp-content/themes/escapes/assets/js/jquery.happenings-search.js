( function( $ ) {

	var category,
		location,
		date,
		page;

	$('#happenings-search').submit(function(e){
		e.preventDefault();

		var $el = $(this)

		getFormValues();

		//Since this is a fresh search, clear out any old results
		$('#show-more-results').find('.posts').empty();
		doAJAXSearch( category, location, date, page );	
	});//#happenings-search

	$('.load-more').on('click', function(){

		//SEND THE PAGE NUMBER
		page = $('.load-more').attr('data-page');
		var next_page = ++page;
		getFormValues();

		//ARE WE LOADING MORE DEFAULT POSTS?
		if( $(this).attr('data-default') ){
			doAJAXDefault( page );
		}else{
			doAJAXSearch( category, location, date, page );
		}
		
		$('.load-more').attr('data-page', next_page);
	});

	//Get the form values
	function getFormValues(){
		category = $('#happenings-search').find('#happenings-category').val();	
		location = $('#happenings-search').find('#happenings-location').val();
		date = $('#happenings-search').find('#happenings-date').val();	
	}

	//Perform the AJAX Search
	function doAJAXSearch( $category, $location, $date, $page ){
		$('.loading').show();
		$('.no-results-wrap').empty();

		$.getJSON({
			url: '/wp-json/happenings-search/v1/search/',
			dataType: 'json',
			data:{
				'happenings-location' : $location,
				'happenings-category' : $category,
				'happenings-date' : $date,
				'page'	: $page
			}

		}).done( function( data ){
			$('.loading').css('display', 'none');

			var source = $('#happenings-results').html();
			var template = Handlebars.compile(source);
			var html = template(data);
			$('#show-more-results').find('.posts').append('<div class="grid-sizer"></div>' + html);
				
			var $grid = $('.masonry-tiles').masonry({
				itemSelector: '.masonry-grid-item',
				columnWidth: '.grid-sizer',
				gutter: 10,
				percentPosition: true			
			});

			$grid.masonry('reloadItems');
			$grid.masonry('layout');

			if( data.next_page ){
				$('.load-more-wrap').css('display', 'block');
			}else{
				$('.load-more-wrap').css('display', 'none');
			}			
			
		}).fail( function( jqxhr, textStatus, error ){
			var err = textStatus + ', ' + error;
			console.log( 'Request Failed: ' + err );

			$('.loading').css('display', 'none');
			
			//DO THE DEFAULT SEARCH INSTEAD
			doAJAXDefault();
			$('.no-results-wrap').empty().append('<div class="no-results">We can\'t find any results, but please enjoy these other results.</div>');
		});//getJSON
	}

	//Default AJAX Search
	function doAJAXDefault( $page ){
		$('.loading').show();
		$('.load-more').attr('data-default','true');
		$('.no-results-wrap').empty();

		$.getJSON({
			url: '/wp-json/happenings-search/v1/default/',
			dataType: 'json',
			data:{ 'page' : $page }

		}).done( function( data ){
			$('.loading').css('display', 'none');

			var source = $('#happenings-results').html();
			var template = Handlebars.compile(source);
			var html = template(data);
			$('#show-more-results').find('.posts').append('<div class="grid-sizer"></div>' + html);
				
			var $grid = $('.masonry-tiles').masonry({
				itemSelector: '.masonry-grid-item',
				columnWidth: '.grid-sizer',
				gutter: 10,
				percentPosition: true			
			});

			$grid.masonry('reloadItems');
			$grid.masonry('layout');

			if( data.next_page ){
				$('.load-more-wrap').css('display', 'block');
			}else{
				$('.load-more-wrap').css('display', 'none');
			}			
			
		}).fail( function( jqxhr, textStatus, error ){
			var err = textStatus + ', ' + error;
			console.log( 'Request Failed: ' + err );

			$('.loading').css('display', 'none');
			$('.no-results-wrap').empty().append('<div class="no-results">Sorry No Results</div>');

		});//getJSON
	}

	$(document).ready(function(){
		doAJAXDefault();
	});

} )( jQuery );