( function( $ ) {

    $(document).ready(function(){
        $('#happenings-category').autoComplete({
            source: function(term, response){
                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: '/wp-admin/admin-ajax.php',
                    data: 'action=get_taxonomy_terms&term=' + term,
                    success: function(data){
                        response(data);
                    }                   
                });
            }

        });

    });

} )( jQuery );