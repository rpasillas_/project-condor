( function( $ ) {

	$('.tabbed-ui').find('.day-tab-1').addClass('on');
	$('.tabbed-ui').find('.day-1').addClass('on');

	function doTabs( el ){	
		var tabHeight = 44;	
		tabHeight += el.height();
		$('.tabbed-ui').find('.days').height( tabHeight );
	}
	
	doTabs( $('.tabbed-ui').find('.day-1') );


	$('.tab').click(function(){
		$('.tab').removeClass('on');
		$('.tabbed-ui').find('.day').removeClass('on');

		var el = $(this);
		el.addClass('on');

		var regex = /\d{1}/;

		var num = el.attr('class').match(regex);
		$('.day-' + num ).addClass('on');
		

		doTabs( $('.day-' + num ) );
	});

} )( jQuery );