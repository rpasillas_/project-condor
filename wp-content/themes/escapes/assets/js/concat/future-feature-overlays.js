( function( $ ) {
	var timer;

	$('.js-account').click(function(e){
		e.preventDefault();

		$('#account-popup').addClass('on');
		$('.feature-popup-outer-container').addClass('on');

		timer = window.setTimeout(hidePop, 4000);
	});

	$('.js-favorites').click(function(e){
		e.preventDefault();

		$('#favorites-popup').addClass('on');
		$('.feature-popup-outer-container').addClass('on');

		timer = window.setTimeout(hidePop, 4000);
	});

	$('.js-itinerary').click(function(e){
		e.preventDefault();

		$('#brewery-popup').addClass('on');
		$('.feature-popup-outer-container').addClass('on');

		timer = window.setTimeout(hidePop, 4000);
	});

	$('.feature-popup').on('click', '.close', function(){

		$('.feature-popup-outer-container').animate({
			top: '-900',
			opacity: 0
		}, 500, function(){
			$(this).parent().removeClass('on'); 
			$('.feature-popup').removeClass('on');
			$('.feature-popup-outer-container').css({'top': 0, 'opacity': 1}).removeClass('on');
		});

		window.clearTimeout(timer);	
	});


	function hidePop(){
		$('.feature-popup .close').trigger('click');		
	}


} )( jQuery );