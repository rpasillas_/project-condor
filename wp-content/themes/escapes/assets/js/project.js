/**
 * Featherlight - ultra slim jQuery lightbox
 * Version 1.5.1 - http://noelboss.github.io/featherlight/
 *
 * Copyright 2016, Noël Raoul Bossart (http://www.noelboss.com)
 * MIT Licensed.
**/
!function(a){"use strict";function b(a,c){if(!(this instanceof b)){var d=new b(a,c);return d.open(),d}this.id=b.id++,this.setup(a,c),this.chainCallbacks(b._callbackChain)}if("undefined"==typeof a)return void("console"in window&&window.console.info("Too much lightness, Featherlight needs jQuery."));var c=[],d=function(b){return c=a.grep(c,function(a){return a!==b&&a.$instance.closest("body").length>0})},e=function(a,b){var c={},d=new RegExp("^"+b+"([A-Z])(.*)");for(var e in a){var f=e.match(d);if(f){var g=(f[1]+f[2].replace(/([A-Z])/g,"-$1")).toLowerCase();c[g]=a[e]}}return c},f={keyup:"onKeyUp",resize:"onResize"},g=function(c){a.each(b.opened().reverse(),function(){return c.isDefaultPrevented()||!1!==this[f[c.type]](c)?void 0:(c.preventDefault(),c.stopPropagation(),!1)})},h=function(c){if(c!==b._globalHandlerInstalled){b._globalHandlerInstalled=c;var d=a.map(f,function(a,c){return c+"."+b.prototype.namespace}).join(" ");a(window)[c?"on":"off"](d,g)}};b.prototype={constructor:b,namespace:"featherlight",targetAttr:"data-featherlight",variant:null,resetCss:!1,background:null,openTrigger:"click",closeTrigger:"click",filter:null,root:"body",openSpeed:250,closeSpeed:250,closeOnClick:"background",closeOnEsc:!0,closeIcon:"&#10005;",loading:"",persist:!1,otherClose:null,beforeOpen:a.noop,beforeContent:a.noop,beforeClose:a.noop,afterOpen:a.noop,afterContent:a.noop,afterClose:a.noop,onKeyUp:a.noop,onResize:a.noop,type:null,contentFilters:["jquery","image","html","ajax","iframe","text"],setup:function(b,c){"object"!=typeof b||b instanceof a!=!1||c||(c=b,b=void 0);var d=a.extend(this,c,{target:b}),e=d.resetCss?d.namespace+"-reset":d.namespace,f=a(d.background||['<div class="'+e+"-loading "+e+'">','<div class="'+e+'-content">','<span class="'+e+"-close-icon "+d.namespace+'-close">',d.closeIcon,"</span>",'<div class="'+d.namespace+'-inner">'+d.loading+"</div>","</div>","</div>"].join("")),g="."+d.namespace+"-close"+(d.otherClose?","+d.otherClose:"");return d.$instance=f.clone().addClass(d.variant),d.$instance.on(d.closeTrigger+"."+d.namespace,function(b){var c=a(b.target);("background"===d.closeOnClick&&c.is("."+d.namespace)||"anywhere"===d.closeOnClick||c.closest(g).length)&&(d.close(b),b.preventDefault())}),this},getContent:function(){if(this.persist!==!1&&this.$content)return this.$content;var b=this,c=this.constructor.contentFilters,d=function(a){return b.$currentTarget&&b.$currentTarget.attr(a)},e=d(b.targetAttr),f=b.target||e||"",g=c[b.type];if(!g&&f in c&&(g=c[f],f=b.target&&e),f=f||d("href")||"",!g)for(var h in c)b[h]&&(g=c[h],f=b[h]);if(!g){var i=f;if(f=null,a.each(b.contentFilters,function(){return g=c[this],g.test&&(f=g.test(i)),!f&&g.regex&&i.match&&i.match(g.regex)&&(f=i),!f}),!f)return"console"in window&&window.console.error("Featherlight: no content filter found "+(i?' for "'+i+'"':" (no target specified)")),!1}return g.process.call(b,f)},setContent:function(b){var c=this;return(b.is("iframe")||a("iframe",b).length>0)&&c.$instance.addClass(c.namespace+"-iframe"),c.$instance.removeClass(c.namespace+"-loading"),c.$instance.find("."+c.namespace+"-inner").not(b).slice(1).remove().end().replaceWith(a.contains(c.$instance[0],b[0])?"":b),c.$content=b.addClass(c.namespace+"-inner"),c},open:function(b){var d=this;if(d.$instance.hide().appendTo(d.root),!(b&&b.isDefaultPrevented()||d.beforeOpen(b)===!1)){b&&b.preventDefault();var e=d.getContent();if(e)return c.push(d),h(!0),d.$instance.fadeIn(d.openSpeed),d.beforeContent(b),a.when(e).always(function(a){d.setContent(a),d.afterContent(b)}).then(d.$instance.promise()).done(function(){d.afterOpen(b)})}return d.$instance.detach(),a.Deferred().reject().promise()},close:function(b){var c=this,e=a.Deferred();return c.beforeClose(b)===!1?e.reject():(0===d(c).length&&h(!1),c.$instance.fadeOut(c.closeSpeed,function(){c.$instance.detach(),c.afterClose(b),e.resolve()})),e.promise()},resize:function(a,b){if(a&&b){this.$content.css("width","").css("height","");var c=Math.max(a/(parseInt(this.$content.parent().css("width"),10)-1),b/(parseInt(this.$content.parent().css("height"),10)-1));c>1&&(c=b/Math.floor(b/c),this.$content.css("width",""+a/c+"px").css("height",""+b/c+"px"))}},chainCallbacks:function(b){for(var c in b)this[c]=a.proxy(b[c],this,a.proxy(this[c],this))}},a.extend(b,{id:0,autoBind:"[data-featherlight]",defaults:b.prototype,contentFilters:{jquery:{regex:/^[#.]\w/,test:function(b){return b instanceof a&&b},process:function(b){return this.persist!==!1?a(b):a(b).clone(!0)}},image:{regex:/\.(png|jpg|jpeg|gif|tiff|bmp|svg)(\?\S*)?$/i,process:function(b){var c=this,d=a.Deferred(),e=new Image,f=a('<img src="'+b+'" alt="" class="'+c.namespace+'-image" />');return e.onload=function(){f.naturalWidth=e.width,f.naturalHeight=e.height,d.resolve(f)},e.onerror=function(){d.reject(f)},e.src=b,d.promise()}},html:{regex:/^\s*<[\w!][^<]*>/,process:function(b){return a(b)}},ajax:{regex:/./,process:function(b){var c=a.Deferred(),d=a("<div></div>").load(b,function(a,b){"error"!==b&&c.resolve(d.contents()),c.fail()});return c.promise()}},iframe:{process:function(b){var c=new a.Deferred,d=a("<iframe/>");return d.hide().attr("src",b).css(e(this,"iframe")).on("load",function(){c.resolve(d.show())}).appendTo(this.$instance.find("."+this.namespace+"-content")),c.promise()}},text:{process:function(b){return a("<div>",{text:b})}}},functionAttributes:["beforeOpen","afterOpen","beforeContent","afterContent","beforeClose","afterClose"],readElementConfig:function(b,c){var d=this,e=new RegExp("^data-"+c+"-(.*)"),f={};return b&&b.attributes&&a.each(b.attributes,function(){var b=this.name.match(e);if(b){var c=this.value,g=a.camelCase(b[1]);if(a.inArray(g,d.functionAttributes)>=0)c=new Function(c);else try{c=JSON.parse(c)}catch(h){}f[g]=c}}),f},extend:function(b,c){var d=function(){this.constructor=b};return d.prototype=this.prototype,b.prototype=new d,b.__super__=this.prototype,a.extend(b,this,c),b.defaults=b.prototype,b},attach:function(b,c,d){var e=this;"object"!=typeof c||c instanceof a!=!1||d||(d=c,c=void 0),d=a.extend({},d);var f,g=d.namespace||e.defaults.namespace,h=a.extend({},e.defaults,e.readElementConfig(b[0],g),d);return b.on(h.openTrigger+"."+h.namespace,h.filter,function(g){var i=a.extend({$source:b,$currentTarget:a(this)},e.readElementConfig(b[0],h.namespace),e.readElementConfig(this,h.namespace),d),j=f||a(this).data("featherlight-persisted")||new e(c,i);"shared"===j.persist?f=j:j.persist!==!1&&a(this).data("featherlight-persisted",j),i.$currentTarget.blur(),j.open(g)}),b},current:function(){var a=this.opened();return a[a.length-1]||null},opened:function(){var b=this;return d(),a.grep(c,function(a){return a instanceof b})},close:function(a){var b=this.current();return b?b.close(a):void 0},_onReady:function(){var b=this;b.autoBind&&(a(b.autoBind).each(function(){b.attach(a(this))}),a(document).on("click",b.autoBind,function(c){c.isDefaultPrevented()||"featherlight"===c.namespace||(c.preventDefault(),b.attach(a(c.currentTarget)),a(c.target).trigger("click.featherlight"))}))},_callbackChain:{onKeyUp:function(b,c){return 27===c.keyCode?(this.closeOnEsc&&a.featherlight.close(c),!1):b(c)},onResize:function(a,b){return this.resize(this.$content.naturalWidth,this.$content.naturalHeight),a(b)},afterContent:function(a,b){var c=a(b);return this.onResize(b),c}}}),a.featherlight=b,a.fn.featherlight=function(a,c){return b.attach(this,a,c)},a(document).ready(function(){b._onReady()})}(jQuery);
( function( $ ) {
	var timer;

	$('.js-account').click(function(e){
		e.preventDefault();

		$('#account-popup').addClass('on');
		$('.feature-popup-outer-container').addClass('on');

		timer = window.setTimeout(hidePop, 4000);
	});

	$('.js-favorites').click(function(e){
		e.preventDefault();

		$('#favorites-popup').addClass('on');
		$('.feature-popup-outer-container').addClass('on');

		timer = window.setTimeout(hidePop, 4000);
	});

	$('.js-itinerary').click(function(e){
		e.preventDefault();

		$('#brewery-popup').addClass('on');
		$('.feature-popup-outer-container').addClass('on');

		timer = window.setTimeout(hidePop, 4000);
	});

	$('.feature-popup').on('click', '.close', function(){

		$('.feature-popup-outer-container').animate({
			top: '-900',
			opacity: 0
		}, 500, function(){
			$(this).parent().removeClass('on'); 
			$('.feature-popup').removeClass('on');
			$('.feature-popup-outer-container').css({'top': 0, 'opacity': 1}).removeClass('on');
		});

		window.clearTimeout(timer);	
	});


	function hidePop(){
		$('.feature-popup .close').trigger('click');		
	}


} )( jQuery );
/**
 * File js-enabled.js
 *
 * If Javascript is enabled, replace the <body> class "no-js".
 */
document.body.className = document.body.className.replace( 'no-js', 'js' );
/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
( function() {
	var isWebkit = navigator.userAgent.toLowerCase().indexOf( 'webkit' ) > -1,
	    isOpera  = navigator.userAgent.toLowerCase().indexOf( 'opera' )  > -1,
	    isIe     = navigator.userAgent.toLowerCase().indexOf( 'msie' )   > -1;

	if ( ( isWebkit || isOpera || isIe ) && document.getElementById && window.addEventListener ) {
		window.addEventListener( 'hashchange', function() {
			var id = location.hash.substring( 1 ),
				element;

			if ( ! ( /^[A-z0-9_-]+$/.test( id ) ) ) {
				return;
			}

			element = document.getElementById( id );

			if ( element ) {
				if ( ! ( /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) ) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false );
	}
})();
( function( $ ) {

	$('.tabbed-ui').find('.day-tab-1').addClass('on');
	$('.tabbed-ui').find('.day-1').addClass('on');

	function doTabs( el ){	
		var tabHeight = 44;	
		tabHeight += el.height();
		$('.tabbed-ui').find('.days').height( tabHeight );
	}
	
	doTabs( $('.tabbed-ui').find('.day-1') );


	$('.tab').click(function(){
		$('.tab').removeClass('on');
		$('.tabbed-ui').find('.day').removeClass('on');

		var el = $(this);
		el.addClass('on');

		var regex = /\d{1}/;

		var num = el.attr('class').match(regex);
		$('.day-' + num ).addClass('on');
		

		doTabs( $('.day-' + num ) );
	});

} )( jQuery );
/**
 * File window-ready.js
 *
 * Add a "ready" class to <body> when window is ready.
 */
window.Window_Ready = {};
( function( window, $, that ) {

	// Constructor.
	that.init = function() {
		that.cache();
		that.bindEvents();
	};

	// Cache document elements.
	that.cache = function() {
		that.$c = {
			window: $(window),
			body: $(document.body),
		};
	};

	// Combine all events.
	that.bindEvents = function() {
		that.$c.window.load( that.addBodyClass );
	};

	// Add a class to <body>.
	that.addBodyClass = function() {
		that.$c.body.addClass( 'ready' );
	};

	// Engage!
	$( that.init );

})( window, jQuery, window.Window_Ready );
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZlYXRoZXJsaWdodC5taW4uanMiLCJmdXR1cmUtZmVhdHVyZS1vdmVybGF5cy5qcyIsImpzLWVuYWJsZWQuanMiLCJza2lwLWxpbmstZm9jdXMtZml4LmpzIiwidGFicy5qcyIsIndpbmRvdy1yZWFkeS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbERBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2hDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzlCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoicHJvamVjdC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogRmVhdGhlcmxpZ2h0IC0gdWx0cmEgc2xpbSBqUXVlcnkgbGlnaHRib3hcbiAqIFZlcnNpb24gMS41LjEgLSBodHRwOi8vbm9lbGJvc3MuZ2l0aHViLmlvL2ZlYXRoZXJsaWdodC9cbiAqXG4gKiBDb3B5cmlnaHQgMjAxNiwgTm/Dq2wgUmFvdWwgQm9zc2FydCAoaHR0cDovL3d3dy5ub2VsYm9zcy5jb20pXG4gKiBNSVQgTGljZW5zZWQuXG4qKi9cbiFmdW5jdGlvbihhKXtcInVzZSBzdHJpY3RcIjtmdW5jdGlvbiBiKGEsYyl7aWYoISh0aGlzIGluc3RhbmNlb2YgYikpe3ZhciBkPW5ldyBiKGEsYyk7cmV0dXJuIGQub3BlbigpLGR9dGhpcy5pZD1iLmlkKyssdGhpcy5zZXR1cChhLGMpLHRoaXMuY2hhaW5DYWxsYmFja3MoYi5fY2FsbGJhY2tDaGFpbil9aWYoXCJ1bmRlZmluZWRcIj09dHlwZW9mIGEpcmV0dXJuIHZvaWQoXCJjb25zb2xlXCJpbiB3aW5kb3cmJndpbmRvdy5jb25zb2xlLmluZm8oXCJUb28gbXVjaCBsaWdodG5lc3MsIEZlYXRoZXJsaWdodCBuZWVkcyBqUXVlcnkuXCIpKTt2YXIgYz1bXSxkPWZ1bmN0aW9uKGIpe3JldHVybiBjPWEuZ3JlcChjLGZ1bmN0aW9uKGEpe3JldHVybiBhIT09YiYmYS4kaW5zdGFuY2UuY2xvc2VzdChcImJvZHlcIikubGVuZ3RoPjB9KX0sZT1mdW5jdGlvbihhLGIpe3ZhciBjPXt9LGQ9bmV3IFJlZ0V4cChcIl5cIitiK1wiKFtBLVpdKSguKilcIik7Zm9yKHZhciBlIGluIGEpe3ZhciBmPWUubWF0Y2goZCk7aWYoZil7dmFyIGc9KGZbMV0rZlsyXS5yZXBsYWNlKC8oW0EtWl0pL2csXCItJDFcIikpLnRvTG93ZXJDYXNlKCk7Y1tnXT1hW2VdfX1yZXR1cm4gY30sZj17a2V5dXA6XCJvbktleVVwXCIscmVzaXplOlwib25SZXNpemVcIn0sZz1mdW5jdGlvbihjKXthLmVhY2goYi5vcGVuZWQoKS5yZXZlcnNlKCksZnVuY3Rpb24oKXtyZXR1cm4gYy5pc0RlZmF1bHRQcmV2ZW50ZWQoKXx8ITEhPT10aGlzW2ZbYy50eXBlXV0oYyk/dm9pZCAwOihjLnByZXZlbnREZWZhdWx0KCksYy5zdG9wUHJvcGFnYXRpb24oKSwhMSl9KX0saD1mdW5jdGlvbihjKXtpZihjIT09Yi5fZ2xvYmFsSGFuZGxlckluc3RhbGxlZCl7Yi5fZ2xvYmFsSGFuZGxlckluc3RhbGxlZD1jO3ZhciBkPWEubWFwKGYsZnVuY3Rpb24oYSxjKXtyZXR1cm4gYytcIi5cIitiLnByb3RvdHlwZS5uYW1lc3BhY2V9KS5qb2luKFwiIFwiKTthKHdpbmRvdylbYz9cIm9uXCI6XCJvZmZcIl0oZCxnKX19O2IucHJvdG90eXBlPXtjb25zdHJ1Y3RvcjpiLG5hbWVzcGFjZTpcImZlYXRoZXJsaWdodFwiLHRhcmdldEF0dHI6XCJkYXRhLWZlYXRoZXJsaWdodFwiLHZhcmlhbnQ6bnVsbCxyZXNldENzczohMSxiYWNrZ3JvdW5kOm51bGwsb3BlblRyaWdnZXI6XCJjbGlja1wiLGNsb3NlVHJpZ2dlcjpcImNsaWNrXCIsZmlsdGVyOm51bGwscm9vdDpcImJvZHlcIixvcGVuU3BlZWQ6MjUwLGNsb3NlU3BlZWQ6MjUwLGNsb3NlT25DbGljazpcImJhY2tncm91bmRcIixjbG9zZU9uRXNjOiEwLGNsb3NlSWNvbjpcIiYjMTAwMDU7XCIsbG9hZGluZzpcIlwiLHBlcnNpc3Q6ITEsb3RoZXJDbG9zZTpudWxsLGJlZm9yZU9wZW46YS5ub29wLGJlZm9yZUNvbnRlbnQ6YS5ub29wLGJlZm9yZUNsb3NlOmEubm9vcCxhZnRlck9wZW46YS5ub29wLGFmdGVyQ29udGVudDphLm5vb3AsYWZ0ZXJDbG9zZTphLm5vb3Asb25LZXlVcDphLm5vb3Asb25SZXNpemU6YS5ub29wLHR5cGU6bnVsbCxjb250ZW50RmlsdGVyczpbXCJqcXVlcnlcIixcImltYWdlXCIsXCJodG1sXCIsXCJhamF4XCIsXCJpZnJhbWVcIixcInRleHRcIl0sc2V0dXA6ZnVuY3Rpb24oYixjKXtcIm9iamVjdFwiIT10eXBlb2YgYnx8YiBpbnN0YW5jZW9mIGEhPSExfHxjfHwoYz1iLGI9dm9pZCAwKTt2YXIgZD1hLmV4dGVuZCh0aGlzLGMse3RhcmdldDpifSksZT1kLnJlc2V0Q3NzP2QubmFtZXNwYWNlK1wiLXJlc2V0XCI6ZC5uYW1lc3BhY2UsZj1hKGQuYmFja2dyb3VuZHx8Wyc8ZGl2IGNsYXNzPVwiJytlK1wiLWxvYWRpbmcgXCIrZSsnXCI+JywnPGRpdiBjbGFzcz1cIicrZSsnLWNvbnRlbnRcIj4nLCc8c3BhbiBjbGFzcz1cIicrZStcIi1jbG9zZS1pY29uIFwiK2QubmFtZXNwYWNlKyctY2xvc2VcIj4nLGQuY2xvc2VJY29uLFwiPC9zcGFuPlwiLCc8ZGl2IGNsYXNzPVwiJytkLm5hbWVzcGFjZSsnLWlubmVyXCI+JytkLmxvYWRpbmcrXCI8L2Rpdj5cIixcIjwvZGl2PlwiLFwiPC9kaXY+XCJdLmpvaW4oXCJcIikpLGc9XCIuXCIrZC5uYW1lc3BhY2UrXCItY2xvc2VcIisoZC5vdGhlckNsb3NlP1wiLFwiK2Qub3RoZXJDbG9zZTpcIlwiKTtyZXR1cm4gZC4kaW5zdGFuY2U9Zi5jbG9uZSgpLmFkZENsYXNzKGQudmFyaWFudCksZC4kaW5zdGFuY2Uub24oZC5jbG9zZVRyaWdnZXIrXCIuXCIrZC5uYW1lc3BhY2UsZnVuY3Rpb24oYil7dmFyIGM9YShiLnRhcmdldCk7KFwiYmFja2dyb3VuZFwiPT09ZC5jbG9zZU9uQ2xpY2smJmMuaXMoXCIuXCIrZC5uYW1lc3BhY2UpfHxcImFueXdoZXJlXCI9PT1kLmNsb3NlT25DbGlja3x8Yy5jbG9zZXN0KGcpLmxlbmd0aCkmJihkLmNsb3NlKGIpLGIucHJldmVudERlZmF1bHQoKSl9KSx0aGlzfSxnZXRDb250ZW50OmZ1bmN0aW9uKCl7aWYodGhpcy5wZXJzaXN0IT09ITEmJnRoaXMuJGNvbnRlbnQpcmV0dXJuIHRoaXMuJGNvbnRlbnQ7dmFyIGI9dGhpcyxjPXRoaXMuY29uc3RydWN0b3IuY29udGVudEZpbHRlcnMsZD1mdW5jdGlvbihhKXtyZXR1cm4gYi4kY3VycmVudFRhcmdldCYmYi4kY3VycmVudFRhcmdldC5hdHRyKGEpfSxlPWQoYi50YXJnZXRBdHRyKSxmPWIudGFyZ2V0fHxlfHxcIlwiLGc9Y1tiLnR5cGVdO2lmKCFnJiZmIGluIGMmJihnPWNbZl0sZj1iLnRhcmdldCYmZSksZj1mfHxkKFwiaHJlZlwiKXx8XCJcIiwhZylmb3IodmFyIGggaW4gYyliW2hdJiYoZz1jW2hdLGY9YltoXSk7aWYoIWcpe3ZhciBpPWY7aWYoZj1udWxsLGEuZWFjaChiLmNvbnRlbnRGaWx0ZXJzLGZ1bmN0aW9uKCl7cmV0dXJuIGc9Y1t0aGlzXSxnLnRlc3QmJihmPWcudGVzdChpKSksIWYmJmcucmVnZXgmJmkubWF0Y2gmJmkubWF0Y2goZy5yZWdleCkmJihmPWkpLCFmfSksIWYpcmV0dXJuXCJjb25zb2xlXCJpbiB3aW5kb3cmJndpbmRvdy5jb25zb2xlLmVycm9yKFwiRmVhdGhlcmxpZ2h0OiBubyBjb250ZW50IGZpbHRlciBmb3VuZCBcIisoaT8nIGZvciBcIicraSsnXCInOlwiIChubyB0YXJnZXQgc3BlY2lmaWVkKVwiKSksITF9cmV0dXJuIGcucHJvY2Vzcy5jYWxsKGIsZil9LHNldENvbnRlbnQ6ZnVuY3Rpb24oYil7dmFyIGM9dGhpcztyZXR1cm4oYi5pcyhcImlmcmFtZVwiKXx8YShcImlmcmFtZVwiLGIpLmxlbmd0aD4wKSYmYy4kaW5zdGFuY2UuYWRkQ2xhc3MoYy5uYW1lc3BhY2UrXCItaWZyYW1lXCIpLGMuJGluc3RhbmNlLnJlbW92ZUNsYXNzKGMubmFtZXNwYWNlK1wiLWxvYWRpbmdcIiksYy4kaW5zdGFuY2UuZmluZChcIi5cIitjLm5hbWVzcGFjZStcIi1pbm5lclwiKS5ub3QoYikuc2xpY2UoMSkucmVtb3ZlKCkuZW5kKCkucmVwbGFjZVdpdGgoYS5jb250YWlucyhjLiRpbnN0YW5jZVswXSxiWzBdKT9cIlwiOmIpLGMuJGNvbnRlbnQ9Yi5hZGRDbGFzcyhjLm5hbWVzcGFjZStcIi1pbm5lclwiKSxjfSxvcGVuOmZ1bmN0aW9uKGIpe3ZhciBkPXRoaXM7aWYoZC4kaW5zdGFuY2UuaGlkZSgpLmFwcGVuZFRvKGQucm9vdCksIShiJiZiLmlzRGVmYXVsdFByZXZlbnRlZCgpfHxkLmJlZm9yZU9wZW4oYik9PT0hMSkpe2ImJmIucHJldmVudERlZmF1bHQoKTt2YXIgZT1kLmdldENvbnRlbnQoKTtpZihlKXJldHVybiBjLnB1c2goZCksaCghMCksZC4kaW5zdGFuY2UuZmFkZUluKGQub3BlblNwZWVkKSxkLmJlZm9yZUNvbnRlbnQoYiksYS53aGVuKGUpLmFsd2F5cyhmdW5jdGlvbihhKXtkLnNldENvbnRlbnQoYSksZC5hZnRlckNvbnRlbnQoYil9KS50aGVuKGQuJGluc3RhbmNlLnByb21pc2UoKSkuZG9uZShmdW5jdGlvbigpe2QuYWZ0ZXJPcGVuKGIpfSl9cmV0dXJuIGQuJGluc3RhbmNlLmRldGFjaCgpLGEuRGVmZXJyZWQoKS5yZWplY3QoKS5wcm9taXNlKCl9LGNsb3NlOmZ1bmN0aW9uKGIpe3ZhciBjPXRoaXMsZT1hLkRlZmVycmVkKCk7cmV0dXJuIGMuYmVmb3JlQ2xvc2UoYik9PT0hMT9lLnJlamVjdCgpOigwPT09ZChjKS5sZW5ndGgmJmgoITEpLGMuJGluc3RhbmNlLmZhZGVPdXQoYy5jbG9zZVNwZWVkLGZ1bmN0aW9uKCl7Yy4kaW5zdGFuY2UuZGV0YWNoKCksYy5hZnRlckNsb3NlKGIpLGUucmVzb2x2ZSgpfSkpLGUucHJvbWlzZSgpfSxyZXNpemU6ZnVuY3Rpb24oYSxiKXtpZihhJiZiKXt0aGlzLiRjb250ZW50LmNzcyhcIndpZHRoXCIsXCJcIikuY3NzKFwiaGVpZ2h0XCIsXCJcIik7dmFyIGM9TWF0aC5tYXgoYS8ocGFyc2VJbnQodGhpcy4kY29udGVudC5wYXJlbnQoKS5jc3MoXCJ3aWR0aFwiKSwxMCktMSksYi8ocGFyc2VJbnQodGhpcy4kY29udGVudC5wYXJlbnQoKS5jc3MoXCJoZWlnaHRcIiksMTApLTEpKTtjPjEmJihjPWIvTWF0aC5mbG9vcihiL2MpLHRoaXMuJGNvbnRlbnQuY3NzKFwid2lkdGhcIixcIlwiK2EvYytcInB4XCIpLmNzcyhcImhlaWdodFwiLFwiXCIrYi9jK1wicHhcIikpfX0sY2hhaW5DYWxsYmFja3M6ZnVuY3Rpb24oYil7Zm9yKHZhciBjIGluIGIpdGhpc1tjXT1hLnByb3h5KGJbY10sdGhpcyxhLnByb3h5KHRoaXNbY10sdGhpcykpfX0sYS5leHRlbmQoYix7aWQ6MCxhdXRvQmluZDpcIltkYXRhLWZlYXRoZXJsaWdodF1cIixkZWZhdWx0czpiLnByb3RvdHlwZSxjb250ZW50RmlsdGVyczp7anF1ZXJ5OntyZWdleDovXlsjLl1cXHcvLHRlc3Q6ZnVuY3Rpb24oYil7cmV0dXJuIGIgaW5zdGFuY2VvZiBhJiZifSxwcm9jZXNzOmZ1bmN0aW9uKGIpe3JldHVybiB0aGlzLnBlcnNpc3QhPT0hMT9hKGIpOmEoYikuY2xvbmUoITApfX0saW1hZ2U6e3JlZ2V4Oi9cXC4ocG5nfGpwZ3xqcGVnfGdpZnx0aWZmfGJtcHxzdmcpKFxcP1xcUyopPyQvaSxwcm9jZXNzOmZ1bmN0aW9uKGIpe3ZhciBjPXRoaXMsZD1hLkRlZmVycmVkKCksZT1uZXcgSW1hZ2UsZj1hKCc8aW1nIHNyYz1cIicrYisnXCIgYWx0PVwiXCIgY2xhc3M9XCInK2MubmFtZXNwYWNlKyctaW1hZ2VcIiAvPicpO3JldHVybiBlLm9ubG9hZD1mdW5jdGlvbigpe2YubmF0dXJhbFdpZHRoPWUud2lkdGgsZi5uYXR1cmFsSGVpZ2h0PWUuaGVpZ2h0LGQucmVzb2x2ZShmKX0sZS5vbmVycm9yPWZ1bmN0aW9uKCl7ZC5yZWplY3QoZil9LGUuc3JjPWIsZC5wcm9taXNlKCl9fSxodG1sOntyZWdleDovXlxccyo8W1xcdyFdW148XSo+Lyxwcm9jZXNzOmZ1bmN0aW9uKGIpe3JldHVybiBhKGIpfX0sYWpheDp7cmVnZXg6Ly4vLHByb2Nlc3M6ZnVuY3Rpb24oYil7dmFyIGM9YS5EZWZlcnJlZCgpLGQ9YShcIjxkaXY+PC9kaXY+XCIpLmxvYWQoYixmdW5jdGlvbihhLGIpe1wiZXJyb3JcIiE9PWImJmMucmVzb2x2ZShkLmNvbnRlbnRzKCkpLGMuZmFpbCgpfSk7cmV0dXJuIGMucHJvbWlzZSgpfX0saWZyYW1lOntwcm9jZXNzOmZ1bmN0aW9uKGIpe3ZhciBjPW5ldyBhLkRlZmVycmVkLGQ9YShcIjxpZnJhbWUvPlwiKTtyZXR1cm4gZC5oaWRlKCkuYXR0cihcInNyY1wiLGIpLmNzcyhlKHRoaXMsXCJpZnJhbWVcIikpLm9uKFwibG9hZFwiLGZ1bmN0aW9uKCl7Yy5yZXNvbHZlKGQuc2hvdygpKX0pLmFwcGVuZFRvKHRoaXMuJGluc3RhbmNlLmZpbmQoXCIuXCIrdGhpcy5uYW1lc3BhY2UrXCItY29udGVudFwiKSksYy5wcm9taXNlKCl9fSx0ZXh0Ontwcm9jZXNzOmZ1bmN0aW9uKGIpe3JldHVybiBhKFwiPGRpdj5cIix7dGV4dDpifSl9fX0sZnVuY3Rpb25BdHRyaWJ1dGVzOltcImJlZm9yZU9wZW5cIixcImFmdGVyT3BlblwiLFwiYmVmb3JlQ29udGVudFwiLFwiYWZ0ZXJDb250ZW50XCIsXCJiZWZvcmVDbG9zZVwiLFwiYWZ0ZXJDbG9zZVwiXSxyZWFkRWxlbWVudENvbmZpZzpmdW5jdGlvbihiLGMpe3ZhciBkPXRoaXMsZT1uZXcgUmVnRXhwKFwiXmRhdGEtXCIrYytcIi0oLiopXCIpLGY9e307cmV0dXJuIGImJmIuYXR0cmlidXRlcyYmYS5lYWNoKGIuYXR0cmlidXRlcyxmdW5jdGlvbigpe3ZhciBiPXRoaXMubmFtZS5tYXRjaChlKTtpZihiKXt2YXIgYz10aGlzLnZhbHVlLGc9YS5jYW1lbENhc2UoYlsxXSk7aWYoYS5pbkFycmF5KGcsZC5mdW5jdGlvbkF0dHJpYnV0ZXMpPj0wKWM9bmV3IEZ1bmN0aW9uKGMpO2Vsc2UgdHJ5e2M9SlNPTi5wYXJzZShjKX1jYXRjaChoKXt9ZltnXT1jfX0pLGZ9LGV4dGVuZDpmdW5jdGlvbihiLGMpe3ZhciBkPWZ1bmN0aW9uKCl7dGhpcy5jb25zdHJ1Y3Rvcj1ifTtyZXR1cm4gZC5wcm90b3R5cGU9dGhpcy5wcm90b3R5cGUsYi5wcm90b3R5cGU9bmV3IGQsYi5fX3N1cGVyX189dGhpcy5wcm90b3R5cGUsYS5leHRlbmQoYix0aGlzLGMpLGIuZGVmYXVsdHM9Yi5wcm90b3R5cGUsYn0sYXR0YWNoOmZ1bmN0aW9uKGIsYyxkKXt2YXIgZT10aGlzO1wib2JqZWN0XCIhPXR5cGVvZiBjfHxjIGluc3RhbmNlb2YgYSE9ITF8fGR8fChkPWMsYz12b2lkIDApLGQ9YS5leHRlbmQoe30sZCk7dmFyIGYsZz1kLm5hbWVzcGFjZXx8ZS5kZWZhdWx0cy5uYW1lc3BhY2UsaD1hLmV4dGVuZCh7fSxlLmRlZmF1bHRzLGUucmVhZEVsZW1lbnRDb25maWcoYlswXSxnKSxkKTtyZXR1cm4gYi5vbihoLm9wZW5UcmlnZ2VyK1wiLlwiK2gubmFtZXNwYWNlLGguZmlsdGVyLGZ1bmN0aW9uKGcpe3ZhciBpPWEuZXh0ZW5kKHskc291cmNlOmIsJGN1cnJlbnRUYXJnZXQ6YSh0aGlzKX0sZS5yZWFkRWxlbWVudENvbmZpZyhiWzBdLGgubmFtZXNwYWNlKSxlLnJlYWRFbGVtZW50Q29uZmlnKHRoaXMsaC5uYW1lc3BhY2UpLGQpLGo9Znx8YSh0aGlzKS5kYXRhKFwiZmVhdGhlcmxpZ2h0LXBlcnNpc3RlZFwiKXx8bmV3IGUoYyxpKTtcInNoYXJlZFwiPT09ai5wZXJzaXN0P2Y9ajpqLnBlcnNpc3QhPT0hMSYmYSh0aGlzKS5kYXRhKFwiZmVhdGhlcmxpZ2h0LXBlcnNpc3RlZFwiLGopLGkuJGN1cnJlbnRUYXJnZXQuYmx1cigpLGoub3BlbihnKX0pLGJ9LGN1cnJlbnQ6ZnVuY3Rpb24oKXt2YXIgYT10aGlzLm9wZW5lZCgpO3JldHVybiBhW2EubGVuZ3RoLTFdfHxudWxsfSxvcGVuZWQ6ZnVuY3Rpb24oKXt2YXIgYj10aGlzO3JldHVybiBkKCksYS5ncmVwKGMsZnVuY3Rpb24oYSl7cmV0dXJuIGEgaW5zdGFuY2VvZiBifSl9LGNsb3NlOmZ1bmN0aW9uKGEpe3ZhciBiPXRoaXMuY3VycmVudCgpO3JldHVybiBiP2IuY2xvc2UoYSk6dm9pZCAwfSxfb25SZWFkeTpmdW5jdGlvbigpe3ZhciBiPXRoaXM7Yi5hdXRvQmluZCYmKGEoYi5hdXRvQmluZCkuZWFjaChmdW5jdGlvbigpe2IuYXR0YWNoKGEodGhpcykpfSksYShkb2N1bWVudCkub24oXCJjbGlja1wiLGIuYXV0b0JpbmQsZnVuY3Rpb24oYyl7Yy5pc0RlZmF1bHRQcmV2ZW50ZWQoKXx8XCJmZWF0aGVybGlnaHRcIj09PWMubmFtZXNwYWNlfHwoYy5wcmV2ZW50RGVmYXVsdCgpLGIuYXR0YWNoKGEoYy5jdXJyZW50VGFyZ2V0KSksYShjLnRhcmdldCkudHJpZ2dlcihcImNsaWNrLmZlYXRoZXJsaWdodFwiKSl9KSl9LF9jYWxsYmFja0NoYWluOntvbktleVVwOmZ1bmN0aW9uKGIsYyl7cmV0dXJuIDI3PT09Yy5rZXlDb2RlPyh0aGlzLmNsb3NlT25Fc2MmJmEuZmVhdGhlcmxpZ2h0LmNsb3NlKGMpLCExKTpiKGMpfSxvblJlc2l6ZTpmdW5jdGlvbihhLGIpe3JldHVybiB0aGlzLnJlc2l6ZSh0aGlzLiRjb250ZW50Lm5hdHVyYWxXaWR0aCx0aGlzLiRjb250ZW50Lm5hdHVyYWxIZWlnaHQpLGEoYil9LGFmdGVyQ29udGVudDpmdW5jdGlvbihhLGIpe3ZhciBjPWEoYik7cmV0dXJuIHRoaXMub25SZXNpemUoYiksY319fSksYS5mZWF0aGVybGlnaHQ9YixhLmZuLmZlYXRoZXJsaWdodD1mdW5jdGlvbihhLGMpe3JldHVybiBiLmF0dGFjaCh0aGlzLGEsYyl9LGEoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7Yi5fb25SZWFkeSgpfSl9KGpRdWVyeSk7IiwiKCBmdW5jdGlvbiggJCApIHtcclxuXHR2YXIgdGltZXI7XHJcblxyXG5cdCQoJy5qcy1hY2NvdW50JykuY2xpY2soZnVuY3Rpb24oZSl7XHJcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG5cdFx0JCgnI2FjY291bnQtcG9wdXAnKS5hZGRDbGFzcygnb24nKTtcclxuXHRcdCQoJy5mZWF0dXJlLXBvcHVwLW91dGVyLWNvbnRhaW5lcicpLmFkZENsYXNzKCdvbicpO1xyXG5cclxuXHRcdHRpbWVyID0gd2luZG93LnNldFRpbWVvdXQoaGlkZVBvcCwgNDAwMCk7XHJcblx0fSk7XHJcblxyXG5cdCQoJy5qcy1mYXZvcml0ZXMnKS5jbGljayhmdW5jdGlvbihlKXtcclxuXHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcblx0XHQkKCcjZmF2b3JpdGVzLXBvcHVwJykuYWRkQ2xhc3MoJ29uJyk7XHJcblx0XHQkKCcuZmVhdHVyZS1wb3B1cC1vdXRlci1jb250YWluZXInKS5hZGRDbGFzcygnb24nKTtcclxuXHJcblx0XHR0aW1lciA9IHdpbmRvdy5zZXRUaW1lb3V0KGhpZGVQb3AsIDQwMDApO1xyXG5cdH0pO1xyXG5cclxuXHQkKCcuanMtaXRpbmVyYXJ5JykuY2xpY2soZnVuY3Rpb24oZSl7XHJcblx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG5cdFx0JCgnI2JyZXdlcnktcG9wdXAnKS5hZGRDbGFzcygnb24nKTtcclxuXHRcdCQoJy5mZWF0dXJlLXBvcHVwLW91dGVyLWNvbnRhaW5lcicpLmFkZENsYXNzKCdvbicpO1xyXG5cclxuXHRcdHRpbWVyID0gd2luZG93LnNldFRpbWVvdXQoaGlkZVBvcCwgNDAwMCk7XHJcblx0fSk7XHJcblxyXG5cdCQoJy5mZWF0dXJlLXBvcHVwJykub24oJ2NsaWNrJywgJy5jbG9zZScsIGZ1bmN0aW9uKCl7XHJcblxyXG5cdFx0JCgnLmZlYXR1cmUtcG9wdXAtb3V0ZXItY29udGFpbmVyJykuYW5pbWF0ZSh7XHJcblx0XHRcdHRvcDogJy05MDAnLFxyXG5cdFx0XHRvcGFjaXR5OiAwXHJcblx0XHR9LCA1MDAsIGZ1bmN0aW9uKCl7XHJcblx0XHRcdCQodGhpcykucGFyZW50KCkucmVtb3ZlQ2xhc3MoJ29uJyk7IFxyXG5cdFx0XHQkKCcuZmVhdHVyZS1wb3B1cCcpLnJlbW92ZUNsYXNzKCdvbicpO1xyXG5cdFx0XHQkKCcuZmVhdHVyZS1wb3B1cC1vdXRlci1jb250YWluZXInKS5jc3Moeyd0b3AnOiAwLCAnb3BhY2l0eSc6IDF9KS5yZW1vdmVDbGFzcygnb24nKTtcclxuXHRcdH0pO1xyXG5cclxuXHRcdHdpbmRvdy5jbGVhclRpbWVvdXQodGltZXIpO1x0XHJcblx0fSk7XHJcblxyXG5cclxuXHRmdW5jdGlvbiBoaWRlUG9wKCl7XHJcblx0XHQkKCcuZmVhdHVyZS1wb3B1cCAuY2xvc2UnKS50cmlnZ2VyKCdjbGljaycpO1x0XHRcclxuXHR9XHJcblxyXG5cclxufSApKCBqUXVlcnkgKTsiLCIvKipcclxuICogRmlsZSBqcy1lbmFibGVkLmpzXHJcbiAqXHJcbiAqIElmIEphdmFzY3JpcHQgaXMgZW5hYmxlZCwgcmVwbGFjZSB0aGUgPGJvZHk+IGNsYXNzIFwibm8tanNcIi5cclxuICovXHJcbmRvY3VtZW50LmJvZHkuY2xhc3NOYW1lID0gZG9jdW1lbnQuYm9keS5jbGFzc05hbWUucmVwbGFjZSggJ25vLWpzJywgJ2pzJyApOyIsIi8qKlxyXG4gKiBGaWxlIHNraXAtbGluay1mb2N1cy1maXguanMuXHJcbiAqXHJcbiAqIEhlbHBzIHdpdGggYWNjZXNzaWJpbGl0eSBmb3Iga2V5Ym9hcmQgb25seSB1c2Vycy5cclxuICpcclxuICogTGVhcm4gbW9yZTogaHR0cHM6Ly9naXQuaW8vdldkcjJcclxuICovXHJcbiggZnVuY3Rpb24oKSB7XHJcblx0dmFyIGlzV2Via2l0ID0gbmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpLmluZGV4T2YoICd3ZWJraXQnICkgPiAtMSxcclxuXHQgICAgaXNPcGVyYSAgPSBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ29wZXJhJyApICA+IC0xLFxyXG5cdCAgICBpc0llICAgICA9IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCAnbXNpZScgKSAgID4gLTE7XHJcblxyXG5cdGlmICggKCBpc1dlYmtpdCB8fCBpc09wZXJhIHx8IGlzSWUgKSAmJiBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCAmJiB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lciApIHtcclxuXHRcdHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCAnaGFzaGNoYW5nZScsIGZ1bmN0aW9uKCkge1xyXG5cdFx0XHR2YXIgaWQgPSBsb2NhdGlvbi5oYXNoLnN1YnN0cmluZyggMSApLFxyXG5cdFx0XHRcdGVsZW1lbnQ7XHJcblxyXG5cdFx0XHRpZiAoICEgKCAvXltBLXowLTlfLV0rJC8udGVzdCggaWQgKSApICkge1xyXG5cdFx0XHRcdHJldHVybjtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0ZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCBpZCApO1xyXG5cclxuXHRcdFx0aWYgKCBlbGVtZW50ICkge1xyXG5cdFx0XHRcdGlmICggISAoIC9eKD86YXxzZWxlY3R8aW5wdXR8YnV0dG9ufHRleHRhcmVhKSQvaS50ZXN0KCBlbGVtZW50LnRhZ05hbWUgKSApICkge1xyXG5cdFx0XHRcdFx0ZWxlbWVudC50YWJJbmRleCA9IC0xO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0ZWxlbWVudC5mb2N1cygpO1xyXG5cdFx0XHR9XHJcblx0XHR9LCBmYWxzZSApO1xyXG5cdH1cclxufSkoKTsiLCIoIGZ1bmN0aW9uKCAkICkge1xyXG5cclxuXHQkKCcudGFiYmVkLXVpJykuZmluZCgnLmRheS10YWItMScpLmFkZENsYXNzKCdvbicpO1xyXG5cdCQoJy50YWJiZWQtdWknKS5maW5kKCcuZGF5LTEnKS5hZGRDbGFzcygnb24nKTtcclxuXHJcblx0ZnVuY3Rpb24gZG9UYWJzKCBlbCApe1x0XHJcblx0XHR2YXIgdGFiSGVpZ2h0ID0gNDQ7XHRcclxuXHRcdHRhYkhlaWdodCArPSBlbC5oZWlnaHQoKTtcclxuXHRcdCQoJy50YWJiZWQtdWknKS5maW5kKCcuZGF5cycpLmhlaWdodCggdGFiSGVpZ2h0ICk7XHJcblx0fVxyXG5cdFxyXG5cdGRvVGFicyggJCgnLnRhYmJlZC11aScpLmZpbmQoJy5kYXktMScpICk7XHJcblxyXG5cclxuXHQkKCcudGFiJykuY2xpY2soZnVuY3Rpb24oKXtcclxuXHRcdCQoJy50YWInKS5yZW1vdmVDbGFzcygnb24nKTtcclxuXHRcdCQoJy50YWJiZWQtdWknKS5maW5kKCcuZGF5JykucmVtb3ZlQ2xhc3MoJ29uJyk7XHJcblxyXG5cdFx0dmFyIGVsID0gJCh0aGlzKTtcclxuXHRcdGVsLmFkZENsYXNzKCdvbicpO1xyXG5cclxuXHRcdHZhciByZWdleCA9IC9cXGR7MX0vO1xyXG5cclxuXHRcdHZhciBudW0gPSBlbC5hdHRyKCdjbGFzcycpLm1hdGNoKHJlZ2V4KTtcclxuXHRcdCQoJy5kYXktJyArIG51bSApLmFkZENsYXNzKCdvbicpO1xyXG5cdFx0XHJcblxyXG5cdFx0ZG9UYWJzKCAkKCcuZGF5LScgKyBudW0gKSApO1xyXG5cdH0pO1xyXG5cclxufSApKCBqUXVlcnkgKTsiLCIvKipcclxuICogRmlsZSB3aW5kb3ctcmVhZHkuanNcclxuICpcclxuICogQWRkIGEgXCJyZWFkeVwiIGNsYXNzIHRvIDxib2R5PiB3aGVuIHdpbmRvdyBpcyByZWFkeS5cclxuICovXHJcbndpbmRvdy5XaW5kb3dfUmVhZHkgPSB7fTtcclxuKCBmdW5jdGlvbiggd2luZG93LCAkLCB0aGF0ICkge1xyXG5cclxuXHQvLyBDb25zdHJ1Y3Rvci5cclxuXHR0aGF0LmluaXQgPSBmdW5jdGlvbigpIHtcclxuXHRcdHRoYXQuY2FjaGUoKTtcclxuXHRcdHRoYXQuYmluZEV2ZW50cygpO1xyXG5cdH07XHJcblxyXG5cdC8vIENhY2hlIGRvY3VtZW50IGVsZW1lbnRzLlxyXG5cdHRoYXQuY2FjaGUgPSBmdW5jdGlvbigpIHtcclxuXHRcdHRoYXQuJGMgPSB7XHJcblx0XHRcdHdpbmRvdzogJCh3aW5kb3cpLFxyXG5cdFx0XHRib2R5OiAkKGRvY3VtZW50LmJvZHkpLFxyXG5cdFx0fTtcclxuXHR9O1xyXG5cclxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXHJcblx0dGhhdC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XHJcblx0XHR0aGF0LiRjLndpbmRvdy5sb2FkKCB0aGF0LmFkZEJvZHlDbGFzcyApO1xyXG5cdH07XHJcblxyXG5cdC8vIEFkZCBhIGNsYXNzIHRvIDxib2R5Pi5cclxuXHR0aGF0LmFkZEJvZHlDbGFzcyA9IGZ1bmN0aW9uKCkge1xyXG5cdFx0dGhhdC4kYy5ib2R5LmFkZENsYXNzKCAncmVhZHknICk7XHJcblx0fTtcclxuXHJcblx0Ly8gRW5nYWdlIVxyXG5cdCQoIHRoYXQuaW5pdCApO1xyXG5cclxufSkoIHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cuV2luZG93X1JlYWR5ICk7Il0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9
