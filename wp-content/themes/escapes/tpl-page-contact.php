<?php
/**
 * Template Name: Contact Page
 *
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">

				<?php
				while ( have_posts() ) : the_post();
				?>
					<article <?php post_class(); ?>>
					
						<div class="entry-content">
							<?php
								the_content();							
							?>
						</div><!-- .entry-content -->		
						
						<a href="tel:+19512510295" class="phone-number h2">(951) 251-0295</a>

						<?php echo do_shortcode('[media-kit arrow="true"]Write Us[/media-kit]'); ?>
						
					</article><!-- #post-## -->

				<?php
				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->

<?php get_footer(); ?>