<?php
/**
 * The template for displaying the experience landing page.
 *
 * @package Temecula Escapes
 */

get_header(); ?>

	<div class="wrap">
		<div class="primary content-area">
			<main id="main" class="site-main" role="main">				
				<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<p id="breadcrumbs">','</p>');
				}
				?>

				<h1>Places to Eat &amp; Drink in Temecula</h1>
				<article>
					<p>The wines, brews and foods within Temecula Valley offer a variety like none other. From the wineries and breweries to the exceptional bars and restaurants, you can find a wide selection of delicious drinks and foods to please every palate and diet. With so many options, it may be hard to decide, but that’s where we come in. On this page, you will find an assortment of the best places to eat and drink in Temecula. </p>
				</article>
				
				<?php echo do_shortcode('[caldera_clarity name="eat_and_drink"]'); ?>

			</main><!-- #main -->
		</div><!-- .primary -->

	</div><!-- .wrap -->


	<?php 

		//See inc/template-tags.php for function definition
		escapes_do_local_favorites( 'eat_and_drink' ); 
	?>

<?php get_footer(); ?>