(function ($) {
	"use strict";
	$(function () {
		$(document).ready(function(){

			var dateTime = $('.te_countdown_clock_widget').data('date');
			var hasElapsed = $('.te_countdown_clock_widget').data( 'elapsed' );


			$('.te_countdown_clock_widget').countdown(dateTime, function(event){
				$(this).find('.clock-inner').html( event.strftime( '<span class="clock-panel"><span class="number months">%m</span> <span class="label">Months</span></span> <span class="clock-panel"><span class="number days">%n</span> <span class="label">Days</span></span> <span class="clock-panel"><span class="number hours">%H</span> <span class="label">Hours</span></span> <span class="clock-panel"><span class="number minutes">%M</span> <span class="label">Minutes</span></span>' ));
			});
			
		});
	});
}(jQuery));