<!-- This file is used to markup the public-facing widget. -->
<?php
	// wp_enqueue_script( 'event-countdown-script');
	// wp_enqueue_script( 'event-countdown-moment-script');
	// wp_enqueue_script( 'event-countdown-moment-timezone-script');






	date_default_timezone_set('America/Los_Angeles');

	global $post;
	//$page_id = $post->ID;
	$post_id = $post->ID;
	$title = '';
	$elapse = '';

	$date = get_field('event_date', $post_id);
	$date_end = get_field('event_end_date', $post_id);


	// echo 'starts: '.$date . '<br>';
	// echo 'ends: ' .   get_field('event_end_date', $post_id) . '<br>';
	// echo 'now: ' . date( 'now' );





	//CHECK IF THE EVENT HAS PASSED
	if( strtotime( 'now' ) > strtotime( $date_end ) ){
		
		$title = $instance['title_final_end'];
		$date = '';
		$elapsed = 'true'; 
		//echo '<br>Has passed';

	} else if(  strtotime( 'now' ) < strtotime( $date )  ) {

		$title = $instance['title'];
		$elapsed = 'false';
		//event in progress

	}else{

		$title = $instance['title_end'];
		$date = $date_end;
		$elapsed = 'false';
	}



?>



<div id="clock" class="te_countdown_clock_widget<?php echo $class; ?>" data-date="<?php echo $date; ?>" data-elapsed="<?php echo $elapsed; ?>">
	<span class="h2"><?php echo $title; ?></span>
	<div class="clock-inner"><?php echo $date; ?></div>
</div>