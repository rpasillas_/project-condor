<!-- This file is used to markup the administration form of the widget. -->
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'title_end' ); ?>"><?php _e( 'Title while event is in progress:' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title_end' ); ?>" name="<?php echo $this->get_field_name( 'title_end' ); ?>" type="text" value="<?php echo esc_attr( $title_end ); ?>" />
</p>
<p>
	<label for="<?php echo $this->get_field_id( 'title_final_end' ); ?>"><?php _e( 'Title end of event:' ); ?></label> 
	<input class="widefat" id="<?php echo $this->get_field_id( 'title_final_end' ); ?>" name="<?php echo $this->get_field_name( 'title_final_end' ); ?>" type="text" value="<?php echo esc_attr( $title_final_end ); ?>" />
</p>