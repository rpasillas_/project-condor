<?php
/**
 * Plugin Name: MailChimp Caldera Forms Pre-processor
 * Plugin URI:  
 * Description: Submits a entries into Temecula Escapes' newsletter list. This plugin looks for the following classes to be applied to the forms, .mc_name, .mc_email.
 * Version:     1.0.0
 * Author:      Ron Pasillas
 * Author URI:  http://evaero.co
 */


require_once( 'cf-mc-newsletter.php' );



// Register the form processor
function cf_mc_pre_processor_register( $processors ) {
	// Add our processor to the $processors array using our processor_slug as the key.
	// It is possible to replace an existing processor by redefining it and hooking in with a lower priority i.e 100
	$processors['my_processor_slug'] 	= array(
		"name"              =>  "MailChimp Newsletter Signup",						// Required	 	: Processor name
		"description"       =>  "Submits to Temecula Escapes MailChimp newsletter, yes built with love by Ron.",	// Required 	: Processor description
		"pre_processor"	    =>  'accuity_pre_processor_function',	// Optional 	: Processor function to do stuff
	);
	return $processors;
}

// add filter to use the processor register
add_filter( 'caldera_forms_get_form_processors', 'cf_mc_pre_processor_register' ); 



//LOAD JQUERY SCRIPT
function plugin_scripts(){	
	wp_enqueue_script( 'cf-mailchimp',  plugins_url('mailchimp-for-caldera/jquery.cf-mc.js'), array( 'jquery' ), '1.0', 1000 );
}
add_action( 'wp_enqueue_scripts', 'plugin_scripts' );


/* register route wp rest api */
//http://v2.wp-api.org/extending/adding/
add_action( 'rest_api_init', function () {
    register_rest_route( 'newsletter/v1', '/subscribe', array(
        'methods' => 'GET',
        'callback' => 'mainFunctions',
    ) );
} );


