(function ($, root, undefined) {

	$(function () {
		'use strict';

		//Set up Variables
		var firstname, lastname, email, promo;
		var url = '/wp-json/newsletter/v1/subscribe';

		//Submit it
		$('.caldera_forms_form').submit(function(){		
			var el = $(this),
				name;

			name = el.find('.mc_name').find('input').val();				

			if( typeof name != "undefined"){
				firstname = name.split(' ').slice(0,-1).join(' ');
				lastname = name.split(' ').slice(-1).join(' ');
			}

			email = el.find('.mc_email').find('input').val();

			if( typeof name != "undefined"){
				promo = el.find('.mc_promo').val();
			}		

			if( el.find('.mc_checkbox') && el.find('.mc_checkbox').find('input').is(':checked') ){
				doAjax();
			}else if( el.find('.mc_checkbox').length == 0 ) {
				doAjax();
			}else{
				console.log('return');
				return;
			}

			console.log('first: ' + firstname);
			console.log('last: ' + lastname);
		});

		function doAjax(){
			$.ajax({
				url: url,
				data: {
					"email"		: email,
					"firstname" : firstname,
					"lastname"	: lastname,
					"promo"		: promo
				}

			}).done( function( data ){
				if(data == 400 || data == 400 ){
					console.log('MailChimp plugin error.');					
				}else{
					console.log( 'MailChimp Success.');
				}
								
			}).fail( function( data ){
				console.log( 'MailChimp Submit error.');
			});
		}


	});

})(jQuery, this);