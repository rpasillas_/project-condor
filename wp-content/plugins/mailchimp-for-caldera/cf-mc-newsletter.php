<?php
function mainFunctions( WP_REST_Request $request ) {
    if( $request->get_param('promo') ){ 
        $promo = $request->get_param('promo');
    }else{ 
        $promo = '';
    }

    if( $request->get_param('firstname') ){ 
        $firstname = $request->get_param('firstname');
    }else{ 
        $firstname = '';
    }

    if( $request->get_param('lastname') ){ 
        $lastname = $request->get_param('lastname');
    }else{ 
        $lastname = '';
    }

	$data = [
	    'email'     => $request->get_param('email'),
	    'status'    => 'subscribed',
	    'firstname' => $firstname,
	    'lastname'  => $lastname,
        'promo'     => $promo
	];

	// $data = [
	//     'email'     => 'blank_test@evaero.co',
	//     'status'    => 'subscribed',
	//     'firstname' => '',
	//     'lastname'  => ''
	// ];

	return syncMailchimp($data);
}


//http://stackoverflow.com/questions/30481979/adding-subscribers-to-a-list-using-mailchimps-api-v3
//http://developer.mailchimp.com/documentation/mailchimp/guides/manage-subscribers-with-the-mailchimp-api/
function syncMailchimp($data) {
    $apiKey = '6bd61cc9ebb6a021013bc1bcc691ed26-us6';
    $listId = 'd7c794b67e';

    $memberId = md5(strtolower($data['email']));
    $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
    $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;

    $json = json_encode([
        'email_address' => $data['email'],
        'status'        => $data['status'], // "subscribed","unsubscribed","cleaned","pending"
        'merge_fields'  => [
            'FNAME'     => $data['firstname'],
            'LNAME'     => $data['lastname'],
            'PROMO'     => $data['promo']
        ]
    ]);

    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

    $result = curl_exec($ch);

    //just the status code
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    //--MailChimp's result object
    //--full of all the goodies
    $httpCode = json_decode($result, true);

    return array($httpCode, $json );
}